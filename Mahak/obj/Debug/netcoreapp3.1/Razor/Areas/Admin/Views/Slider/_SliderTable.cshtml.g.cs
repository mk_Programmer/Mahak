#pragma checksum "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2a02443398d4c9a9863d0a2c54a5a687ac65fd2b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Slider__SliderTable), @"mvc.1.0.view", @"/Areas/Admin/Views/Slider/_SliderTable.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\_ViewImports.cshtml"
using Mahak;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\_ViewImports.cshtml"
using Mahak.Entities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
using Mahak.Services.Contracts;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2a02443398d4c9a9863d0a2c54a5a687ac65fd2b", @"/Areas/Admin/Views/Slider/_SliderTable.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d01fe8cb6acad401a26703a3bf3041af5621ab25", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Slider__SliderTable : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div id=\"toolbar\">\r\n    <button type=\"button\" class=\"btn btn-success\" data-toggle=\"ajax-modal\" data-url=\"");
#nullable restore
#line 5 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
                                                                                Write(Url.Action("RenderSlider","Slider"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n        <i class=\"fa fa-plus\"></i> | افزودن اسلایدر جدید\r\n    </button>\r\n\r\n    <button type=\"button\" class=\"btn btn-danger\" data-toggle=\"ajax-modal\" data-url=\"");
#nullable restore
#line 9 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
                                                                               Write(Url.Action("DeleteGroup","Slider"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@""">
        <i class=""fa fa-trash""></i> | حذف گروهی
    </button>
</div>

<table id=""table""
       data-toolbar=""#toolbar""
       data-search=""true""
       data-show-refresh=""true""
       data-show-toggle=""true""
       data-show-fullscreen=""true""
       data-show-columns=""true""
       data-detail-view=""true""
       data-show-export=""true""
       data-click-to-select=""true""
       data-detail-formatter=""detailFormatter""
       data-minimum-count-columns=""2""
       data-show-pagination-switch=""true""
       data-pagination=""true""
       data-id-field=""id""
       data-page-list=""[10, 25, 50, 100, all]""
       data-show-footer=""true""
       data-side-pagination=""server""
       data-url=""/Admin/Slider/GetSliders""
       data-response-handler=""responseHandler"">
</table>

<script>
    var $table = $('#table')
    var selections = []

    function get_query_params(p) {
        return {
            extraParam: 'abc',
            search: p.title,
            sort: p.sort,
            o");
            WriteLiteral(@"rder: p.order,
            limit: p.limit,
            offset: p.offset
        }
    }

    function responseHandler(res) {
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.id, selections) !== -1
        })
        return res
    }

    function detailFormatter(index, row) {
        var html = []
        $.each(row, function (key, value) {
            if (key != ""state"" && key != ""Id"" && key != ""ردیف"" && key != ""آدرس"" && key != ""تصویر اسلایدر"" && key != ""جایگاه اسلایدر"" && key != ""sliderLocations"")
                html.push('<p><b>' + key + ':</b> ' + value + '</p>')
            if (key == ""تصویر اسلایدر"") {
                var url = '");
#nullable restore
#line 64 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
                      Write(string.Format("{0}://{1}", Context.Request.Scheme, Context.Request.Host));

#line default
#line hidden
#nullable disable
            WriteLiteral("\' + \'/images/\' + value;\r\n                html.push(\'<p><b>\' + key + \':</b> <img src=\"\' + url + \'\" height=\"50\" /></p>\')\r\n            }\r\n            if (key == \"آدرس\") {\r\n                var url = \'");
#nullable restore
#line 68 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
                      Write(string.Format("{0}://{1}", Context.Request.Scheme, Context.Request.Host));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"' + '/Slider/' + row.Id + '/' + value;
                html.push('<p><b style=""float:right"">' + key + ':</b> ' + '<a href=""' + url + '"" style=""float:right;direction:ltr;margin-right:2px;"">'  + url + '</a>' + '</p>')
            }
        })
            return html.join('')
    }

    function operateFormatter(value, row, index) {
        var access = """";
        if ('");
#nullable restore
#line 77 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
        Write(securityTrimmingService.CanUserAccess(User,"Admin", "Slider", "RenderSlider"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\' == \'True\') {\r\n            access = access + \'<button type=\"button\" class=\"btn-link text-success\" data-toggle=\"ajax-modal\" data-url=");
#nullable restore
#line 78 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
                                                                                                                Write(Url.Action("RenderSlider", "Slider"));

#line default
#line hidden
#nullable disable
            WriteLiteral("?sliderId=\' + row.Id + \' title=\"ویرایش\"><i class=\"fa fa-edit\"></i></button >\';\r\n        }\r\n        if (\'");
#nullable restore
#line 80 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
        Write(securityTrimmingService.CanUserAccess(User,"Admin", "Slider", "Delete"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\' == \'True\') {\r\n            access = access + \'<button type=\"button\" class=\"btn-link text-danger\" data-toggle=\"ajax-modal\" data-url=");
#nullable restore
#line 81 "C:\Other\Training\Mahak\Mahak\Areas\Admin\Views\Slider\_SliderTable.cshtml"
                                                                                                               Write(Url.Action("Delete", "Slider"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"/?sliderId=' + row.Id + ' title=""حذف""><i class=""fa fa-trash""></i></button >';
        }
        return access;
    }

    function checkBoxFormat(value, row) {
       return '<input type=""checkbox"" name=""btSelectItem"" value=""' + row.Id + '"" />';
    }

    function totalTextFormatter(data) {
        return 'تعداد'
    }

    function totalNameFormatter(data) {
        return data.length
    }

    function initTable() {
        $table.bootstrapTable('destroy').bootstrapTable({
            height: 600,
            locale: 'fa-IR',
            columns: [
                [{
                    field: 'state',
                    checkbox: true,
                    rowspan: 2,
                    align: 'center',
                    valign: 'middle',
                    formatter: checkBoxFormat
                }, {
                    title: 'ردیف',
                    field: 'ردیف',
                    rowspan: 2,
                    align: 'center',
                    valign:");
            WriteLiteral(@" 'middle',
                    footerFormatter: totalTextFormatter
                }, {
                    title: 'جزئیات اطلاعات اسلایدر ها',
                    colspan: 3,
                    align: 'center'
                }],
                [{
                    field: 'عنوان اسلایدر',
                    title: 'عنوان اسلایدر',
                    sortable: true,
                    footerFormatter: totalNameFormatter,
                },{
                    field: 'جایگاه اسلایدر',
                    title: 'جایگاه',
                    sortable: true,
                    align: 'center'
                }, {
                    field: 'operate',
                    title: 'عملیات',
                    align: 'center',
                    events: window.operateEvents,
                    formatter: operateFormatter
                }]
            ]
        })
    }

    $(function () {
        initTable()
        $('#locale').change(initTable)
    })
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public ISecurityTrimmingService securityTrimmingService { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
