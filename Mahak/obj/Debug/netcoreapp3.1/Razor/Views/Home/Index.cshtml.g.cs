#pragma checksum "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7f2290431280c0a7edd856a84d319698f08af445"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 2 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
using Mahak.Common.Extensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
using Mahak.ViewModel.Newsletter;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7f2290431280c0a7edd856a84d319698f08af445", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c8ef5b53398b6c7f8c5eca5fdeed7bc18c19ce3", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Mahak.ViewModel.Home.HomePageViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("600"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("600"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("attachment-post-thumbnail size-post-thumbnail wp-post-image"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/img/SpecifiedImage.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("263"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("142"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("attachment-263x142 size-263x142 wp-post-image"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("no-image"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("rtl home blog theme-kalhors-mahak woocommerce-no-js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7f2290431280c0a7edd856a84d319698f08af4457025", async() => {
                WriteLiteral("\r\n    <div class=\"woocommerce-notices-shortcode woocommerce\"></div>\r\n    <!-- Slider -->\r\n    <div class=\"slider-block\">\r\n        <div class=\"slider\">\r\n");
#nullable restore
#line 14 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
             foreach (var item in Model.Sliders)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                 if (item.Locations == "صفحه اول")
                {

#line default
#line hidden
#nullable disable
                WriteLiteral("                    <a");
                BeginWriteAttribute("href", " href=\"", 580, "\"", 587, 0);
                EndWriteAttribute();
                WriteLiteral(">");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "7f2290431280c0a7edd856a84d319698f08af4458079", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 599, "~/images/", 599, 9, true);
#nullable restore
#line 18 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
AddHtmlAttributeValue("", 608, item.ImageName, 608, 15, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "alt", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 18 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
AddHtmlAttributeValue("", 630, item.Title, 630, 11, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("</a>\r\n");
#nullable restore
#line 19 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                 
            }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"        </div>
    </div>

    <!-- Pay - Banner -->
    <section class=""pay-banner-block"">
        <div class=""container"">
            <div class=""flex-pay-banner"">
                <div class=""pb-right pb-right-animation"">
                    <div class=""animations"" id=""first-animation""></div>
                </div>
                <div class=""pb-left"">
                    <div class=""flex-inner-pb"">
                        <div class=""pay-box"">
                            <div class=""pay-title-block"">
                                <h3 class=""ptb-h3"">  </h3>
                                <span class=""ptb-des""><p style=""text-align: justify;"">شما هم سهمی در نجات کودکان محک داشته باشید</p></span>
                            </div>
                            <a href=""https://mahak-charity.org/online-payment/"" class=""buttons index-pay-button"">کمک آنلاین</a>
                        </div>
                        <!--the banner-->
                        <a href=""https://mahak-charity.org/h");
                WriteLiteral(@"amdelii/""><img src=""https://mahak-charity.org/wp-content/uploads/2020/02/بنر-سایت2-1.jpg"" alt=""banner""></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Shop - Help -->
    <section class=""shop-help"">
        <div class=""container"">
            <div class=""flex-pay-banner flex-shop"">
                <div class=""pb-right"">
                    <div class=""help-block"">
                        <h3 class=""help-h3"">من چگونه می‌توانم به محک کمک کنم؟</h3>
                        <ul class=""help-ul"">
                            <li><a class=""help-ul-register"" href=""https://mahak-charity.org/membership-request/""> عضویت<span></span></a></li>
                            <li><a class=""help-ul-medical"" href=""https://mahak-charity.org/request-assistance/""> درمان یار<span></span></a></li>
                            <li><a class=""help-ul-other"" href=""https://mahak-charity.org/assistance-methods/""> سایر روش های کمک<span></span></a></li>
        ");
                WriteLiteral(@"                </ul>
                    </div>
                </div>
                <div class=""pb-left"">
                    <div class=""shop-box"">
                        <div class=""shop-lightslider-block"">
                            <div class=""shop-lightslider"">
");
#nullable restore
#line 66 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                 foreach (var item in Model.Products)
                                {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                    <div class=\"inner-shop-box\">\r\n                                        <a");
                BeginWriteAttribute("href", " href=\"", 3225, "\"", 3266, 4);
                WriteAttributeValue("", 3232, "/product/", 3232, 9, true);
#nullable restore
#line 69 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 3241, item.ProductId, 3241, 15, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 3256, "/", 3256, 1, true);
#nullable restore
#line 69 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 3257, item.Url, 3257, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"shop-img\">\r\n");
#nullable restore
#line 70 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                             if (item.ImageName != null)
                                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                                ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "7f2290431280c0a7edd856a84d319698f08af44514308", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 3466, "~/images/", 3466, 9, true);
#nullable restore
#line 72 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
AddHtmlAttributeValue("", 3475, item.ImageName, 3475, 15, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 73 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                                ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "7f2290431280c0a7edd856a84d319698f08af44516540", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 77 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                            }

#line default
#line hidden
#nullable disable
                WriteLiteral("                                        </a>\r\n                                        <h2 class=\"shop-h2\"><a");
                BeginWriteAttribute("href", " href=\"", 4090, "\"", 4131, 4);
                WriteAttributeValue("", 4097, "/product/", 4097, 9, true);
#nullable restore
#line 79 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 4106, item.ProductId, 4106, 15, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 4121, "/", 4121, 1, true);
#nullable restore
#line 79 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 4122, item.Url, 4122, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">");
#nullable restore
#line 79 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                                                                                    Write(item.ProductName);

#line default
#line hidden
#nullable disable
                WriteLiteral(" کد ( ");
#nullable restore
#line 79 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                                                                                                           Write(item.ProductCode);

#line default
#line hidden
#nullable disable
                WriteLiteral(" )</a></h2>\r\n                                        <ul class=\"prices\">\r\n                                            <li class=\"new-price\">");
#nullable restore
#line 81 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                                             Write(item.Price);

#line default
#line hidden
#nullable disable
                WriteLiteral(" ریال</li>\r\n                                        </ul>\r\n                                        <ul class=\"shop-buttons\">\r\n                                            <li><a");
                BeginWriteAttribute("href", " href=\"", 4500, "\"", 4541, 4);
                WriteAttributeValue("", 4507, "/product/", 4507, 9, true);
#nullable restore
#line 84 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 4516, item.ProductId, 4516, 15, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 4531, "/", 4531, 1, true);
#nullable restore
#line 84 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 4532, item.Url, 4532, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"shop-read-more\">جزییات</a></li>\r\n                                            <li>\r\n                                                <a class=\"shop-add-to-cart\"");
                BeginWriteAttribute("href", " href=\"", 4708, "\"", 4739, 2);
                WriteAttributeValue("", 4715, "/product/", 4715, 9, true);
#nullable restore
#line 86 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 4724, item.ProductId, 4724, 15, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@">
                                                    افزودن سفارش
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
");
#nullable restore
#line 92 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- News -->
    <section class=""news"">
        <div class=""container"">
            <div class=""flex-news"">
                <div class=""news-right"">
                    <h3 class=""news-h3"">در محک چه می‌گذرد؟</h3>
                    <div class=""flex-inner-news"">
");
#nullable restore
#line 108 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                         foreach (var item in Model.News)
                        {
                            

#line default
#line hidden
#nullable disable
#nullable restore
#line 110 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                             if (item.Locations == "خبر")
                            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                <div>\r\n                                    <article class=\"news-box\">\r\n                                        <a");
                BeginWriteAttribute("href", " href=\"", 5810, "\"", 5845, 4);
                WriteAttributeValue("", 5817, "/News/", 5817, 6, true);
#nullable restore
#line 114 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 5823, item.NewsId, 5823, 12, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 5835, "/", 5835, 1, true);
#nullable restore
#line 114 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 5836, item.Url, 5836, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"news-img\">\r\n                                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "7f2290431280c0a7edd856a84d319698f08af44523839", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 5945, "~/images/", 5945, 9, true);
#nullable restore
#line 115 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
AddHtmlAttributeValue("", 5954, item.ImageName, 5954, 15, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n                                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "7f2290431280c0a7edd856a84d319698f08af44525754", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n                                        </a>\r\n                                        <h2 class=\"news-h2\">\r\n                                            <a");
                BeginWriteAttribute("href", " href=\"", 6296, "\"", 6331, 4);
                WriteAttributeValue("", 6303, "/News/", 6303, 6, true);
#nullable restore
#line 119 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 6309, item.NewsId, 6309, 12, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 6321, "/", 6321, 1, true);
#nullable restore
#line 119 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
WriteAttributeValue("", 6322, item.Url, 6322, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">");
#nullable restore
#line 119 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                                                              Write(item.Title);

#line default
#line hidden
#nullable disable
                WriteLiteral("</a>\r\n                                        </h2>\r\n                                        <ul class=\"news-ul\">\r\n                                            <li>");
#nullable restore
#line 122 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                           Write(item.PublishDateTime.ConvertMiladiToShamsi("dd MMMM"));

#line default
#line hidden
#nullable disable
                WriteLiteral("</li>\r\n                                            <li>");
#nullable restore
#line 123 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                           Write(item.NumberOfVisit);

#line default
#line hidden
#nullable disable
                WriteLiteral(" بازدید</li>\r\n                                        </ul>\r\n                                        <div class=\"news-des\">\r\n                                            <p>");
#nullable restore
#line 126 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                                          Write(item.Description);

#line default
#line hidden
#nullable disable
                WriteLiteral("&#8230;</p>\r\n                                        </div>\r\n                                    </article>\r\n                                </div>\r\n");
#nullable restore
#line 130 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 130 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                             
                        }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                    </div>
                    <a href=""https://mahak-charity.org/category/about/about-about/news/news-mahak/"" class=""buttons news-button"">آرشیو اخبار</a>
                </div>
                <div class=""news-left"">
                    <div class=""animations"" id=""second-animation""></div>
                </div>
            </div>
        </div>
    </section>

    <!-- Newslwtter -->
    <section class=""newsletter"">
        <div class=""container"">
            <div class=""flex-newsletter"">
                <div class=""newsletter-box"">
                    <div class=""newsletter-form-block"">
                        <h3 class=""nf-h3"">با عضویت در خبرنامه محک، از اتفاقات زیر این سقف باخبر شوید</h3>
                        <span class=""nf-des"">برای دریافت خبرنامه، ایمیل خود را وارد کنید</span>
                        <style media=""screen"" class=""mailster-custom-form-css"">
                            .mailster-form.mailster-form-1 .mailster-wrapper label {
                       ");
                WriteLiteral("         color: #ffffff;\r\n                            }\r\n                        </style>\r\n                        ");
#nullable restore
#line 156 "C:\Other\Training\Mahak\Mahak\Views\Home\Index.cshtml"
                   Write(await Html.PartialAsync("_RegisterInNewsletter", new NewsletterViewModel()));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                    </div>
                    <div class=""talk-to-mahak"">
                        <h3 class=""nf-h3"">گفتگو با محک</h3>
                        <span class=""nf-des"">سوالات، انتقادات و پیشنهادات خود را برای ما ارسال کنید</span>
                        <a href=""https://mahak-charity.org/talk-to-mahak"" class=""buttons ttm-button"">با ما گفتگو کنید</a>
                    </div>
                </div>
                <div class=""newsletter-animation"">
                    <div class=""animations"" id=""third-animation""></div>
                </div>
            </div>
        </div>
    </section>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Mahak.ViewModel.Home.HomePageViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
