#pragma checksum "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_NewsDetails), @"mvc.1.0.view", @"/Views/Home/NewsDetails.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 2 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
using Mahak.Common.Extensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
using Mahak.ViewModel.Comments;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc2", @"/Views/Home/NewsDetails.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c8ef5b53398b6c7f8c5eca5fdeed7bc18c19ce3", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_NewsDetails : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Mahak.ViewModel.Home.NewsDetailsViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("post-title mb-2"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("90"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/img/UserPic.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("author"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
  
    ViewData["Title"] = "NewsDetails";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    .post-meta.d-flex a i {
        font-size: 15px !important;
    }
</style>

<div class=""vizew-breadcrumb"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-12"">
                <nav aria-label=""breadcrumb"">
                    <ol class=""breadcrumb  justify-content-center"">
                        <li class=""breadcrumb-item""><a href=""/""><i class=""fa fa-home"" aria-hidden=""true""></i> خانه</a></li>
                        <li class=""breadcrumb-item active"" aria-current=""page"">");
#nullable restore
#line 22 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                          Write(Model.News.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<section class=""post-details-area mb-80"">
    <div class=""container"">
        <div class=""row justify-content-center"">
            <div class=""row"">
                <div class=""col-12"">
                    <div class=""post-details-thumb mb-50"">
                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc25977", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            AddHtmlAttributeValue("", 1165, "~/newsImage/", 1165, 12, true);
#nullable restore
#line 36 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
AddHtmlAttributeValue("", 1177, Model.News.ImageName, 1177, 21, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "alt", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 36 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
AddHtmlAttributeValue("", 1205, Model.News.ImageName, 1205, 21, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                    </div>
                </div>
            </div>

            <div class=""col-12 col-lg-9 col-xl-8"">
                <div class=""post-details-content"">
                    <!-- Blog Content -->
                    <div class=""blog-content"">
                        <!-- Post Content -->
                        <div class=""post-content mt-0"">
                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc28307", async() => {
#nullable restore
#line 47 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                                   Write(Model.News.Title);

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 4, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            AddHtmlAttributeValue("", 1640, "~/News/", 1640, 7, true);
#nullable restore
#line 47 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
AddHtmlAttributeValue("", 1647, Model.News.NewsId, 1647, 18, false);

#line default
#line hidden
#nullable disable
            AddHtmlAttributeValue("", 1665, "/", 1665, 1, true);
#nullable restore
#line 47 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
AddHtmlAttributeValue("", 1666, Model.News.Url, 1666, 15, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                            <div class=\"d-flex justify-content-between mb-30\">\r\n                                <div class=\"post-meta d-flex align-items-center\">\r\n                                    <a href=\"#\" class=\"post-author\">");
#nullable restore
#line 50 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                               Write(Model.News.AuthorInfo.FirstName);

#line default
#line hidden
#nullable disable
            WriteLiteral("  ");
#nullable restore
#line 50 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                                 Write(Model.News.AuthorInfo.LastName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                    <i class=\"fa fa-circle\" aria-hidden=\"true\"></i>\r\n                                    <a href=\"#\" class=\"post-date\"> ");
#nullable restore
#line 52 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                              Write(Model.News.PublishDateTime.ConvertMiladiToShamsi("dd MMMM yyyy ساعت HH:MM"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                </div>\r\n                                <div class=\"post-meta d-flex\">\r\n                                    <a href=\"#\"><i class=\"fa fa-comments-o\" aria-hidden=\"true\"></i> ");
#nullable restore
#line 55 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                               Write(Model.News.NumberOfComments);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                    <a href=\"#\"><i class=\"fa fa-eye\" aria-hidden=\"true\"></i> ");
#nullable restore
#line 56 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                        Write(Model.News.NumberOfVisit);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                    <a");
            BeginWriteAttribute("onclick", " onclick=\"", 2666, "\"", 2706, 3);
            WriteAttributeValue("", 2676, "Bookmark(\'", 2676, 10, true);
#nullable restore
#line 57 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 2686, Model.News.NewsId, 2686, 18, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2704, "\')", 2704, 2, true);
            EndWriteAttribute();
            WriteLiteral(" id=\"bookmark\">\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                        ");
#nullable restore
#line 63 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                   Write(Html.Raw(Model.News.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n                        <!-- Post Author -->\r\n                        <div class=\"vizew-post-author d-flex align-items-center py-2\">\r\n                            <div class=\"post-author-thumb\">\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc213874", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            AddHtmlAttributeValue("", 3174, "~/avatars/", 3174, 10, true);
#nullable restore
#line 68 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
AddHtmlAttributeValue("", 3184, Model.News.AuthorInfo.Image, 3184, 28, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "alt", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 68 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
AddHtmlAttributeValue("", 3219, Model.News.AuthorInfo.Image, 3219, 28, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                            </div>\r\n                            <div class=\"post-author-desc pr-4\">\r\n                                <h6 href=\"#\" class=\"author-name\"> نویسنده : ");
#nullable restore
#line 71 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                       Write(Model.News.AuthorInfo.FirstName);

#line default
#line hidden
#nullable disable
            WriteLiteral("  ");
#nullable restore
#line 71 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                                         Write(Model.News.AuthorInfo.LastName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n                                <p>");
#nullable restore
#line 72 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                              Write(Model.News.AuthorInfo.Bio);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                            </div>
                        </div>

                        <!-- Comment Area Start -->
                        <div class=""comment_area clearfix mb-50"">

                            <!-- Section Title -->
                            <div class=""section-heading style-2"">
                                <h4>نظرات</h4>
                                <div class=""line""></div>
                            </div>
");
#nullable restore
#line 84 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                             if (Model.Comments.Count() != 0)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <ul>\r\n");
#nullable restore
#line 87 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                     foreach (var item in Model.Comments)
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <li class=\"single_comment_area\">\r\n                                            <!-- Comment Content -->\r\n                                            <div");
            BeginWriteAttribute("id", " id=\"", 4470, "\"", 4503, 1);
#nullable restore
#line 91 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 4475, "comment-"+item.CommentId, 4475, 28, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"comment-content d-flex\">\r\n                                                <!-- Comment Author -->\r\n                                                <div class=\"comment-author\">\r\n                                                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc218819", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                                                </div>
                                                <!-- Comment Meta -->
                                                <div class=""comment-meta"">
                                                    <a href=""#"" class=""comment-date"">");
#nullable restore
#line 98 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                Write(item.PostageDateTime.ConvertMiladiToShamsi("dd MMMM yyyy ساعت HH:mm"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                                    <h6>");
#nullable restore
#line 99 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                   Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n                                                    <p>");
#nullable restore
#line 100 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                  Write(item.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                                    <div class=\"d-flex align-items-center\">\r\n                                                        <a");
            BeginWriteAttribute("id", " id=\"", 5458, "\"", 5487, 1);
#nullable restore
#line 102 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 5463, "btn-"+item.CommentId, 5463, 24, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("onclick", " onclick=\"", 5488, "\"", 5547, 5);
            WriteAttributeValue("", 5498, "ShowCommentForm(\'", 5498, 17, true);
#nullable restore
#line 102 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 5515, item.CommentId, 5515, 15, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 5530, "\',\'", 5530, 3, true);
#nullable restore
#line 102 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 5533, item.NewsId, 5533, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 5545, "\')", 5545, 2, true);
            EndWriteAttribute();
            WriteLiteral(" href=\"#\" class=\"reply\">پاسخ</a>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n\r\n");
#nullable restore
#line 107 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                             if (item.comments.Count() != 0)
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <ol class=\"children\">\r\n");
#nullable restore
#line 110 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                     foreach (var sub in item.comments)
                                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                        <li class=\"single_comment_area\">\r\n                                                            <div");
            BeginWriteAttribute("id", " id=\"", 6246, "\"", 6278, 1);
#nullable restore
#line 113 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 6251, "comment-"+sub.CommentId, 6251, 27, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"comment-content d-flex\">\r\n                                                                <div class=\"comment-author\">\r\n                                                                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "e2a8cdb84d6f3c04d6be2e1187f9dab71a0cafc224055", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                                                                </div>
                                                                <div class=""comment-meta"">
                                                                    <a href=""#"" class=""comment-date"">");
#nullable restore
#line 118 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                                Write(sub.PostageDateTime.ConvertMiladiToShamsi("dd MMMM yyyy ساعت HH:mm"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                                                    <h6>");
#nullable restore
#line 119 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                   Write(sub.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n                                                                    <p>");
#nullable restore
#line 120 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                  Write(sub.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                                                    <div class=\"d-flex align-items-center\">\r\n                                                                        <a");
            BeginWriteAttribute("id", " id=\"", 7230, "\"", 7258, 1);
#nullable restore
#line 122 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 7235, "btn-"+sub.CommentId, 7235, 23, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" href=\"#\"");
            BeginWriteAttribute("onclick", " onclick=\"", 7268, "\"", 7326, 5);
            WriteAttributeValue("", 7278, "ShowCommentForm(\'", 7278, 17, true);
#nullable restore
#line 122 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 7295, sub.CommentId, 7295, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 7309, "\',\'", 7309, 3, true);
#nullable restore
#line 122 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
WriteAttributeValue("", 7312, item.NewsId, 7312, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 7324, "\')", 7324, 2, true);
            EndWriteAttribute();
            WriteLiteral(" class=\"reply\">پاسخ</a>\r\n                                                                    </div>\r\n                                                                </div>\r\n                                                            </div>\r\n");
#nullable restore
#line 126 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                             if (sub.comments.Count() != 0)
                                                            {
                                                                

#line default
#line hidden
#nullable disable
#nullable restore
#line 128 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                           Write(await Html.PartialAsync("_SubComments", sub.comments));

#line default
#line hidden
#nullable disable
#nullable restore
#line 128 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                                                                                      
                                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                        </li>\r\n");
#nullable restore
#line 131 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                </ol>\r\n");
#nullable restore
#line 133 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        </li>\r\n");
#nullable restore
#line 135 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                </ul>\r\n");
#nullable restore
#line 137 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <p class=\"alert alert-info mb-0\">دیدگاهی برای این خبر ارسال نشده است.</p>\r\n");
#nullable restore
#line 141 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        </div>

                        <!-- Post A Comment Area -->
                        <div class=""post-a-comment-area"">

                            <!-- Section Title -->
                            <div class=""section-heading style-2"">
                                <h4>ارسال دیدگاه</h4>
                                <div class=""line""></div>
                            </div>

                            <!-- Reply Form -->
                            ");
#nullable restore
#line 154 "C:\Other\Training\Mahak\Mahak\Views\Home\NewsDetails.cshtml"
                       Write(await Html.PartialAsync("_SendComment", new CommentViewModel(null, Model.News.NewsId, null)));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Mahak.ViewModel.Home.NewsDetailsViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
