﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Mahak.Entities;
using Mahak.Entities.Identity;
using Mahak.Data.Contracts;
using Mahak.Common.Extensions;
using Mahak.Services.Contracts;
using Mahak.ViewModel.Home;
using Mahak.ViewModel.Order;
using Mahak.ViewModel.Customer;
using Mahak.ViewModel.UserManager;

namespace Mahak.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _uw;
        private readonly IHttpContextAccessor _accessor;
        private readonly IWebHostEnvironment _env;
        private readonly IMapper _mapper;
        private readonly IApplicationUserManager _userManager;
        public HomeController(IUnitOfWork uw, IHttpContextAccessor accessor, IWebHostEnvironment env, IMapper mapper, IApplicationUserManager userManager)
        {
            _uw = uw;
            _accessor = accessor;
            _env = env;
            _mapper = mapper;
            _userManager = userManager;
        }


        public async Task<IActionResult> Index()
        {
            User model = new User();
            UsersViewModel viewModel = new UsersViewModel();
            bool requiresTwoFactor = false;

            int userId = User.Identity.GetUserId<int>();
            if (userId != 0)
            {
                viewModel = await _userManager.FindUserWithRolesByIdAsync(userId);
                model = await _userManager.FindByIdAsync(userId.ToString());
                viewModel.CheckTwoFactor = model.TwoFactorEnabled;
            }
            else if (viewModel != null && viewModel.TwoFactorEnabled == true)
                requiresTwoFactor = true;
            else if (userId == 0)
                return RedirectToAction("SignIn", "Account");

            var news = await _uw.NewsRepository.GetPaginateNewsAsync(0, 10, "PublishDateTime desc", "", true, null);
            var mostViewedNews = await _uw.NewsRepository.MostViewedNewsAsync(0, 3, "day");
            var products = await _uw.ProductRepository.GetPaginateProductAsync(0, 10, "ProductName desc", "");
            var sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(0, 10, "Title desc", "");

            var homePageViewModel = new HomePageViewModel(sliders, model, news, products, requiresTwoFactor);
            return View(homePageViewModel);
        }


        [Route("Product/{productId}/{categoryId}")]
        public async Task<IActionResult> ProductDetails(string productId, string categoryId)
        {
            if (!productId.HasValue())
                return NotFound();
            else
            {
                int userId = User.Identity.GetUserId<int>();
                var Product = await _uw.BaseRepository<Product>().FindByIdAsync(productId);

                if (Product == null)
                    return NotFound();
                else
                {
                    var category = await _uw.CategoryRepository.GetCategoryTitleByIdAsync(categoryId);
                    var product = await _uw.ProductRepository.GetProductByIdAsync(productId, userId);
                    var productComments = await _uw.ProductRepository.GetProductCommentsAsync(productId);
                    var productDetailsViewModel = new ProductDetailsViewModel(0, 0, product, null, category, null, null, productComments, null, null);

                    return View(productDetailsViewModel);
                }
            }
        }


        [Route("/product-category/{categoryId}")]
        public async Task<IActionResult> ProductList(string categoryId, int limit = 10, int offset = 0)
        {
            if (!categoryId.HasValue())
                return NotFound();
            else
            {
                var category = await _uw.BaseRepository<Category>().FindByIdAsync(categoryId);

                if (category == null)
                    return NotFound();
                else
                {
                    int userId = User.Identity.GetUserId<int>();
                    var order = _uw.ProductRepository.GetOrderIdByCustomerId(userId);

                    int categoryCount = _uw.CategoryRepository.CountCategories();
                    var orderviewModel = await _uw.ProductRepository.GetOrderByIdAsync(order.OrderId);
                    orderviewModel.CustomerId = userId;
                    var viewModel = await _uw.CategoryRepository.GetCategoryByIdAsync(categoryId);
                    var viewModels = await _uw.CategoryRepository.GetParentInfoAsync(0, 10, "ParentInfo.CategoryName desc");
                    var sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(0, 10, "Title desc", "");
                    var products = await _uw.ProductRepository.GetPaginateProductAsync(0, 10, "ProductName desc", "");
                    var productsviewModel = await _uw.ProductRepository.GetProductInCategoryAsync(categoryId, offset, limit);
                    var categoryDetailsViewModel = new ProductDetailsViewModel(categoryCount, 0, null, products, viewModel, viewModels, orderviewModel, null, sliders, productsviewModel);

                    return View(categoryDetailsViewModel);
                }
            }
        }


        [Route("Product/{productId}")]
        public async Task<IActionResult> Cart(string productId, int quantity)
        {
            List<CustomerViewModel> customers = new List<CustomerViewModel>();
            OrderViewModel viewModel = new OrderViewModel();
            List<OrderViewModel> orderViewModels = new List<OrderViewModel>();
            Order model = new Order();

            if (!productId.HasValue())
                return NotFound();
            else
            {
                int userId = User.Identity.GetUserId<int>();

                var Product = await _uw.BaseRepository<Product>().FindByIdAsync(productId);
                if (Product == null)
                    return NotFound();
                else
                {
                    int orders = _uw.ProductRepository.GetOrderByCustomerId(userId);
                    if (orders == 0)
                    {
                        viewModel.CustomerId = userId;
                        viewModel.OrderId = StringExtensions.GenerateId(10);
                        viewModel.DispatchNumber = StringExtensions.GenerateId(8);
                        await _uw.BaseRepository<Order>().CreateAsync(_mapper.Map<Order>(viewModel));
                        await _uw.Commit();
                    }
                    else
                    {
                        var order = _uw.ProductRepository.GetOrderIdByCustomerId(userId);

                        orderViewModels = await _uw.ProductRepository.GetPaginateOrdersAsync(0, 10, "OrderStatus");
                        viewModel.ProductId = Product.ProductId;
                        viewModel.ProductName = Product.ProductName;
                        viewModel.ProductImageName = Product.ImageName;
                        viewModel.ProductCode = Product.ProductCode;
                        viewModel.ProductUrl = Product.Url;
                        viewModel.ProductPrice = Product.Price;

                        viewModel.CustomerId = userId;
                        _uw.BaseRepository<Order>().Update(_mapper.Map(viewModel, model));
                        await _uw.Commit();
                    }

                    var product = await _uw.ProductRepository.GetProductByIdAsync(productId, userId);
                    var productDetailsViewModel = new ProductDetailsViewModel(product, viewModel, orderViewModels);

                    return PartialView("_ShoppingCart", productDetailsViewModel);
                }
            }
        }


        [HttpGet, Route("Product/{productId}/{orderId?}")]
        public async Task<IActionResult> Cart(string productId, string orderId)
        {
            OrderViewModel viewModel = new OrderViewModel();
            List<OrderViewModel> orderViewModels = new List<OrderViewModel>();

            if (!productId.HasValue())
                return NotFound();
            else
            {
                int userId = User.Identity.GetUserId<int>();
                var product = await _uw.BaseRepository<Product>().FindByIdAsync(productId);
                if (product == null)
                    return NotFound();
                else
                {
                    int orders = _uw.ProductRepository.GetOrderByCustomerId(userId);
                    if (orders == 0)
                    {
                        viewModel.CustomerId = userId;
                        viewModel.OrderId = StringExtensions.GenerateId(10);
                        viewModel.DispatchNumber = StringExtensions.GenerateId(8);
                        viewModel.PurchaseDateTime = DateTime.Now;
                        await _uw.BaseRepository<Order>().CreateAsync(_mapper.Map<Order>(viewModel));
                        await _uw.Commit();
                    }
                    else
                    {
                        var order = _uw.ProductRepository.GetOrderIdByCustomerId(userId);
                        if (viewModel.ProductId != productId)
                        {
                            //viewModel.Quantity++;

                            viewModel.CustomerId = userId;
                            viewModel.OrderId = StringExtensions.GenerateId(10);
                            viewModel.DispatchNumber = StringExtensions.GenerateId(8);
                            viewModel.PurchaseDateTime = DateTime.Now;
                            await _uw.BaseRepository<Order>().CreateAsync(_mapper.Map<Order>(viewModel));
                            await _uw.Commit();
                        }
                        else
                        {
                            viewModel.OrderId = order.OrderId;

                            orderViewModels = await (from n in _uw.Context.Orders
                                                     join c in _uw.Context.Customers on n.CustomerId equals c.CustomerId into nc
                                                     from nct in nc.DefaultIfEmpty()
                                                     join o in _uw.Context.ProductOrders on n.OrderId equals o.OrderId into po
                                                     from pot in po.DefaultIfEmpty()
                                                     join p in _uw.Context.Products on pot.ProductId equals p.ProductId into op
                                                     from opt in op.DefaultIfEmpty()
                                                     where (n.OrderId == orderId)
                                                     select new OrderViewModel
                                                     {
                                                         OrderId = n.OrderId,
                                                         AmountPaid = n.AmountPaid,
                                                         Quantity = n.Quantity,
                                                         DispatchNumber = n.DispatchNumber,
                                                         OrderStatus = n.OrderStatus,
                                                         PurchaseDateTime = n.PurchaseDateTime,
                                                         Calculate = n.AmountPaid * n.Quantity,
                                                         CustomerName = nct.FirstName1 + " " + nct.LastName1,
                                                     }).ToListAsync();


                            if (orderViewModels != null)
                                viewModel = _mapper.Map<OrderViewModel>(orderViewModels.FirstOrDefault());

                            viewModel.ProductId = product.ProductId;
                            viewModel.ProductName = product.ProductName;
                            viewModel.ProductImageName = product.ImageName;
                            viewModel.ProductCode = product.ProductCode;
                            viewModel.ProductUrl = product.Url;
                            viewModel.ProductPrice = product.Price;

                            viewModel.CustomerId = userId;
                            _uw.BaseRepository<Order>().Update(_mapper.Map(viewModel, order));
                            await _uw.Commit();
                        }
                    }

                    orderViewModels = await _uw.ProductRepository.GetPaginateOrdersAsync(0, 10, "OrderStatus");
                    var productViewModel = await _uw.ProductRepository.GetProductByIdAsync(productId, userId);
                    var productDetailsViewModel = new ProductDetailsViewModel(productViewModel, viewModel, orderViewModels);

                    return PartialView("_ShoppingCart", productDetailsViewModel);
                }
            }
        }


        [HttpPost]
#pragma warning disable MVC1004 // Rename model bound parameter.
        public async Task<IActionResult> DoCart(ProductDetailsViewModel viewModels)
#pragma warning restore MVC1004 // Rename model bound parameter.
        {
            OrderViewModel viewModel = new OrderViewModel();
            int userId = User.Identity.GetUserId<int>();
            viewModels.Order.CustomerId = userId;

            if (!ModelState.IsValid)
            {
                Order model = new Order();
                model = await _uw.BaseRepository<Order>().FindByIdAsync(viewModels.Order.OrderId);
                var product = await _uw.BaseRepository<Product>().FindByIdAsync(viewModels.Product.ProductId);

                model.OrderId = viewModels.Order.OrderId;
                model.CustomerId = viewModels.Order.CustomerId;
                model.AmountPaid = viewModels.Order.AmountPaid;
                model.OrderStatus = "در انتظار پرداخت";
                model.PurchaseDateTime = DateTime.Now;
                model.Quantity = viewModels.Order.Quantity;
                ////////////////////////////
                viewModels.Order.ProductName = product.ProductName;
                viewModels.Order.ProductImageName = product.ImageName;
                viewModels.Order.ProductCode = product.ProductCode;
                viewModels.Order.ProductUrl = product.Url;
                viewModels.Order.ProductPrice = product.Price;

                _uw.BaseRepository<Order>().Update(model);
                await _uw.Commit();
            }

            return PartialView("_ShoppingCart", viewModels);
        }


        [Route("Order/{orderId}")]
        public async Task<IActionResult> DeleteCart(string orderId)
        {
            if (!orderId.HasValue())
                return NotFound();
            else
            {
                var order = await _uw.BaseRepository<Order>().FindByIdAsync(orderId);
                if (order.OrderId == null)
                    return NotFound();
                else
                {
                    _uw.BaseRepository<Order>().Delete(order);
                    await _uw.Commit();

                    var productDetailsViewModel = new ProductDetailsViewModel(order);
                    return PartialView("_ShoppingCart", productDetailsViewModel);
                }
            }
        }


        [HttpGet]
        public async Task<IActionResult> GetCategoriesPaginate()
        {
            int categoryCount = _uw.CategoryRepository.CountCategories();
            var categories = await _uw.CategoryRepository.GetPaginateCategoriesAsync(0, 10, "CategoryInfo.CategoryName desc", "");
            var sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(0, 10, "Title desc", "");
            return PartialView("_CategoryPaginate", new ProductDetailsViewModel(categoryCount, 0, null, null, null, categories, null, null, sliders, null));
        }


        [Route("News/{location}/{newsId?}/{url?}")]
        public async Task<IActionResult> NewsDetails(TextLocation location, string newsId)
        {
            //if (!newsId.HasValue())
            //    return NotFound();
            //else
            //{
                int userId = User.Identity.GetUserId<int>();
                var existNews = await _uw.BaseRepository<News>().FindByIdAsync(newsId);

                //if (existNews == null)
                //    return NotFound();
                //else
                //{
                    string ipAddress = _accessor.HttpContext?.Connection?.RemoteIpAddress.ToString();
                    await _uw.NewsRepository.InsertVisitOfUserAsync(newsId, ipAddress);
                    var news = await _uw.NewsRepository.GetNewsByLocationAsync(location);
                    var newsComments = await _uw.NewsRepository.GetNewsCommentsAsync(newsId);

                    var newsDetailsViewModel = new NewsDetailsViewModel(news, newsComments);
                    return View(newsDetailsViewModel);
                //}
            //}
        }


        [HttpGet]
        public IActionResult Error()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Error404()
        {
            return View();
        }
    }
}
