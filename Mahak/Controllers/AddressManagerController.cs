﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Mahak.Entities.Identity;
using Mahak.Common.Extensions;
using Mahak.Services.Identity;
using Mahak.Services.Contracts;
using Mahak.ViewModel.Customer;

namespace Mahak.Controllers
{
    public class AddressManagerController : Controller
    {
        private readonly IUnitOfWork _uw;
        private readonly IHttpContextAccessor _accessor;
        private readonly IApplicationUserManager _userManager;
        private readonly IApplicationRoleManager _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly IMapper _mapper;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger<AccountController> _logger;
        private readonly ApplicationIdentityErrorDescriber _errorDescriber;
        public AddressManagerController(IUnitOfWork uw, IHttpContextAccessor accessor, IApplicationUserManager userManager, IApplicationRoleManager roleManager, IEmailSender emailSender, ISmsSender smsSender, IMapper mapper, SignInManager<User> signInManager, ILogger<AccountController> logger, ApplicationIdentityErrorDescriber errorDescriber)
        {
            _uw = uw;
            _accessor = accessor;
            _userManager = userManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _mapper = mapper;
            _signInManager = signInManager;
            _logger = logger;
            _errorDescriber = errorDescriber;
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> AddressManager(int userId)
        {
            Customer model = new Customer();
            CustomerViewModel viewModel = new CustomerViewModel();
            viewModel.CustomerId = userId;

            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return NotFound();

            if (user == null)
                return NotFound();
            else
            {
                model = await _uw.BaseRepository<Customer>().FindByIdAsync(viewModel.CustomerId);
                if (model == null)
                {
                    viewModel = new CustomerViewModel
                    {
                        CustomerId = user.Id,
                        FirstName1 = user.FirstName,
                        LastName1 = user.LastName,
                        PhoneNumber1 = user.PhoneNumber,
                        Email = user.Email,
                    };

                    await _uw.BaseRepository<Customer>().CreateAsync(_mapper.Map<Customer>(viewModel));
                    await _uw.Commit();
                    if (_uw.Commit().IsCompletedSuccessfully)
                        ViewBag.Alert1 = user.FirstName + " " + user.LastName;

                    return PartialView("_AddressManager", viewModel);
                }

                if (model.FirstName1 != "")
                {
                    viewModel.CustomerId = model.CustomerId;
                    viewModel.FirstName1 = model.FirstName1;
                    viewModel.LastName1 = model.LastName1;
                    viewModel.PhoneNumber1 = model.PhoneNumber1;
                    viewModel.Email = model.Email;
                    viewModel.Company1 = model.Company1;
                    viewModel.Address1 = model.Address1;
                    viewModel.Province1 = model.Province1;
                    viewModel.City1 = model.City1;
                    viewModel.PostalCode1 = model.PostalCode1;
                }
                else
                    ViewBag.Alert1 = user.FirstName + " " + user.LastName;

                if (model.FirstName2 != null || model.LastName2 != null)
                {
                    viewModel.CustomerId = model.CustomerId;
                    viewModel.FirstName2 = model.FirstName2;
                    viewModel.LastName2 = model.LastName2;
                    viewModel.PhoneNumber2 = model.PhoneNumber2;
                    viewModel.Company2 = model.Company2;
                    viewModel.Address2 = model.Address2;
                    viewModel.ContinueAddress = model.ContinueAddress;
                    viewModel.Province2 = model.Province2;
                    viewModel.City2 = model.City2;
                    viewModel.PostalCode2 = model.PostalCode2;
                }
                else
                    ViewBag.Alert2 = "شما هنوز آدرسی ثبت نکرده اید.";
            }

            return PartialView("_AddressManager", viewModel);
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> EditAddress(int userId)
        {
            Customer model = new Customer();
            CustomerViewModel viewModel = new CustomerViewModel();
            viewModel.CustomerId = userId;
            var user = await _userManager.FindByIdAsync(userId.ToString());

            if (user == null)
                return NotFound();
            else
            {
                model = await _uw.BaseRepository<Customer>().FindByIdAsync(viewModel.CustomerId);
                if (model == null)
                {
                    viewModel = new CustomerViewModel
                    {
                        CustomerId = user.Id,
                        FirstName1 = user.FirstName,
                        LastName1 = user.LastName,
                        PhoneNumber1 = user.PhoneNumber,
                        Email = user.Email,
                    };

                    await _uw.BaseRepository<Customer>().CreateAsync(_mapper.Map<Customer>(viewModel));
                    await _uw.Commit();
                }
                else
                {
                    var customers = await (from n in _uw.Context.Customers
                                           join w in _uw.Context.Users on n.CustomerId equals w.Id into bc
                                           from bct in bc.DefaultIfEmpty()
                                           where (n.CustomerId == userId)
                                           select new CustomerViewModel
                                           {
                                               CustomerId = n.CustomerId,
                                               FirstName1 = n.FirstName1,
                                               LastName1 = n.LastName1,
                                               PhoneNumber1 = n.PhoneNumber1,
                                               Email = n.Email,
                                               Address1 = n.Address1,
                                               PostalCode1 = n.PostalCode1,
                                               Company1 = n.Company1,
                                               Province1 = n.Province1,
                                               City1 = n.City1,
                                           }).ToListAsync();

                    if (customers != null)
                        viewModel = _mapper.Map<CustomerViewModel>(customers.FirstOrDefault());
                }
            }

            return PartialView("_EditAddress", viewModel);
        }


        [HttpPost, Authorize]
        public async Task<IActionResult> EditAddress(CustomerViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Customer model = new Customer();
                model = await _uw.BaseRepository<Customer>().FindByIdAsync(viewModel.CustomerId);
                if (!model.Address1.HasValue() || !model.Company1.HasValue() || !model.PostalCode1.HasValue() || !model.City1.HasValue() || model.Province1 == null)
                {
                    model.CustomerId = viewModel.CustomerId;
                    model.FirstName1 = viewModel.FirstName1;
                    model.LastName1 = viewModel.LastName1;
                    model.Company1 = viewModel.Company1;
                    model.Address1 = viewModel.Address1;
                    model.Province1 = viewModel.Province1;
                    model.City1 = viewModel.City1;
                    model.PostalCode1 = viewModel.PostalCode1;
                    model.PhoneNumber1 = viewModel.PhoneNumber1;
                    model.Email = viewModel.Email;
                    model.City1 = viewModel.City1;
                    model.Province1 = viewModel.Province1;

                    _uw.BaseRepository<Customer>().Update(model);
                    await _uw.Commit();
                    ViewBag.Alert = "تغییر اطلاعات با موفقیت انجام شد.";
                }
            }

            return PartialView("_EditAddress", viewModel);
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> AddAddress(int userId)
        {
            CustomerViewModel viewModel = new CustomerViewModel();
            viewModel.CustomerId = userId;
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return NotFound();
            else
            {
                Customer model = new Customer();
                model = await _uw.BaseRepository<Customer>().FindByIdAsync(viewModel.CustomerId);
                if (model == null)
                    viewModel = new CustomerViewModel { CustomerId = user.Id };
                else
                {
                    var customers = await (from n in _uw.Context.Customers
                                           join w in _uw.Context.Users on n.CustomerId equals w.Id into bc
                                           from bct in bc.DefaultIfEmpty()
                                           where (n.CustomerId == userId)
                                           select new CustomerViewModel
                                           {
                                               CustomerId = n.CustomerId,
                                               FirstName2 = n.FirstName2,
                                               LastName2 = n.LastName2,
                                               PhoneNumber2 = n.PhoneNumber2,
                                               Address2 = n.Address2,
                                               ContinueAddress = n.ContinueAddress,
                                               PostalCode2 = n.PostalCode2,
                                               Company2 = n.Company2,
                                           }).ToListAsync();

                    if (customers != null)
                        viewModel = _mapper.Map<CustomerViewModel>(customers.FirstOrDefault());
                }
            }

            return PartialView("_AddAddress", viewModel);
        }


        [HttpPost, Authorize]
        public async Task<IActionResult> AddAddress(CustomerViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Customer model = new Customer();
                model = await _uw.BaseRepository<Customer>().FindByIdAsync(viewModel.CustomerId);
                if (!model.Address2.HasValue() || !model.Company2.HasValue() || !model.PostalCode2.HasValue() || !model.City2.HasValue() || !model.Province2.HasValue())
                {
                    model.CustomerId = viewModel.CustomerId;
                    model.FirstName2 = viewModel.FirstName2;
                    model.LastName2 = viewModel.LastName2;
                    model.Company2 = viewModel.Company2;
                    model.Address2 = viewModel.Address2;
                    model.ContinueAddress = viewModel.ContinueAddress;
                    model.PostalCode2 = viewModel.PostalCode2;
                    model.Province2 = viewModel.Province2;
                    model.City2 = viewModel.City2;

                    _uw.BaseRepository<Customer>().Update(model);
                    await _uw.Commit();
                    ViewBag.Alert = "ثبت اطلاعات با موفقیت انجام شد.";
                }
            }

            return PartialView("_AddAddress", viewModel);
        }
    }
}
