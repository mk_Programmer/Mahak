﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Mahak.Data.Contracts;
using Mahak.ViewModel.Account;
using Mahak.Entities.Identity;
using Mahak.Common.Extensions;
using Mahak.Services.Contracts;
using Mahak.Services.Identity;
using static Mahak.ViewModel.Account.UserPanelViewModel;
using Mahak.Entities;
using System.Text.Encodings.Web;

namespace Mahak.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUnitOfWork _uw;
        private readonly IHttpContextAccessor _accessor;
        private readonly IApplicationUserManager _userManager;
        private readonly IApplicationRoleManager _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly IMapper _mapper;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger<AccountController> _logger;
        private readonly ApplicationIdentityErrorDescriber _errorDescriber;
        public AccountController(IUnitOfWork uw, IHttpContextAccessor accessor, IApplicationUserManager userManager, IApplicationRoleManager roleManager, IEmailSender emailSender, ISmsSender smsSender, IMapper mapper, SignInManager<User> signInManager, ILogger<AccountController> logger, ApplicationIdentityErrorDescriber errorDescriber)
        {
            _uw = uw;
            _accessor = accessor;
            _userManager = userManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _mapper = mapper;
            _signInManager = signInManager;
            _logger = logger;
            _errorDescriber = errorDescriber;
        }


        [HttpGet]
        public IActionResult SignIn()
        {
            return PartialView("_SignIn");
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> SignIn(SignInViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(viewModel.UserName);
                if (user != null)
                {
                    if (user.IsActive)
                    {
                        var result = await _signInManager.PasswordSignInAsync(viewModel.UserName, viewModel.Password, viewModel.RememberMe, true);
                        if (result.Succeeded)
                            return RedirectToAction("Profile", "Account");
                        if (result.IsLockedOut)
                        {
                            ModelState.AddModelError(string.Empty, "حساب کاربری شما به مدت 20 دقیقه به دلیل تلاش های ناموفق قفل شد.");
                            return View();
                        }
                        if (result.RequiresTwoFactor)
                            return RedirectToAction("SendCode", "Code", new { RememberMe = viewModel.RememberMe });
                    }
                    else
                        ModelState.AddModelError(string.Empty, "حساب کاربری شما غیرفعال است.");
                }
                else
                {
                    _logger.LogWarning($"The user attempts to login with the IP address({_accessor.HttpContext?.Connection?.RemoteIpAddress.ToString()}) and username ({viewModel.UserName}) and password ({viewModel.Password}).");
                    ModelState.AddModelError(string.Empty, "نام کاربری یا کلمه عبور شما صحیح نمی باشد.");
                }
            }

            return PartialView("_SignIn");
        }


        [HttpGet]
        public IActionResult Register()
        {
            return PartialView("_Register");
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                User user = new User { UserName = ViewModel.UserName, FirstName = ViewModel.FirstName, LastName = ViewModel.LastName, Email = ViewModel.Email, RegisterDateTime = DateTime.Now, IsActive = true };
                ViewModel.PhoneNumber = ViewModel.UserName;
                IdentityResult result = await _userManager.CreateAsync(user, ViewModel.Password);

                if (result.Succeeded)
                {
                    var role = await _roleManager.FindByNameAsync("کاربر");

                    if (role == null)
                        await _roleManager.CreateAsync(new Role("کاربر"));

                    result = await _userManager.AddToRoleAsync(user, "کاربر");

                    if (result.Succeeded)
                    {
                        user.PhoneNumberConfirmed = true;
                        user.LockoutEnabled = false;
                        user.EmailConfirmed = true;
                        user.TwoFactorEnabled = true;
                        await _userManager.UpdateAsync(user);

                        return RedirectToAction("SignIn", "Account");
                    }
                }

                foreach (var item in result.Errors)
                    ModelState.AddModelError(string.Empty, item.Description);
            }

            return PartialView("_Register");
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> SignOut(SignInViewModel viewModel)
        {
            viewModel.UserId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindClaimsInUser(viewModel.UserId);

            await _userManager.UpdateAsync(user);

            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public IActionResult ForgetPassword()
        {
            return View();
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgetPassword(ResetPasswordViewModel ViewModel)
        {
            if (!ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(ViewModel.Email);
                if (user == null)
                    ModelState.AddModelError(string.Empty, "ایمیل شما صحیح نمی باشد.");
                else
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                        ModelState.AddModelError(string.Empty, "لطفا با تایید ایمیل حساب کاربری خود را فعال کنید.");
                    else
                    {
                        var Code = await _userManager.GeneratePasswordResetTokenAsync(user);
                        var CallbackUrl = Url.Action("ResetPassword", "Account", values: new { Code }, protocol: Request.Scheme);
                        await _emailSender.SendEmailAsync(ViewModel.Email, "بازیابی کلمه عبور", $"<p style='font-family:tahoma;font-size:14px'> برای بازنشانی کلمه عبور خود <a href='{HtmlEncoder.Default.Encode(CallbackUrl)}'>اینجا کلیک کنید</a> </p>");

                        return RedirectToAction("ForgetPasswordConfirmation");
                    }
                }
            }

            return View(ViewModel);
        }


        [HttpGet]
        public IActionResult ForgetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        public IActionResult ResetPassword(string Code = null)
        {
            if (Code == null)
                return NotFound();
            else
            {
                var ViewModel = new ResetPasswordViewModel { Code = Code };
                return View(ViewModel);
            }
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                var User = await _userManager.FindByEmailAsync(ViewModel.Email);
                if (User == null)
                    ModelState.AddModelError(string.Empty, "ایمیل شما صحیح نمی باشد.");
                else
                {
                    var Result = await _userManager.ResetPasswordAsync(User, ViewModel.Code, ViewModel.NewPassword);
                    if (Result.Succeeded)
                        return RedirectToAction("ResetPasswordConfirmation");
                    else
                    {
                        foreach (var error in Result.Errors)
                            ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return View(ViewModel);
        }


        [HttpGet]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> Profile()
        {
            UserPanelViewModel viewModel = new UserPanelViewModel();

            int userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindByIdAsync(userId.ToString());
            viewModel.CheckTwoFactorAccount = user.TwoFactorEnabled;

            return View(new UserPanelViewModel(user));
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> SendMessage()
        {
            int userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindUserWithRolesByIdAsync(userId);

            MessageUsersViewModel viewModel = new MessageUsersViewModel();
            viewModel.UserId = userId;
            viewModel.FullName = user.FirstName + " " + user.LastName;
            viewModel.PhoneNumber = user.PhoneNumber;
            viewModel.Email = user.Email;

            if (user == null)
                return NotFound();
            else
                return PartialView("_SendMessage", viewModel);
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> SendMessage(MessageUsersViewModel viewModel)
        {
            int userId = User.Identity.GetUserId<int>();
            MessageUsers model = new MessageUsers();

            viewModel = new MessageUsersViewModel
            {
                UserId = userId,
                Email = viewModel.Email,
                Description = viewModel.Description,
                MessageTypes = viewModel.MessageTypes,
                IsRegisterCode = false,
            };

            viewModel.MessageId = StringExtensions.GenerateId(10);
            await _uw.BaseRepository<MessageUsers>().CreateAsync(_mapper.Map(viewModel, model));
            await _uw.Commit();
            if (_uw.Commit().IsCompletedSuccessfully)
                ViewBag.Alert = "ثبت پیام شما با موفقیت انجام شد.";

            return RedirectToAction("MessageCode", "Code", viewModel);
        }


        [HttpGet, Authorize]
        public IActionResult Hamdeli()
        {
            return PartialView("_Hamdeli");
        }


        [HttpGet, Authorize]
        public IActionResult Tabrik()
        {
            return PartialView("_Tabrik");
        }


        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Tabrik(UserPanelViewModel viewModel)
        {
            return PartialView("_Tabrik");
        }


        [HttpGet, Authorize]
        public IActionResult Tasliat()
        {
            return PartialView("_Tasliat");
        }


        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Tasliat(UserPanelViewModel viewModel)
        {
            return PartialView("_Tasliat");
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> OnlineHelp()
        {
            int userId = User.Identity.GetUserId<int>();

            var user = await _userManager.FindUserWithRolesByIdAsync(userId);

            if (user == null)
                return NotFound();
            else
                return PartialView("_OnlineHelp", user);
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> RegisteryRequest()
        {
            int userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindUserWithRolesByIdAsync(userId);

            AccountViewModel viewModel = new AccountViewModel();
            viewModel.UserId = userId;
            viewModel.FirstName = user.FirstName;
            viewModel.LastName = user.LastName;
            viewModel.PhoneNumber = user.PhoneNumber;

            if (user == null)
                return NotFound();
            else
                return PartialView("_Registery", viewModel);
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisteryRequest(AccountViewModel viewModel)
        {
            int userId = User.Identity.GetUserId<int>();
            Account model = new Account();

            viewModel = new AccountViewModel
            {
                UserId = userId,
                Amount = viewModel.Amount,
                HelpID = viewModel.HelpID,
                RequestTypes = viewModel.RequestTypes,
                PortTypes = viewModel.PortTypes,
                PaymentTypes = 0,
            };

            viewModel.AccountId = StringExtensions.GenerateId(10);
            await _uw.BaseRepository<Account>().CreateAsync(_mapper.Map(viewModel, model));
            await _uw.Commit();
            if (_uw.Commit().IsCompletedSuccessfully)
                ViewBag.Alert = "عضویت شما با موفقیت انجام شد.";

            return PartialView("_Registery", viewModel);
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> UserInfo(int userId)
        {
            var loginUserId = User.Identity.GetUserId<int>();

            if (loginUserId == userId)
            {
                UserPanelViewModel viewModel = new UserPanelViewModel();

                if (userId == 0)
                    return NotFound();
                else
                {
                    var user = await _userManager.FindByIdAsync(userId.ToString());
                    if (user == null)
                        return NotFound();
                    else
                        viewModel = _mapper.Map<UserPanelViewModel>(user);
                }

                return PartialView("_UserInfo", viewModel);
            }
            else
                return NotFound();
        }


        [HttpPost, Authorize]
        public async Task<IActionResult> UserInfo(UserPanelViewModel viewModel)
        {
            if (viewModel.Id == null)
                return NotFound();
            else
            {
                var user = await _userManager.FindByIdAsync(viewModel.Id.ToString());

                if (user == null)
                    return NotFound();
                else
                {
                    IdentityResult result = null;

                    if (viewModel.NewPassword.HasValue() && viewModel.ConfirmNewPassword.HasValue())
                        result = await _userManager.ChangePasswordAsync(user, viewModel.OldPassword, viewModel.NewPassword);

                    result = await _userManager.UpdateAsync(_mapper.Map(viewModel, user));

                    if (result.Succeeded)
                        ViewBag.Alert = "ویرایش اطلاعات با موفقیت انجام شد.";
                    else
                        ModelState.AddErrorsFromResult(result);
                }

                return PartialView("_UserInfo", viewModel);
            }
        }


        [HttpGet]
        public async Task<IActionResult> ChangePassword2()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
                return NotFound();

            return View();
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> ChangePassword()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
                return NotFound();

            return View(new UserPanelViewModel());
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(UserPanelViewModel ViewModel)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                var ChangePassResult = await _userManager.ChangePasswordAsync(user, ViewModel.OldPassword, ViewModel.NewPassword);

                if (ChangePassResult.Succeeded)
                    ViewBag.Alert = "کلمه عبور شما با موفقیت تغییر یافت.";
                else
                    foreach (var item in ChangePassResult.Errors)
                        ModelState.AddModelError(string.Empty, item.Description);
            }

            return View(ViewModel);
        }


        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
                return RedirectToAction("Index", "Home");

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                return NotFound($"Unable to load user with ID '{userId}'");

            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (!result.Succeeded)
                throw new InvalidOperationException($"Error Confirming email for user with ID '{userId}'");

            return View();
        }
    }
}
