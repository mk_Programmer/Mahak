﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Mahak.Entities;
using Mahak.Entities.Identity;
using Mahak.Data.Contracts;
using Mahak.ViewModel.Account;
using Mahak.Common.Extensions;
using Mahak.Services.Contracts;

namespace Mahak.Controllers
{
    public class CodeController : Controller
    {
        private readonly IUnitOfWork _uw;
        private readonly IApplicationUserManager _userManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly SignInManager<User> _signInManager;
        public CodeController(IUnitOfWork uw, IApplicationUserManager userManager, IEmailSender emailSender, ISmsSender smsSender, SignInManager<User> signInManager)
        {
            _uw = uw;
            _userManager = userManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _signInManager = signInManager;
        }


        [HttpGet]
        public async Task<IActionResult> SendCode(bool RememberMe)
        {
            var FactorOptions = new List<SelectListItem>();
            var User = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (User == null)
                return NotFound();

            var UserFactors = await _userManager.GetValidTwoFactorProvidersAsync(User);
            foreach (var item in UserFactors)
            {
                if (item == "Authenticator")
                    FactorOptions.Add(new SelectListItem { Text = "اپلیکشن احراز هویت", Value = item });
                else
                    FactorOptions.Add(new SelectListItem { Text = (item == "Email" ? "ارسال ایمیل" : "ارسال پیامک"), Value = item });
            }

            return View(new UserPanelViewModel { Providers = FactorOptions, RememberMe = RememberMe });
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> SendCode(UserPanelViewModel ViewModel)
        {
            if (ModelState.IsValid)
                return View(ViewModel);

            var User = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (User == null)
                return NotFound();

            if (ViewModel.SelectedProvider != "Authenticator")
            {
                var Code = await _userManager.GenerateTwoFactorTokenAsync(User, ViewModel.SelectedProvider);
                if (string.IsNullOrWhiteSpace(Code))
                    return View("Error");

                var Message = "<p style='direction:rtl;font-size:14px;font-family:tahoma'>کد اعتبارسنجی شما :" + Code + "</p>";

                if (ViewModel.SelectedProvider == "Email")
                    await _emailSender.SendEmailAsync(User.Email, "کد اعتبارسنجی", Message);
                else if (ViewModel.SelectedProvider == "Phone")
                {
                    string ResponseSms = await _smsSender.SendAuthSmsAsync(Code, User.PhoneNumber);
                    if (ResponseSms == "Failed")
                    {
                        ModelState.AddModelError(string.Empty, "در ارسال پیامک خطایی رخ داده است.");
                        return View(ViewModel);
                    }
                }

                return RedirectToAction("VerifyCode", new { Provider = ViewModel.SelectedProvider, RememberMe = ViewModel.RememberMe });
            }
            else
                return RedirectToAction("LoginWith2fa", new { RememberMe = ViewModel.RememberMe });
        }


        [HttpGet]
        public async Task<IActionResult> VerifyCode(string Provider, bool RememberMe)
        {
            var User = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (User == null)
                return NotFound();

            return View(new UserPanelViewModel { Provider = Provider, RememberMe = RememberMe });
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyCode(UserPanelViewModel viewModel)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (ModelState.IsValid)
                return View(viewModel);

            var Result = await _signInManager.TwoFactorSignInAsync(viewModel.Provider, viewModel.Code, viewModel.RememberMe, viewModel.RememberBrowser);
            if (Result.Succeeded)
            {
                user.TwoFactorEnabled = false;
                await _userManager.UpdateAsync(user);
                return RedirectToAction("Index", "Home");
            }
            else if (Result.IsLockedOut)
                ModelState.AddModelError(string.Empty, "حساب کاربری شما به دلیل تلاش های ناموفق به مدت 20 دقیقه قفل شد.");
            else
                ModelState.AddModelError(string.Empty, "کد اعتبارسنجی صحیح نمی باشد");

            return View(viewModel);
        }


        [HttpGet, Authorize]
        public async Task<IActionResult> MessageCode(MessageUsersViewModel viewModel)
        {
            int userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindClaimsInUser(userId);

            var usersViewModel = await _userManager.FindUserWithRolesByIdAsync(userId);

            viewModel.UserId = userId;
            viewModel.FullName = user.FirstName + " " + user.LastName;
            viewModel.PhoneNumber = user.PhoneNumber;
            viewModel.Email = user.Email;

            viewModel.SelectedProvider = "Email";
            var Code = await _userManager.GenerateTwoFactorTokenAsync(user, viewModel.SelectedProvider);
            if (string.IsNullOrWhiteSpace(Code))
                return View("Error");

            return PartialView("_MessageCode", new MessageUsersViewModel { CommentCode = Code });
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> MessageCode(MessageUsersViewModel viewModel, string CommentCode)
        {
            MessageUsers model = new MessageUsers();
            model = await _uw.BaseRepository<MessageUsers>().FindByIdAsync(viewModel.MessageId);
            int userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindClaimsInUser(userId);

            var Message = "<p style='direction:rtl;font-size:14px;font-family:tahoma'>کد پیگیری شما :" + CommentCode + "</p>";
            await _emailSender.SendEmailAsync(user.Email, "کد پیگیری", Message);

            var Result = await _signInManager.TwoFactorSignInAsync(viewModel.SelectedProvider, viewModel.CommentCode, false, false);
            if (Result.Succeeded)
            {
                model.IsRegisterCode = true;
                _uw.BaseRepository<MessageUsers>().Update(model);
                await _uw.Commit();
                return RedirectToAction("SendMessage", "Account");
            }
            else
                ModelState.AddModelError(string.Empty, "کد اعتبارسنجی صحیح نمی باشد");

            return PartialView("_MessageCode", viewModel);
        }
    }
}
