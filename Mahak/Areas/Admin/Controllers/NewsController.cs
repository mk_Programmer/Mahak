﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Mahak.ViewModel.News;
using Mahak.Common.Extensions;
using Mahak.Common.Attributes;
using Mahak.ViewModel.DynamicAccess;

namespace Mahak.Areas.Admin.Controllers
{
    [DisplayName("مدیریت اخبار")]
    public class NewsController : BaseController
    {
        private const string NewsNotFound = "خبر یافت نشد.";
        private readonly IUnitOfWork _uw;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _env;
        private readonly IHttpContextAccessor _context;
        public NewsController(IUnitOfWork uw, IMapper mapper, IWebHostEnvironment env, IHttpContextAccessor context)
        {
            _uw = uw;
            _uw.CheckArgumentIsNull(nameof(_uw));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));

            _env = env;
            _env.CheckArgumentIsNull(nameof(_env));

            _context = context;
            _context.CheckArgumentIsNull(nameof(_context));
        }


        [HttpGet, DisplayName("مشاهده"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> GetNews(string search, string order, int offset, int limit, string sort)
        {
            List<NewsViewModel> news;
            int total = _uw.BaseRepository<News>().CountEntities();

            if (!search.HasValue())
                search = "";

            if (limit == 0)
                limit = total;

            if (sort == "ShortTitle")
            {
                if (order == "asc")
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "ShortTitle", search, null, null);
                else
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "ShortTitle desc", search, null, null);
            }
            else if (sort == "بازدید")
            {
                if (order == "asc")
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "NumberOfVisit", search, null, null);
                else
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "NumberOfVisit desc", search, null, null);
            }
            else if (sort == "نوع متن")
            {
                if (order == "asc")
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "TextLocations", search, null, null);
                else
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "TextLocations desc", search, null, null);
            }
            else if (sort == "تاریخ انتشار")
            {
                if (order == "asc")
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "PublishDateTime", search, null, null);
                else
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "PublishDateTime desc", search, null, null);
            }
            else if (sort == "نظرات")
            {
                if (order == "asc")
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "NumberOfComments", search, null, null);
                else
                    news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "NumberOfComments desc", search, null, null);
            }
            else
                news = await _uw.NewsRepository.GetPaginateNewsAsync(offset, limit, "PublishDateTime desc", search, null, null);

            if (search != "")
                total = news.Count();

            return Json(new { total = total, rows = news });
        }


        [HttpGet, DisplayName("درج و ویرایش"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> CreateOrUpdate(string newsId)
        {
            NewsViewModel newsViewModel = new NewsViewModel();
            ViewBag.Tags = _uw.Context.Tags.Select(t => t.TagName).ToList();

            if (newsId.HasValue())
            {
                var news = await (from n in _uw.Context.News
                                  join w in _uw.Context.NewsTags on n.NewsId equals w.NewsId into bc
                                  from bct in bc.DefaultIfEmpty()
                                  join t in _uw.Context.Tags on bct.TagId equals t.TagId into cg
                                  from cog in cg.DefaultIfEmpty()
                                  where (n.NewsId == newsId)
                                  select new NewsViewModel
                                  {
                                      NewsId = n.NewsId,
                                      Title = n.Title,
                                      Abstract = n.Abstract,
                                      Description = n.Description,
                                      PublishDateTime = n.PublishDateTime,
                                      IsPublish = n.IsPublish,
                                      ImageName = n.ImageName,
                                      Url = n.Url,
                                      NameOfTags = cog != null ? cog.TagName : "",
                                      Locations = n.TextLocations == TextLocation.News ? "خبر" : n.TextLocations == TextLocation.Introduction ? "معرفی محک" : n.TextLocations == TextLocation.History ? "تاریخچه" : "-",
                                  }).ToListAsync();

                if (news != null)
                {
                    newsViewModel = _mapper.Map<NewsViewModel>(news.FirstOrDefault());

                    if (news.FirstOrDefault().PublishDateTime > DateTime.Now)
                    {
                        newsViewModel.FuturePublish = true;
                        newsViewModel.PersianPublishDate = news.FirstOrDefault().PublishDateTime.ConvertMiladiToShamsi("yyyy/MM/dd");
                        newsViewModel.PersianPublishTime = news.FirstOrDefault().PublishDateTime.Value.TimeOfDay.ToString();
                    }

                    newsViewModel.NameOfTags = news.Select(t => t.NameOfTags).Distinct().ToArray().CombineWith(',');
                }
            }

            return View(newsViewModel);
        }


        [HttpPost]
        public async Task<IActionResult> CreateOrUpdate(NewsViewModel viewModel, string submitButton)
        {
            News model = new News();
            viewModel.Url = viewModel.Url.Trim();
            ViewBag.Tags = _uw.Context.Tags.Select(t => t.TagName).ToList();

            if (!viewModel.FuturePublish)
            {
                ModelState.Remove("PersianPublishTime");
                ModelState.Remove("PersianPublishDate");
            }

            if (viewModel.NewsId.HasValue())
                ModelState.Remove("ImageFile");

            if (ModelState.IsValid)
            {
                model.TextLocations = viewModel.TextLocations;

                if (submitButton != "ذخیره پیش نویس")
                    viewModel.IsPublish = true;

                if (viewModel.ImageFile != null)
                    viewModel.ImageName = $"news-{StringExtensions.GenerateId(10)}.jpg";

                if (viewModel.NewsId.HasValue())
                {
                    viewModel.TextLocations = model.TextLocations;
                    viewModel.Locations = model.TextLocations == TextLocation.News ? "خبر" : model.TextLocations == TextLocation.Introduction ? "معرفی محک" : model.TextLocations == TextLocation.History ? "تاریخچه" : "-";
                    
                    var news = _uw.BaseRepository<News>().FindByConditionAsync(n => n.NewsId == viewModel.NewsId, null, n => n.NewsTags).Result.FirstOrDefault();

                    if (news == null)
                        ModelState.AddModelError(string.Empty, NewsNotFound);
                    else
                    {
                        if (viewModel.IsPublish && news.IsPublish == false)
                            viewModel.PublishDateTime = DateTimeExtensions.DateTimeWithOutMilliSecends(DateTime.Now);

                        if (viewModel.IsPublish && news.IsPublish == true)
                        {
                            if (viewModel.PersianPublishDate.HasValue())
                            {
                                var persianTimeArray = viewModel.PersianPublishTime.Split(':');
                                viewModel.PublishDateTime = viewModel.PersianPublishDate.ConvertShamsiToMiladi().Date + new TimeSpan(int.Parse(persianTimeArray[0]), int.Parse(persianTimeArray[1]), 0);
                            }
                            else
                                viewModel.PublishDateTime = news.PublishDateTime;
                        }

                        if (viewModel.ImageFile != null)
                        {
                            viewModel.ImageFile.UploadFileBase64($"{_env.WebRootPath}/images/{viewModel.ImageName}");
                            FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{news.ImageName}");
                        }
                        else
                            viewModel.ImageName = news.ImageName;

                        if (viewModel.NameOfTags.HasValue())
                            viewModel.NewsTags = await _uw.TagRepository.InsertNewsTags(viewModel.NameOfTags.Split(','), news.NewsId);
                        else
                            viewModel.NewsTags = news.NewsTags;

                        viewModel.UserId = news.UserId;
                        _uw.BaseRepository<News>().Update(_mapper.Map(viewModel, news));
                        await _uw.Commit();
                        ViewBag.Alert = "ذخیره تغییرات با موفقیت انجام شد.";
                    }
                }
                else
                {
                    viewModel.ImageFile.UploadFileBase64($"{_env.WebRootPath}/images/{viewModel.ImageName}");
                    viewModel.NewsId = StringExtensions.GenerateId(10);
                    viewModel.UserId = User.Identity.GetUserId<int>();

                    if (viewModel.IsPublish)
                    {
                        if (!viewModel.PersianPublishDate.HasValue())
                            viewModel.PublishDateTime = DateTimeExtensions.DateTimeWithOutMilliSecends(DateTime.Now);
                        else
                        {
                            var persianTimeArray = viewModel.PersianPublishTime.Split(':');
                            viewModel.PublishDateTime = viewModel.PersianPublishDate.ConvertShamsiToMiladi().Date + new TimeSpan(int.Parse(persianTimeArray[0]), int.Parse(persianTimeArray[1]), 0);
                        }
                    }

                    if (viewModel.NameOfTags.HasValue())
                        viewModel.NewsTags = await _uw.TagRepository.InsertNewsTags(viewModel.NameOfTags.Split(","));
                    else
                        viewModel.NewsTags = null;

                    await _uw.BaseRepository<News>().CreateAsync(_mapper.Map<News>(viewModel));
                    await _uw.Commit();
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(viewModel);
        }


        [HttpGet, AjaxOnly, DisplayName("حذف"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> Delete(string newsId)
        {
            if (!newsId.HasValue())
                ModelState.AddModelError(string.Empty, NewsNotFound);
            else
            {
                var news = await _uw.BaseRepository<News>().FindByIdAsync(newsId);

                if (news == null)
                    ModelState.AddModelError(string.Empty, NewsNotFound);
                else
                    return PartialView("_DeleteConfirmation", news);
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("Delete"), AjaxOnly]
        public async Task<IActionResult> DeleteConfirmed(News model)
        {
            if (model.NewsId == null)
                ModelState.AddModelError(string.Empty, NewsNotFound);
            else
            {
                var news = await _uw.BaseRepository<News>().FindByIdAsync(model.NewsId);

                if (news == null)
                    ModelState.AddModelError(string.Empty, NewsNotFound);
                else
                {
                    _uw.BaseRepository<News>().Delete(news);
                    await _uw.Commit();
                    FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{news.ImageName}");
                    TempData["notification"] = DeleteSuccess;
                    return PartialView("_DeleteConfirmation", news);
                }
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("DeleteGroup"), AjaxOnly, DisplayName("حذف گروهی"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> DeleteGroupConfirmed(string[] btSelectItem)
        {
            if (btSelectItem.Count() == 0)
                ModelState.AddModelError(string.Empty, "هیچ خبری برای حذف انتخاب نشده است.");
            else
            {
                foreach (var item in btSelectItem)
                {
                    var news = await _uw.BaseRepository<News>().FindByIdAsync(item);
                    _uw.BaseRepository<News>().Delete(news);
                    FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{news.ImageName}");
                }

                await _uw.Commit();
                TempData["notification"] = DeleteGroupSuccess;
            }

            return PartialView("_DeleteGroup");
        }


        public async Task<JsonResult> UploadNewsImage(IFormFile file)
        {
            await FileExtensions.UploadFileAsync(file, $"{_env.WebRootPath}/images/{file.FileName}");
            return Json(new { location = $"{_context.HttpContext.Request.Scheme}://{_context.HttpContext.Request.Host}/images/{file.FileName}" });
        }
    }
}
