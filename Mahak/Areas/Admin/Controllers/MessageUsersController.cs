﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Mahak.Common.Extensions;
using Mahak.ViewModel.Account;
using Mahak.ViewModel.DynamicAccess;

namespace Mahak.Areas.Admin.Controllers
{
    [DisplayName("مدیریت پیام ها")]
    public class MessageUsersController  : BaseController
    {
        private const string MessageNotFound = "پیام یافت نشد.";
        private readonly IUnitOfWork _uw;
        private readonly IMapper _mapper;
        public MessageUsersController(IUnitOfWork uw, IMapper mapper)
        {
            _uw = uw;
            _uw.CheckArgumentIsNull(nameof(_uw));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));
        }


        [HttpGet, DisplayName("مشاهده"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public IActionResult Index(int userId, bool? IsRegisterCode)
        {
            return View(nameof(Index), new MessageUsersViewModel { UserId = userId, IsRegisterCode = IsRegisterCode });
        }


        [HttpGet]
        public async Task<IActionResult> GetMessages(string search, string order, int offset, int limit, string sort, int userId, bool? isRegisterCode)
        {
            List<MessageUsersViewModel> viewModels;
            int total = _uw.BaseRepository<MessageUsers>().CountEntities();

            if (!search.HasValue())
                search = "";

            if (limit == 0)
                limit = total;

            if (userId == 0)
                userId = 0;

            if (sort == "ایمیل")
            {
                if (order == "asc")
                    viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "Email", search, userId, isRegisterCode);
                else
                    viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "Email desc", search, userId, isRegisterCode);
            }
            else if (sort == "نوع پیام")
            {
                if (order == "asc")
                    viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "MessageType", search, userId, isRegisterCode);
                else
                    viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "MessageType desc", search, userId, isRegisterCode);
            }
            else if (sort == "تاریخ ثبت")
            {
                if (order == "asc")
                    viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "RegisterDateTime", search, userId, isRegisterCode);
                else
                    viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "RegisterDateTime desc", search, userId, isRegisterCode);
            }
            else
                viewModels = await _uw.MessageUsersRepository.GetPaginateMessagesAsync(offset, limit, "RegisterDateTime desc", search, userId, isRegisterCode);

            if (search != "")
                total = viewModels.Count();

            return Json(new { total = total, rows = viewModels });
        }


        [HttpGet, DisplayName("حذف"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> Delete(string messageId)
        {
            if (!messageId.HasValue())
                ModelState.AddModelError(string.Empty, MessageNotFound);
            else
            {
                var message = await _uw.BaseRepository<MessageUsers>().FindByIdAsync(messageId);

                if (message == null)
                    ModelState.AddModelError(string.Empty, MessageNotFound);
                else
                    return PartialView("_DeleteConfirmation", message);
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(MessageUsers model)
        {
            if (model.MessageId == null)
                ModelState.AddModelError(string.Empty, MessageNotFound);
            else
            {
                var message = await _uw.BaseRepository<MessageUsers>().FindByIdAsync(model.MessageId);

                if (message == null)
                    ModelState.AddModelError(string.Empty, MessageNotFound);
                else
                {
                    _uw.BaseRepository<MessageUsers>().Delete(message);
                    await _uw.Commit();
                    TempData["notification"] = DeleteSuccess;
                    return PartialView("_DeleteConfirmation", message);
                }
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("DeleteGroup"), DisplayName("حذف گروهی"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> DeleteGroupConfirmed(string[] btSelectItem)
        {
            if (btSelectItem.Count() == 0)
                ModelState.AddModelError(string.Empty, "هیچ دیدگاهی برای حذف انتخاب نشده است.");
            else
            {
                foreach (var item in btSelectItem)
                {
                    var message = await _uw.BaseRepository<MessageUsers>().FindByIdAsync(item);
                    _uw.BaseRepository<MessageUsers>().Delete(message);
                }

                await _uw.Commit();
                TempData["notification"] = DeleteGroupSuccess;
            }

            return PartialView("_DeleteGroup");
        }


        [HttpGet, DisplayName("تایید و عدم تایید"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> AnswerMessage(string messageId)
        {
            if (!messageId.HasValue())
                ModelState.AddModelError(string.Empty, MessageNotFound);
            else
            {
                var message = await _uw.BaseRepository<MessageUsers>().FindByIdAsync(messageId);

                if (message == null)
                    ModelState.AddModelError(string.Empty, MessageNotFound);
                else
                    return PartialView("_ConfirmOrInconfirm", message);
            }

            return PartialView("_ConfirmOrInconfirm");
        }


        [HttpPost]
        public async Task<IActionResult> AnswerMessage(MessageUsers model)
        {
            if (model.MessageId == null)
                ModelState.AddModelError(string.Empty, MessageNotFound);
            else
            {
                var message = await _uw.BaseRepository<MessageUsers>().FindByIdAsync(model.MessageId);

                if (message == null)
                    ModelState.AddModelError(string.Empty, MessageNotFound);
                else
                {
                    if (message.IsAnswer)
                        message.IsAnswer = false;
                    else
                        message.IsAnswer = true;

                    _uw.BaseRepository<MessageUsers>().Update(message);
                    await _uw.Commit();
                    TempData["notification"] = OperationSuccess;
                    return PartialView("_ConfirmOrInconfirm", message);
                }
            }

            return PartialView("_ConfirmOrInconfirm");
        }


        [HttpGet]
        public IActionResult SendMessage()
        {
            return PartialView("_SendMessage", new MessageUsersViewModel());
        }


        [HttpPost]
        public async Task<IActionResult> SendMessage(MessageUsersViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.RegisterDateTime = DateTimeExtensions.DateTimeWithOutMilliSecends(DateTime.Now);
                viewModel.MessageId = StringExtensions.GenerateId(10);
                await _uw.BaseRepository<MessageUsers>().CreateAsync(_mapper.Map<MessageUsers>(viewModel));
                await _uw.Commit();
                TempData["notification"] = "پیام شما با موفقیت ارسال شد و بعد از تایید در سایت نمایش داده می شود.";
            }

            return PartialView("_SendMessage", viewModel);
        }
    }
}

