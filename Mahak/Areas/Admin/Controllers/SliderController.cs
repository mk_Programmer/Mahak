﻿using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Mahak.ViewModel.Slider;
using Mahak.Common.Extensions;
using Mahak.Common.Attributes;
using Mahak.ViewModel.DynamicAccess;

namespace Mahak.Areas.Admin.Controllers
{
    [DisplayName("مدیریت اسلایدرها")]
    public class SliderController : BaseController
    {
        private const string SliderNotFound = "اسلایدر درخواستی یافت نشد.";
        private readonly IUnitOfWork _uw;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _env;
        public SliderController(IUnitOfWork uw, IMapper mapper, IWebHostEnvironment env)
        {
            _uw = uw;
            _uw.CheckArgumentIsNull(nameof(_uw));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));

            _env = env;
            _env.CheckArgumentIsNull(nameof(_env));
        }


        [HttpGet, DisplayName("مشاهده"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> GetSliders(string search, string order, int offset, int limit, string sort)
        {
            List<SliderViewModel> sliders;
            int total = _uw.BaseRepository<Slider>().CountEntities();

            if (!search.HasValue())
                search = "";

            if (limit == 0)
                limit = total;

            if (sort == "عنوان اسلایدر")
            {
                if (order == "asc")
                    sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(offset, limit, "Title", search);
                else
                    sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(offset, limit, "Title desc", search);
            }
            else if (sort == "جایگاه اسلایدر")
            {
                if (order == "asc")
                    sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(offset, limit, "SliderLocations", search);
                else
                    sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(offset, limit, "SliderLocations desc", search);
            }
            else
                sliders = await _uw.SliderRepository.GetPaginateSlidersAsync(offset, limit, "Title desc", search);

            if (search != "")
                total = sliders.Count();

            return Json(new { total = total, rows = sliders });
        }


        [HttpGet, AjaxOnly(), DisplayName("درج و ویرایش"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> RenderSlider(string sliderId)
        {
            var sliderViewModel = new SliderViewModel();

            if (sliderId.HasValue())
            {
                var slider = await _uw.BaseRepository<Slider>().FindByIdAsync(sliderId);

                if (slider != null)
                    sliderViewModel = _mapper.Map<SliderViewModel>(slider);
                else
                    ModelState.AddModelError(string.Empty, SliderNotFound);
            }

            return PartialView("_RenderSlider", sliderViewModel);
        }


        [HttpPost, AjaxOnly()]
        public async Task<IActionResult> CreateOrUpdate(SliderViewModel viewModel)
        {
            Slider model = new Slider();
            if (ModelState.IsValid)
            {
                if (viewModel.ImageFile != null)
                {
                    viewModel.ImageName = $"slider-{StringExtensions.GenerateId(10)}.jpg";
                    await viewModel.ImageFile.UploadFileAsync($"{_env.WebRootPath}/images/{viewModel.ImageName}");
                }

                model.SliderLocations = viewModel.SliderLocations;

                if (viewModel.SliderId.HasValue())
                {
                    viewModel.SliderLocations = model.SliderLocations;
                    var slider = await _uw.BaseRepository<Slider>().FindByIdAsync(viewModel.SliderId);

                    if (slider != null)
                    {
                        if (viewModel.ImageFile != null)
                            FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{slider.ImageName}");
                        else
                            viewModel.ImageName = slider.ImageName;

                        _uw.BaseRepository<Slider>().Update(_mapper.Map(viewModel, slider));
                        await _uw.Commit();
                        TempData["notification"] = EditSuccess;
                    }
                    else
                        ModelState.AddModelError(string.Empty, SliderNotFound);
                }
                else
                {
                    viewModel.SliderId = StringExtensions.GenerateId(10);
                    await _uw.BaseRepository<Slider>().CreateAsync(_mapper.Map<Slider>(viewModel));
                    await _uw.Commit();
                    TempData["notification"] = InsertSuccess;
                }
            }

            return PartialView("_RenderSlider", viewModel);
        }


        [HttpGet, AjaxOnly(), DisplayName("حذف"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> Delete(string sliderId)
        {
            if (!sliderId.HasValue())
                ModelState.AddModelError(string.Empty, SliderNotFound);
            else
            {
                var slider = await _uw.BaseRepository<Slider>().FindByIdAsync(sliderId);

                if (slider == null)
                    ModelState.AddModelError(string.Empty, SliderNotFound);
                else
                    return PartialView("_DeleteConfirmation", slider);
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("Delete"), AjaxOnly()]
        public async Task<IActionResult> DeleteConfirmed(Slider model)
        {
            if (model.SliderId == null)
                ModelState.AddModelError(string.Empty, SliderNotFound);
            else
            {
                var slider = await _uw.BaseRepository<Slider>().FindByIdAsync(model.SliderId);

                if (slider == null)
                    ModelState.AddModelError(string.Empty, SliderNotFound);
                else
                {
                    FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{slider.ImageName}");
                    _uw.BaseRepository<Slider>().Delete(slider);
                    await _uw.Commit();
                    TempData["notification"] = DeleteSuccess;
                    return PartialView("_DeleteConfirmation", slider);
                }
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("DeleteGroup"), AjaxOnly(), DisplayName("حذف گروهی"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> DeleteGroupConfirmed(string[] btSelectItem)
        {
            if (btSelectItem.Count() == 0)
                ModelState.AddModelError(string.Empty, "هیچ اسلایدری برای حذف انتخاب نشده است.");
            else
            {
                foreach (var item in btSelectItem)
                {
                    var slider = await _uw.BaseRepository<Slider>().FindByIdAsync(item);
                    _uw.BaseRepository<Slider>().Delete(slider);
                    await _uw.Commit();
                    FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{slider.ImageName}");
                }

                TempData["notification"] = DeleteGroupSuccess;
            }

            return PartialView("_DeleteGroup");
        }
    }
}
