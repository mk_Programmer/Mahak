﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mahak.Common.Extensions;
using Mahak.Data.Contracts;
using Mahak.ViewModel.Dashboard;
using Mahak.ViewModel.DynamicAccess;

namespace Mahak.Areas.Admin.Controllers
{
    [DisplayName("داشبورد")]
    public class DashboardController : BaseController
    {
        private readonly IUnitOfWork _uw;
        public DashboardController(IUnitOfWork uw)
        {
            _uw = uw;
        }


        [HttpGet, DisplayName("مشاهده"), Authorize(Policy = ConstantPolicies.DynamicPermission)]        
        public IActionResult Index()
        {
            //ViewBag.News = _uw.NewsRepository.CountNews();
            //ViewBag.FuturePublishedNews = _uw.NewsRepository.CountFuturePublishedNews();
            //ViewBag.NewsPublished = _uw.NewsRepository.CountNewsPublishedOrDraft(true);
            //ViewBag.DraftNews = _uw.NewsRepository.CountNewsPublishedOrDraft(false);

            //int numberOfVisit;
            var month = StringExtensions.GetMonth();
            var year = DateTimeExtensions.ConvertMiladiToShamsi(DateTime.Now, "yyyy");
            var numberOfVisitList = new List<NumberOfVisitChartViewModel>();
            DateTime StartDateTimeMiladi;
            DateTime EndDateTimeMiladi;

            for (int i = 0; i < month.Length; i++)
            {
                StartDateTimeMiladi = DateTimeExtensions.ConvertShamsiToMiladi($"{year}/{i + 1}/01");

                if (i < 11)
                    EndDateTimeMiladi = DateTimeExtensions.ConvertShamsiToMiladi($"{year}/{i + 2}/01");
                else
                    EndDateTimeMiladi = DateTimeExtensions.ConvertShamsiToMiladi($"{year}/01/01");

                //numberOfVisit = _uw.Context.News.Where(n => n.PublishDateTime < EndDateTimeMiladi && StartDateTimeMiladi <= n.PublishDateTime)
                //    .Include(v => v.Visits).Select(k => k.Visits.Sum(v => v.NumberOfVisit)).AsEnumerable().Sum(); // AsEnumerable() اینجا داده هایی که از سمت دیتابیس بسمت برنامه می اند زیاد نیستند استفاده از متد

                //numberOfVisitList.Add(new NumberOfVisitChartViewModel { Name = month[i], Value = numberOfVisit });
            }

            ViewBag.NumberOfVisitChart = numberOfVisitList;
            return View();
        }
    }
}
