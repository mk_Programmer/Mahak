﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Mahak.Common.Extensions;
using Mahak.Common.Attributes;
using Mahak.ViewModel.Product;
using Mahak.ViewModel.DynamicAccess;

namespace Mahak.Areas.Admin.Controllers
{
    [DisplayName("مدیریت محصولات")]
    public class ProductController : BaseController
    {
        private readonly IUnitOfWork _uw;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _env;
        private readonly IHttpContextAccessor _context;
        private const string ProductNotFound = "محصول درخواستی یافت نشد.";
        public ProductController(IUnitOfWork uw, IMapper mapper, IWebHostEnvironment env, IHttpContextAccessor context)
        {
            _uw = uw;
            _uw.CheckArgumentIsNull(nameof(_uw));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));

            _env = env;
            _env.CheckArgumentIsNull(nameof(_env));

            _context = context;
            _context.CheckArgumentIsNull(nameof(_context));
        }


        [HttpGet, DisplayName("مشاهده"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> GetProducts(string search, string order, int offset, int limit, string sort)
        {
            List<ProductViewModel> viewModels;
            int total = _uw.BaseRepository<Product>().CountEntities();

            if (!search.HasValue())
                search = "";

            if (limit == 0)
                limit = total;

            if (sort == "نام محصول")
            {
                if (order == "asc")
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "ProductName", search);
                else
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "ProductName desc", search);
            }
            else if (sort == "دسته")
            {
                if (order == "asc")
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "NameOfCategories", search);
                else
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "NameOfCategories desc", search);
            }
            else if (sort == "قیمت")
            {
                if (order == "asc")
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "Price", search);
                else
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "Price desc", search);
            }
            else if (sort == "موجودی")
            {
                if (order == "asc")
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "Stock", search);
                else
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "Stock desc", search);
            }
            else if (sort == "تاریخ ساخت")
            {
                if (order == "asc")
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "CreateDate", search);
                else
                    viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "CreateDate desc", search);
            }
            else
                viewModels = await _uw.ProductRepository.GetPaginateProductAsync(offset, limit, "CreateDate desc", search);

            if (search != "")
                total = viewModels.Count();

            return Json(new { total = total, rows = viewModels });
        }


        [HttpGet, DisplayName("درج و ویرایش"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> CreateOrUpdate(string productId)
        {
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.ProductCategoriesViewModel = new ProductCategoriesViewModel(await _uw.CategoryRepository.GetAllCategoriesAsync(), null);

            if (productId.HasValue())
            {
                var products = await (from n in _uw.Context.Products
                                      join e in _uw.Context.ProductCategories on n.ProductId equals e.ProductId into nc
                                      from nct in nc.DefaultIfEmpty()
                                      where (n.ProductId == productId)
                                      select new ProductViewModel
                                      {
                                          ProductId = n.ProductId,
                                          ProductName = n.ProductName,
                                          Price = n.Price,
                                          Stock = n.Stock,
                                          ProductCode = n.ProductCode,
                                          Description = n.Description,
                                          Url = n.Url,
                                          ImageName = n.ImageName,
                                          CreateDate = n.CreateDate,
                                          PersianCreateDate = n.CreateDate.ConvertMiladiToShamsi("yyyy/MM/dd"),
                                          IdOfCategories = nct != null ? nct.CategoryId : "",
                                      }).ToListAsync();

                if (products != null)
                {
                    viewModel = _mapper.Map<ProductViewModel>(products.FirstOrDefault());

                    if (products.FirstOrDefault().CreateDate > DateTime.Now)
                        viewModel.PersianCreateDate = products.FirstOrDefault().CreateDate.ConvertMiladiToShamsi("yyyy/MM/dd");

                    viewModel.ProductCategoriesViewModel = new ProductCategoriesViewModel(await _uw.CategoryRepository.GetAllCategoriesAsync(), products.Select(n => n.IdOfCategories).Distinct().ToArray());
                }
            }

            return View(viewModel);
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOrUpdate(ProductViewModel viewModel, string submitButton)
        {
            Product model = new Product();
            viewModel.ProductCategoriesViewModel = new ProductCategoriesViewModel(await _uw.CategoryRepository.GetAllCategoriesAsync(), viewModel.CategoryIds);

            if (ModelState.IsValid != true)
            {
                viewModel.CreateDate = viewModel.PersianCreateDate.ConvertShamsiToMiladi();

                if (viewModel.ImageFile != null)
                    viewModel.ImageName = $"product-{StringExtensions.GenerateId(10)}.jpg";

                if (viewModel.ProductId.HasValue())
                {
                    var product = _uw.BaseRepository<Product>().FindByConditionAsync(n => n.ProductId == viewModel.ProductId, null, n => n.ProductCategories).Result.FirstOrDefault();

                    if (product == null)
                        ModelState.AddModelError(string.Empty, ProductNotFound);
                    else
                    {
                        if (viewModel.PersianCreateDate.HasValue())
                            viewModel.CreateDate = viewModel.PersianCreateDate.ConvertShamsiToMiladi();
                        else
                            viewModel.CreateDate = product.CreateDate;

                        if (viewModel.ImageFile != null)
                        {
                            viewModel.ImageFile.UploadFileBase64($"{_env.WebRootPath}/images/{viewModel.ImageName}");
                            FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{product.ImageName}");
                            model.ImageName = viewModel.ImageName;
                        }

                        if (viewModel.CategoryIds == null)
                            viewModel.ProductCategories = product.ProductCategories;
                        else
                            viewModel.ProductCategories = viewModel.CategoryIds.Select(c => new ProductCategory { CategoryId = c, ProductId = product.ProductId }).ToList();

                        _uw.BaseRepository<Product>().Update(_mapper.Map(viewModel, product));
                        await _uw.Commit();
                        ViewBag.Alert = EditSuccess;
                    }
                }
                else
                {
                    if (viewModel.CategoryIds != null)
                        viewModel.ProductCategories = viewModel.CategoryIds.Select(c => new ProductCategory { CategoryId = c }).ToList();
                    else
                        viewModel.ProductCategories = null;

                    viewModel.ImageFile.UploadFileBase64($"{_env.WebRootPath}/images/{viewModel.ImageName}");
                    viewModel.ProductId = StringExtensions.GenerateId(10);
                    await _uw.BaseRepository<Product>().CreateAsync(_mapper.Map<Product>(viewModel));
                    await _uw.Commit();
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(viewModel);
        }


        [HttpGet, AjaxOnly(), DisplayName("حذف"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> Delete(string productId)
        {
            if (!productId.HasValue())
                ModelState.AddModelError(string.Empty, ProductNotFound);
            else
            {
                var product = await _uw.BaseRepository<Product>().FindByIdAsync(productId);

                if (product == null)
                    ModelState.AddModelError(string.Empty, ProductNotFound);
                else
                    return PartialView("_DeleteConfirmation", product);
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("Delete"), AjaxOnly(), ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Product model)
        {
            if (model.ProductId == null)
                ModelState.AddModelError(string.Empty, ProductNotFound);
            else
            {
                var product = await _uw.BaseRepository<Product>().FindByIdAsync(model.ProductId);

                if (product == null)
                    ModelState.AddModelError(string.Empty, ProductNotFound);
                else
                {
                    FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{product.ImageName}");
                    _uw.BaseRepository<Product>().Delete(product);
                    await _uw.Commit();
                    TempData["notification"] = DeleteSuccess;
                    return PartialView("_DeleteConfirmation", product);
                }
            }

            return PartialView("_DeleteConfirmation");
        }


        [HttpPost, ActionName("DeleteGroup"), AjaxOnly(), DisplayName("حذف گروهی"), Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> DeleteGroupConfirmed(string[] btSelectItem)
        {
            if (btSelectItem.Count() == 0)
                ModelState.AddModelError(string.Empty, "هیچ محصولی برای حذف انتخاب نشده است.");
            else
            {
                foreach (var item in btSelectItem)
                {
                    var product = await _uw.BaseRepository<Product>().FindByIdAsync(item);
                    _uw.BaseRepository<Product>().Delete(product);
                    await _uw.Commit();
                    FileExtensions.DeleteFile($"{_env.WebRootPath}/images/{product.ImageName}");
                }
                TempData["notification"] = DeleteGroupSuccess;
            }

            return PartialView("_DeleteGroup");
        }    
    }
}
