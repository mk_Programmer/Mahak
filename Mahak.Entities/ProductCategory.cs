﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Mahak.Entities
{
    public class ProductCategory
    {
        public string ProductId { get; set; }
        public string CategoryId { get; set; }

        public Product Product { get; set; }
        public Category Category { get; set; }
    }
}
