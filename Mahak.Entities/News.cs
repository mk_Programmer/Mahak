﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Mahak.Entities.Identity;

namespace Mahak.Entities
{
    public class News
    {
        [Key]
        public string NewsId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string ImageName { get; set; }
        public bool IsPublish { get; set; }
        public string Abstract { get; set; }
        public DateTime? PublishDateTime { get; set; }
        public TextLocation TextLocations { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Visit> Visits { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<NewsTag> NewsTags { get; set; }
    }


    public enum TextLocation
    {
        [Display(Name = "خبر")]
        News = 1,

        [Display(Name = "معرفی محک")]
        Introduction = 2,

        [Display(Name = "تاریخچه")]
        History = 3,
    }
}
