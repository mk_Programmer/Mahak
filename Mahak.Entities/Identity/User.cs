﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Mahak.Entities.Identity
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public string Bio { get; set; }
        public string ShowName { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? RegisterDateTime { get; set; }
        public GenderType Gender { get; set; }

        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<UserRole> Roles { get; set; }
        public virtual ICollection<UserClaim> Claims { get; set; }
    }
    

    public enum GenderType
    {
        [Display(Name = "مرد")]
        Male = 1,

        [Display(Name = "زن")]
        Female = 2
    }
}
