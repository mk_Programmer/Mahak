﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Mahak.Entities.Identity;

namespace Mahak.Entities
{
    public class Product
    {
        [Key]
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public int Price { get; set; }
        public int Stock { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
        public virtual ICollection<ProductTag> ProductTags { get; set; }
    }
}
