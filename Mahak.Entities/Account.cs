﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mahak.Entities
{
    public class Account
    {
        [Key]
        public string AccountId { get; set; }
        public int UserId { get; set; }
        public long Amount { get; set; }
        public string HelpID { get; set; }
        public DateTime? RegisterDateTime { get; set; }

        public RequestType RequestTypes { get; set; }
        public PaymentType PaymentTypes { get; set; }
        public PortType PortTypes { get; set; }
    }


    #region Enums
    public enum RequestType
    {
        [Display(Name = "عمومی")]
        Public = 1,

        [Display(Name = "عضویت")]
        Register = 2,

        [Display(Name = "درمان‌یار")]
        Assistance = 3
    }


    public enum PaymentType
    {
        [Display(Name = "کفاره")]
        Kafaare = 1,

        [Display(Name = "فطریه")]
        Fetrieh = 2,
    }


    public enum PortType
    {
        [Display(Name = "ملت")]
        Melat = 1,

        [Display(Name = "سامان")]
        Saman = 2,
    }
    #endregion
}