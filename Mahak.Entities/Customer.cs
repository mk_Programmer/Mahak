﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using Microsoft.AspNetCore.Mvc.Rendering;

namespace Mahak.Entities
{
    public class Customer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerId { get; set; }
        public string FirstName1 { get; set; }
        public string LastName1 { get; set; }
        public string PhoneNumber1 { get; set; }
        public string Company1 { get; set; }
        public string Address1 { get; set; }
        public string PostalCode1 { get; set; }
        public string Email { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Company2 { get; set; }
        public string Address2 { get; set; }
        public string ContinueAddress { get; set; }
        public string PostalCode2 { get; set; }

        public string City1 { get; set; }
        //public ICollection<SelectListItem> Province1 { get; set; }
        public string Province1 { get; set; }
        public string City2 { get; set; }
        public string Province2 { get; set; }


        public virtual ICollection<Order> Orders { get; set; }
    }
}
