﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mahak.Entities
{
    public class Category
    {
        [Key]
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }

        [ForeignKey("Parent")]
        public string ParentCategoryId { get; set; }
        public string Url { get; set; }
        public string ImageName { get; set; }
        public string Description { get; set; }

        public virtual Category Parent { get; set; }
        public virtual List<Category> Categories { get; set; }
        public ICollection<ProductCategory> ProductCategories { get; set; }
    }
}
