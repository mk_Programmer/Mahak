﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mahak.Entities
{
    public class Order
    {
        [Key]
        public string OrderId { get; set; }
        public int AmountPaid { get; set; }
        public int Quantity { get; set; }
        public string DispatchNumber { get; set; }
        public string OrderStatus { get; set; }
        public DateTime? PurchaseDateTime { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
