﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mahak.Entities
{
    public class MessageUsers
    {
        [Key]
        public string MessageId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool? IsRegisterCode { get; set; }
        public bool IsAnswer { get; set; }
        public DateTime? RegisterDateTime { get; set; }
        
        public MessageType MessageTypes { get; set; }
    }


    public enum MessageType
    {
        [Display(Name = "پیشنهادات")]
        Offers = 1,

        [Display(Name = "انتقادات")]
        Feedback = 2,

        [Display(Name = "سوالات")]
        Questions = 3,

        [Display(Name = "سایر پیام‌ها")]
        Other = 4
    }
}
