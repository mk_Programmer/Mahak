﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mahak.Entities
{
    public class Tag
    {
        [Key]
        public string TagId { get; set; }
        public string TagName { get; set; }

        public virtual ICollection<NewsTag> NewsTags { get; set; }
        public virtual ICollection<ProductTag> ProductTags { get; set; }
    }
}
