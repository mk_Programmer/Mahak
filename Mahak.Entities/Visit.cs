﻿using System;

namespace Mahak.Entities
{
    public class Visit
    {
        public string NewsId { get; set; }
        public string IpAddress { get; set; }
        public int NumberOfVisit { get; set; }
        public DateTime LastVisitDateTime { get; set; }

        public virtual News News { get; set; }
    }
}
