﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mahak.Entities
{
    public class Slider
    {
        [Key]
        public string SliderId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImageName { get; set; }
        public SliderLocation SliderLocations { get; set; }
    }


    public enum SliderLocation
    {
        [Display(Name = "صفحه اول")]
        Home = 1,

        [Display(Name = "صفحه دسته بندی ها")]
        CategoriesPaginate = 2,
    }
}
