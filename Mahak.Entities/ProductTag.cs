﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Mahak.Entities
{
    public class ProductTag
    {
        public string ProductId { get; set; }
        public string TagId { get; set; }

        public Product Product { get; set; }
        public Tag Tag { get; set; }
    }
}
