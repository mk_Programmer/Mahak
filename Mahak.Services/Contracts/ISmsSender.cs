﻿using System.Threading.Tasks;

namespace Mahak.Services.Contracts
{
    public interface ISmsSender
    {
        Task<string> SendAuthSmsAsync(string Code, string PhoneNumber);
    }
}
