﻿using Mahak.ViewModel.DynamicAccess;
using System.Collections.Generic;

namespace Mahak.Services.Contracts
{
    public interface IMvcActionsDiscoveryService
    {
        ICollection<ControllerViewModel> GetAllSecuredControllerActionsWithPolicy(string policyName);
    }
}
