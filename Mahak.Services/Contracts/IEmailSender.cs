﻿using System.Threading.Tasks;

namespace Mahak.Services.Contracts
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
