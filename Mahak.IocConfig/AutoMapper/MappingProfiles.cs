﻿using AutoMapper;
using Mahak.Entities;
using Mahak.Entities.Identity;
using Mahak.ViewModel.Tag;
using Mahak.ViewModel.News;
using Mahak.ViewModel.Order;
using Mahak.ViewModel.Slider;
using Mahak.ViewModel.Manage;
using Mahak.ViewModel.Product;
using Mahak.ViewModel.Account;
using Mahak.ViewModel.Customer;
using Mahak.ViewModel.Category;
using Mahak.ViewModel.Comments;
using Mahak.ViewModel.RoleManager;
using Mahak.ViewModel.UserManager;

namespace Mahak.IocConfig.AutoMapper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<MessageUsers, MessageUsersViewModel>().ReverseMap();

            CreateMap<Product, ProductViewModel>().ReverseMap();

            CreateMap<Customer, CustomerViewModel>().ReverseMap();

            CreateMap<News, NewsViewModel>().ReverseMap();

            CreateMap<Comment, CommentViewModel>().ReverseMap();

            CreateMap<Slider, SliderViewModel>().ReverseMap();

            CreateMap<Tag, TagViewModel>().ReverseMap()
                .ForMember(p => p.NewsTags, opt => opt.Ignore())
                .ForMember(p => p.ProductTags, opt => opt.Ignore());

            CreateMap<Order, OrderViewModel>().ReverseMap()
                .ForMember(p => p.Customer, opt => opt.Ignore())
                .ForMember(p => p.ProductOrders, opt => opt.Ignore());

            CreateMap<Category, CategoryViewModel>().ReverseMap()
                .ForMember(p => p.Parent, opt => opt.Ignore())
                .ForMember(p => p.Categories, opt => opt.Ignore())
                .ForMember(p => p.ProductCategories, opt => opt.Ignore());

            CreateMap<Role, RolesViewModel>().ReverseMap()
                .ForMember(p => p.Users, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore());

            CreateMap<User, UsersViewModel>().ReverseMap()
                .ForMember(p => p.News, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore());

            CreateMap<User, MessageUsersViewModel>().ReverseMap()
                .ForMember(p => p.News, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore());

            CreateMap<User, EmailViewModel>().ReverseMap()
                .ForMember(p => p.News, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore()); ;

            CreateMap<User, EditInfoViewModel>().ReverseMap()
                .ForMember(p => p.News, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore());

            CreateMap<User, ProfileViewModel>().ReverseMap()
                .ForMember(p => p.News, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore());

            CreateMap<User, UserPanelViewModel>().ReverseMap()
                .ForMember(p => p.News, opt => opt.Ignore())
                .ForMember(p => p.Claims, opt => opt.Ignore());
        }
    }
}
