﻿using Microsoft.Extensions.DependencyInjection;
using Mahak.Services;
using Mahak.Data.Contracts;
using Mahak.Data.UnitOfWork;
using Mahak.Data.Repositories;
using Mahak.Services.Contracts;

namespace Mahak.IocConfig.Extensions
{
    public static class AddCustomServicesExtensions
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ICommentRepository, CommentRepository>();
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<ISmsSender, SmsSender>();
            services.AddTransient<SendWeeklyNewsletter>();

            return services;
        }
    }
}
