﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Mahak.ViewModel.Customer
{
    public class CustomerViewModel
    {
        [JsonPropertyName("Id")]
        public int CustomerId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "نام 1"), JsonPropertyName("نام 1")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string FirstName1 { get; set; }


        [Display(Name = "نام خانوادگی 1"), JsonPropertyName("نام خانوادگی 1")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string LastName1 { get; set; }


        [Display(Name = "آدرس 1"), JsonPropertyName("آدرس 1")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string Address1 { get; set; }


        [Display(Name = "نام شرکت 1"), JsonIgnore]
        public string Company1 { get; set; }


        [Display(Name = "شماره همراه"), JsonPropertyName("شماره همراه")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string PhoneNumber1 { get; set; }


        [Display(Name = "کد پستی 1"), JsonPropertyName("کد پستی 1")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string PostalCode1 { get; set; }


        [Display(Name = "کد پستی 2"), JsonPropertyName("کد پستی 2")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string PostalCode2 { get; set; }


        [Display(Name = "استان"), JsonIgnore]
        //public ICollection<SelectListItem> Province1 { get; set; }
        public string Province1 { get; set; }


        [Display(Name = "شهر"), JsonIgnore]
        public string City1 { get; set; }


        [Display(Name = "ایمیل"), JsonPropertyName("ایمیل")]
        public string Email { get; set; }


        [Display(Name = "نام 2"), JsonPropertyName("نام 2")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string FirstName2 { get; set; }


        [Display(Name = "نام خانوادگی 2"), JsonPropertyName("نام خانوادگی 2")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string LastName2 { get; set; }


        [Display(Name = "شماره همراه"), JsonPropertyName("شماره همراه")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string PhoneNumber2 { get; set; }


        [Display(Name = " 2نام شرکت"), JsonIgnore]
        public string Company2 { get; set; }


        [Display(Name = "آدرس 2"), JsonPropertyName("آدرس 2")/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است.")*/]
        public string Address2 { get; set; }


        [JsonIgnore]
        public string ContinueAddress { get; set; }


        [Display(Name = "استان"), JsonIgnore]
        public string Province2 { get; set; }


        [Display(Name = "شهر"), JsonIgnore]
        public string City2 { get; set; }


        [JsonIgnore]
        public virtual ICollection<Entities.Order> Orders { get; set; }


        [JsonIgnore]
        public virtual ICollection<Entities.Product> Products { get; set; }
    }
}
