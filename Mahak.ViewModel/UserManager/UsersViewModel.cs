﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Mahak.Entities.Identity;
using System.ComponentModel;

namespace Mahak.ViewModel.UserManager
{
    public class UsersViewModel
    {
        [JsonPropertyName("Id")]
        public int? Id { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "تصویر پروفایل"), JsonPropertyName("تصویر")]
        public string Image { get; set; }


        [Display(Name = "تصویر پروفایل"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public IFormFile ImageFile { get; set; }


        [Display(Name = "نام کاربری"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("نام کاربری")]
        public string UserName { get; set; }


        [Display(Name = "ایمیل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("ایمیل"), EmailAddress(ErrorMessage = "ایمیل وارد شده صحیح نمی باشد.")]
        public string Email { get; set; }


        [DataType(DataType.Password), Display(Name = "کلمه عبور"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore, StringLength(100, ErrorMessage = "{0} باید دارای حداقل {2} کاراکتر و حداکثر دارای {1} کاراکتر باشد.", MinimumLength = 6)]
        public string Password { get; set; }


        [DataType(DataType.Password), Display(Name = "تکرار کلمه عبور"), Compare("Password", ErrorMessage = "کلمه عبور وارد شده با تکرار کلمه عبور مطابقت ندارد."), JsonIgnore]
        public string ConfirmPassword { get; set; }


        [Display(Name = "شماره موبایل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("شماره تماس")]
        public string PhoneNumber { get; set; }


        [Display(Name = "نام"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("نام")]
        public string FirstName { get; set; }


        [Display(Name = "نام خانوادگی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("نام خانوادگی")]
        public string LastName { get; set; }


        [Display(Name = "تاریخ تولد"), JsonIgnore()]
        public DateTime? BirthDate { get; set; }


        [Display(Name = "تاریخ تولد"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("تاریخ تولد")]
        public string PersianBirthDate { get; set; }


        [Display(Name = "تاریخ عضویت"), JsonIgnore]
        public DateTime? RegisterDateTime { get; set; }


        [Display(Name = "تاریخ عضویت"), JsonPropertyName("تاریخ عضویت")]
        public string PersianRegisterDateTime { get; set; }


        [Display(Name = "فعال / غیرفعال"), JsonPropertyName("IsActive")]
        public bool IsActive { get; set; }


        [JsonPropertyName("جنسیت")]
        public string GenderName { get; set; }


        [Display(Name = "معرفی"), JsonPropertyName("معرفی")]
        public string Bio { get; set; }


        [JsonIgnore]
        public string IpAddress { get; set; }


        [Display(Name = "جنسیت"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public GenderType? Gender { get; set; }


        [JsonIgnore]
        public ICollection<UserRole> Roles { get; set; }


        [Display(Name = "نقش"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public int? RoleId { get; set; }


        [JsonPropertyName("نقش")]
        public string RoleName { get; set; }


        [JsonIgnore]
        public bool PhoneNumberConfirmed { get; set; }


        [JsonIgnore]
        public bool TwoFactorEnabled { get; set; }


        [JsonIgnore]
        public bool LockoutEnabled { get; set; }


        [JsonIgnore]
        public bool EmailConfirmed { get; set; }


        [JsonIgnore]
        public int AccessFailedCount { get; set; }


        [JsonIgnore]
        public DateTimeOffset? LockoutEnd { get; set; }


        [JsonIgnore]
        public bool CheckTwoFactor { get; set; }


        [JsonIgnore]
        public Account.UserPanelViewModel UserPanelViewModel { get; set; }


        [Display(Name = "نام نمایشی"), Required(ErrorMessage = "انتخاب {0} الزامی است."), DefaultValue("نام نمایشی"), JsonIgnore]
        public string ShowName { get; set; }


        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
            set
            {
                FullName = value;
            }
        }
    }


    public class EmailViewModel
    {
        [JsonPropertyName("Id")]
        public int? Id { get; set; }


        [Display(Name = "ایمیل قبلی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), EmailAddress(ErrorMessage = "ایمیل وارد شده صحیح نمی باشد.")]
        public string OldEmail { get; set; }


        [Display(Name = "ایمیل جدید"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), EmailAddress(ErrorMessage = "ایمیل وارد شده صحیح نمی باشد.")]
        public string NewEmail { get; set; }
    }
}
