﻿using System.ComponentModel.DataAnnotations;

namespace Mahak.ViewModel.Account
{
    public class SignInViewModel
    {
        public int UserId { get; set; }


        [Display(Name = "نام کاربری"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string UserName { get; set; }


        //[Display(Name = "شماره موبایل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        //public string PhoneNumber { get; set; }


        [DataType(DataType.Password), Display(Name = "کلمه عبور"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string Password { get; set; }


        [Display(Name = "مرا به خاطر بسپار؟")]
        public bool RememberMe { get; set; }


        //[GoogleRecaptchaValidation, BindProperty(Name = "g-recaptcha-response")]
        //public string GoogleRecaptchaResponse { get; set; }
    }
}
