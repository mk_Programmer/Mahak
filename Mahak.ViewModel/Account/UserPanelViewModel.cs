﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Mahak.Entities.Identity;

namespace Mahak.ViewModel.Account
{
    public class UserPanelViewModel
    {
        public UserPanelViewModel()
        {

        }
        public UserPanelViewModel(User userInfo)
        {
            UserInfo = userInfo;
        }


        public User UserInfo { get; set; }

        public Entities.Customer Customer { get; set; }

        public bool CheckTwoFactorAccount { get; set; }
        //////////////////////////////////////////
        #region SendCodeTwoFactor
        public string SelectedProvider { get; set; }

        public ICollection<SelectListItem> Providers { get; set; }
        #endregion
        //////////////////////////////////////////
        #region VerfiyCode
        public string Provider { get; set; }

        [Display(Name = "کد اعتبارسنجی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string Code { get; set; }

        [Display(Name = "مرا به خاطر بسپار؟")]
        public bool RememberBrowser { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
        #endregion
        //////////////////////////////////////////
        #region UserInfo
        public int? Id { get; set; }

        [Display(Name = "نام کاربری"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string UserName { get; set; }

        [Display(Name = "ایمیل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), EmailAddress(ErrorMessage = "ایمیل وارد شده صحیح نمی باشد.")]
        public string Email { get; set; }

        [Display(Name = "نام"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string LastName { get; set; }

        [Display(Name = "نام نمایشی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string ShowName { get; set; }

        [Display(Name = "شماره همراه"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string PhoneNumber { get; set; }

        [Display(Name = "کلمه عبور فعلی"), DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Display(Name = "کلمه عبور جدید"), DataType(DataType.Password), StringLength(100, ErrorMessage = "{0} باید دارای حداقل {2} کاراکتر و حداکثر دارای {1} کاراکتر باشد.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [Display(Name = "تکرار کلمه عبور جدید"), DataType(DataType.Password), Compare("NewPassword", ErrorMessage = "کلمه عبور وارد شده با تکرار کلمه عبور مطابقت ندارد.")]
        public string ConfirmNewPassword { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
            set
            {
                FullName = value;
            }
        }
        #endregion
    }


    public class ResetPasswordViewModel
    {
        [Display(Name = "کد اعتبارسنجی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string Code { get; set; }

        [Display(Name = "ایمیل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), EmailAddress(ErrorMessage = "ایمیل وارد شده صحیح نمی باشد.")]
        public string Email { get; set; }

        [Display(Name = "کلمه عبور جدید"), DataType(DataType.Password), StringLength(100, ErrorMessage = "{0} باید دارای حداقل {2} کاراکتر و حداکثر دارای {1} کاراکتر باشد.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [Display(Name = "تکرار کلمه عبور جدید"), DataType(DataType.Password), Compare("NewPassword", ErrorMessage = "کلمه عبور وارد شده با تکرار کلمه عبور مطابقت ندارد.")]
        public string ConfirmNewPassword { get; set; }
    }
}
