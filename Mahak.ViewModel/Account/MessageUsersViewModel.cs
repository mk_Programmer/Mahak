﻿using Mahak.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace Mahak.ViewModel.Account
{
    public class MessageUsersViewModel
    {
        [JsonPropertyName("Id")]
        public string MessageId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [JsonIgnore]
        public int UserId { get; set; }


        [Display(Name = "نام"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string FirstName { get; set; }


        [Display(Name = "نام خانوادگی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string LastName { get; set; }


        [JsonIgnore]
        public string PhoneNumber { get; set; }


        [JsonPropertyName("ایمیل")]
        public string Email { get; set; }


        [JsonPropertyName("متن پیام")]
        public string Description { get; set; }


        [JsonPropertyName("نام کامل"), JsonIgnore]
        public string FullName { get; set; }


        [JsonIgnore]
        public bool? IsRegisterCode { get; set; }


        [JsonIgnore]
        public DateTime? RegisterDateTime { get; set; }


        [JsonIgnore]
        public bool? IsAnswer { get; set; }


        [JsonPropertyName("تاریخ ثبت")]
        public string PersianRegisterDateTime { get; set; }





        [JsonIgnore]
        public string SelectedProvider { get; set; }


        [Display(Name = "کد شش رقمی"), JsonIgnore/*, Required(ErrorMessage = "وارد نمودن {0} الزامی است."), DefaultValue("00000")*/]
        public string CommentCode { get; set; }




        


        [Display(Name = "نوع پیام"), Required(ErrorMessage = "مشخص نمودن {0} الزامی است.")]
        public MessageType MessageTypes { get; set; }


        [JsonPropertyName("نوع پیام")]
        public string MessageType { get; set; }


        //[JsonIgnore]
        //public RequestType RequestTypes { get; set; }
        //[JsonIgnore]
        //public string RequestType { get; set; }
        //[JsonIgnore]
        //public PaymentType PaymentTypes { get; set; }
        //[JsonIgnore]
        //public string PaymentType { get; set; }
    }
}
