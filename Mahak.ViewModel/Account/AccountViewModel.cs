﻿using Mahak.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace Mahak.ViewModel.Account
{
    public class AccountViewModel
    {
        [JsonPropertyName("Id")]
        public string AccountId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [JsonIgnore]
        public int UserId { get; set; }


        [Display(Name = "شناسه نیکوکاری"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string HelpID { get; set; }


        [Display(Name = "نام"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string FirstName { get; set; }


        [Display(Name = "نام خانوادگی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string LastName { get; set; }


        [JsonIgnore]
        public string PhoneNumber { get; set; }


        [JsonPropertyName("ایمیل")]
        public string Email { get; set; }


        [JsonPropertyName("متن پیام")]
        public string Description { get; set; }


        [JsonPropertyName("نام کامل"), JsonIgnore]
        public string FullName { get; set; }


        [JsonIgnore]
        public DateTime? RegisterDateTime { get; set; }


        [JsonIgnore]
        public bool? IsAnswer { get; set; }


        [JsonPropertyName("تاریخ ثبت")]
        public string PersianRegisterDateTime { get; set; }


        public long Amount { get; set; }


        [Display(Name = "نوع درخواست"), Required(ErrorMessage = "مشخص نمودن {0} الزامی است.")]
        public RequestType RequestTypes { get; set; }


        [JsonPropertyName("نوع درخواست")]
        public string RequestType { get; set; }


        [Display(Name = "نوع پرداخت"), Required(ErrorMessage = "مشخص نمودن {0} الزامی است.")]
        public PaymentType PaymentTypes { get; set; }


        [JsonPropertyName("نوع پرداخت")]
        public string PaymentType { get; set; }


        [Display(Name = "درگاه پرداخت"), Required(ErrorMessage = "مشخص نمودن {0} الزامی است.")]
        public PortType PortTypes { get; set; }


        [JsonPropertyName("درگاه پرداخت")]
        public string PortType { get; set; }
    }
}
