﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Mahak.ViewModel.Account
{
    public class RegisterViewModel
    {
        [Display(Name = "نام کاربری"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")/*, PhoneValidate()*/]
        public string UserName { get; set; }


        [Display(Name = "شماره موبایل")]
        public string PhoneNumber { get; set; }


        [Display(Name = "نام خانوادگی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string FirstName { get; set; }


        [Display(Name = "نام"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string LastName { get; set; }


        [Display(Name = "ایمیل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), EmailAddress(ErrorMessage = "ایمیل وارد شده صحیح نمی باشد.")]
        public string Email { get; set; }


        [DataType(DataType.Password), Display(Name = "کلمه عبور"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), StringLength(100, ErrorMessage = "{0} باید دارای حداقل {2} کاراکتر و حداکثر دارای {1} کاراکتر باشد.", MinimumLength = 6)]
        public string Password { get; set; }


        [DataType(DataType.Password), Display(Name = "تکرار کلمه عبور"), Compare("Password", ErrorMessage = "کلمه عبور وارد شده با تکرار کلمه عبور مطابقت ندارد.")]
        public string ConfirmPassword { get; set; }
    }
}
