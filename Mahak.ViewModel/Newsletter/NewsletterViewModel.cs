﻿using System;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Mahak.ViewModel.Newsletter
{
    public class NewsletterViewModel
    {
        [Display(Name = "ایمیل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("Id"), EmailAddress(ErrorMessage = "ایمیل وارد شده معتبر نمی باشد.")]
        public string Email { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [JsonPropertyName("تاریخ عضویت")]
        public string PersianRegisterDateTime { get; set; }


        [JsonIgnore]
        public DateTime? RegisterDateTime { get; set; }


        [JsonPropertyName("IsActive")]
        public bool IsActive { get; set; }
    }
}
