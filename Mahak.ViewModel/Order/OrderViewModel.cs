﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using Mahak.Entities;
using Mahak.ViewModel.Product;

namespace Mahak.ViewModel.Order
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {

        }
        public OrderViewModel(List<OrderViewModel> orders)
        {
            OrderViewModels = orders;
        }
        public OrderViewModel(
            ProductViewModel product,
            OrderViewModel order,
            List<OrderViewModel> _orders)
        {
            ProductViewModel = product;
            Order = order;
            Orders = _orders;
        }


        [JsonPropertyName("Id")]
        public string OrderId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "مبلغ پرداخت"), JsonPropertyName("مبلغ پرداخت"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public int AmountPaid { get; set; }


        [Display(Name = "تعداد"), JsonPropertyName("تعداد")]
        public int Quantity { get; set; }


        [Display(Name = "قیمت کل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("قیمت کل")]
        public long Calculate { get; set; }


        [Display(Name = "مجموع سبد خرید"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("مجموع سبد خرید")]
        public int TotalCart { get; set; }


        [Display(Name = "شماره مرسوله"), JsonPropertyName("شماره مرسوله")]
        public string DispatchNumber { get; set; }


        [Display(Name = "وضعیت"), JsonPropertyName("وضعیت"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string OrderStatus { get; set; }


        [Display(Name = "تاریخ خرید")]
        public DateTime? PurchaseDateTime { get; set; }


        [JsonPropertyName("Id")]
        public int CustomerId { get; set; }


        [JsonIgnore]
        public List<Entities.Order> orders { get; set; }


        [JsonIgnore]
        public List<OrderViewModel> OrderViewModels { get; set; }


        [JsonIgnore]
        public string CustomerName { get; set; }


        [JsonIgnore]
        public virtual Entities.Customer Customer { get; set; }


        [JsonIgnore]
        public virtual Entities.Product Product { get; set; }


        [JsonIgnore]
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }




        [JsonIgnore]
        public string ProductId { get; set; }
        [JsonIgnore]
        public string ProductName { get; set; }
        [JsonIgnore]
        public string ProductCode { get; set; }
        [JsonIgnore]
        public string ProductUrl { get; set; }
        [JsonIgnore]
        public string ProductImageName { get; set; }
        [JsonIgnore]
        public int ProductPrice { get; set; }


        public ProductViewModel ProductViewModel { get; set; }
        public OrderViewModel Order { get; set; }
        public List<OrderViewModel> Orders { get; set; }
    }
}
