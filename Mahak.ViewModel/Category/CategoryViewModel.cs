﻿using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Mahak.Common.Attributes;

namespace Mahak.ViewModel.Category
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
                
        }
        public CategoryViewModel(string categoryId, string categoryName, string url, string parentCategoryId, string parentCategoryName)
        {
            CategoryId = categoryId;
            CategoryName = categoryName;
            Url = url;
            ParentCategoryId = parentCategoryId;
            ParentCategoryName = parentCategoryName;
        }


        [JsonPropertyName("Id")]
        public string CategoryId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "تصویر دسته"), JsonPropertyName("تصویر دسته")]
        public string ImageName { get; set; }


        [Display(Name = "تصویر دسته"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public IFormFile ImageFile { get; set; }


        [Display(Name ="عنوان دسته بندی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("دسته")]
        public string CategoryName { get; set; }


        [Display(Name = "عنوان نمایشی"), JsonPropertyName("عنوان نمایشی")]
        public string Title { get; set; }


        [JsonIgnore]
        public string ParentCategoryId { get; set; }


        [Display(Name ="دسته پدر"), JsonPropertyName("دسته پدر")]
        public string ParentCategoryName { get; set; }


        [Display(Name = "توضیحات"), JsonPropertyName("توضیحات")]
        public string Description { get; set; }


        [Display(Name = "آدرس دسته"), JsonPropertyName("آدرس"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), UrlValidate("/", @"\", " ")]
        public string Url { get; set; }
    }
}
