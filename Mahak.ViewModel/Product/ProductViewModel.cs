﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
using Mahak.Entities;
using Mahak.Common.Attributes;

namespace Mahak.ViewModel.Product
{
    public class ProductViewModel
    {
        [JsonPropertyName("Id")]
        public string ProductId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "تصویر محصول"), JsonPropertyName("تصویر محصول")]
        public string ImageName { get; set; }


        [Display(Name = "تصویر محصول"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public string ImageFile { get; set; }


        [Display(Name = "نام محصول"),  JsonPropertyName("نام محصول"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string ProductName { get; set; }


        [Display(Name = "کد محصول"), JsonPropertyName("کد محصول"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string ProductCode { get; set; }


        [Display(Name = "موجودی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("موجودی")]
        public int Stock { get; set; }


        [Display(Name = "قیمت"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("قیمت")]
        public long Price { get; set; }


        [Display(Name = "تاریخ ساخت"), JsonIgnore()]
        public DateTime? CreateDate { get; set; }


        [Display(Name = "تاریخ ساخت"), JsonPropertyName("تاریخ ساخت"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string PersianCreateDate { get; set; }


        [Display(Name = "دسته"), JsonPropertyName("دسته"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string NameOfCategories { get; set; }


        [JsonPropertyName("تعداد دیدگاه ها")]
        public int NumberOfComments { get; set; }


        [Display(Name = "توضیحات محصول"), JsonPropertyName("توضیحات محصول")]
        public string Description { get; set; }      


        [Display(Name = "آدرس"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("آدرس"), UrlValidate("/", @"\", " ")]
        public string Url { get; set; }


        [JsonIgnore]
        public string[] CategoryIds { get; set; }


        [JsonIgnore]
        public string IdOfCategories { get; set; }


        [JsonIgnore]
        public ProductCategoriesViewModel ProductCategoriesViewModel { get; set; }


        [JsonIgnore]
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}
