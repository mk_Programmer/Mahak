﻿using System.Collections.Generic;
using Mahak.ViewModel.Category;

namespace Mahak.ViewModel.Product
{
    public class ProductCategoriesViewModel
    {
        public ProductCategoriesViewModel(List<TreeViewCategory> categories, string[] categoryIds)
        {
            Categories = categories;
            CategoryIds = categoryIds;
        }


        public List<TreeViewCategory> Categories { get; set; }
        public string[] CategoryIds { get; set; }
    }
}
