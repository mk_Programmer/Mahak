﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Mahak.ViewModel.Comments
{
    public class CommentViewModel
    {
        public CommentViewModel() { }
        public CommentViewModel(string parentCommentId, string newsId, string productId)
        {
            ParentCommentId = parentCommentId;
            NewsId = newsId;
            ProductId = productId;
        }


        [JsonPropertyName("Id")]
        public string CommentId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "نام"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("نام")]
        public string Name { get; set; }


        [Display(Name = "ایمیل"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("ایمیل"), EmailAddress(ErrorMessage = "ایمیل وارد شده معتبر نمی باشد.")]
        public string Email { get; set; }


        [Display(Name = "دیدگاه"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("دیدگاه")]
        public string Description { get; set; }


        [JsonIgnore]
        public string NewsId { get; set; }


        [JsonIgnore]
        public string ProductId { get; set; }


        [JsonPropertyName("IsConfirm")]
        public bool? IsConfirm { get; set; }


        [JsonIgnore]
        public DateTime? PostageDateTime { get; set; }


        [JsonPropertyName("تاریخ ارسال")]
        public string PersianPostageDateTime { get; set; }


        [JsonIgnore]
        public string ParentCommentId { get; set; }


        [JsonIgnore]
        public virtual List<CommentViewModel> comments { get; set; }
    }
}
