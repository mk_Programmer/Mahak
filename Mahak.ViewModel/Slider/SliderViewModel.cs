﻿using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Mahak.Entities;
using System.Collections.Generic;

namespace Mahak.ViewModel.Slider
{
    public class SliderViewModel
    {
        [JsonPropertyName("Id")]
        public string SliderId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "تصویر اسلایدر"), JsonPropertyName("تصویر اسلایدر")]
        public string ImageName { get; set; }


        [Display(Name = "تصویر اسلایدر"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public IFormFile ImageFile { get; set; }


        [Display(Name = "عنوان اسلایدر"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("عنوان اسلایدر")]
        public string Title { get; set; }


        [Display(Name = "آدرس"), JsonPropertyName("آدرس"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string Url { get; set; }


        [JsonPropertyName("جایگاه اسلایدر")]
        public string Locations { get; set; }


        [Display(Name = "جایگاه اسلایدر"), Required(ErrorMessage = "مشخص نمودن {0} الزامی است.")]
        public SliderLocation SliderLocations { get; set; }


        //////////////////////////////////////////////////////


        //[Display(Name = "عنوان اسلایدر"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("عنوان اسلایدر")]
        //public string TitleCategory { get; set; }


        //[Display(Name = "آدرس اسلایدر"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), Url(ErrorMessage = "آدرس وارد شده نا معتبر است.")]
        //public string UrlCategory { get; set; }


        //public string PosterCategory { get; set; }


        //[Display(Name = "پوستر اسلایدر"), JsonIgnore]
        //public IFormFile PosterFileCategory { get; set; }


        //[JsonIgnore]
        //public DateTime? PublishDateTimeCategory { get; set; }


        //[JsonPropertyName("تاریخ انتشار")]
        //public string PersianPublishDateTimeCategory { get; set; }
    }
}
