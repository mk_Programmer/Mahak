﻿using System.Collections.Generic;
using Mahak.ViewModel.Category;
using Mahak.ViewModel.Product;

namespace Mahak.ViewModel.Home
{
    public class ProductsPaginateViewModel
    {
        public ProductsPaginateViewModel(int productCount, List<ProductViewModel> products)
        {
            ProductCount = productCount;
            Products = products;           
        }


        public int ProductCount { get; set; }
        public List<ProductViewModel> Products { get; set; }      
    }
}
