﻿using System.Collections.Generic;
using Mahak.Entities;
using Mahak.ViewModel.Slider;
using Mahak.ViewModel.Product;
using Mahak.ViewModel.Category;
using Mahak.Entities.Identity;
using Mahak.ViewModel.Order;

namespace Mahak.ViewModel.Home
{
    public class ProductDetailsViewModel
    {
        #region Constructore
        public ProductDetailsViewModel()
        {

        }
        public ProductDetailsViewModel(Entities.Order order)
        {
            OrderDb = order;
        }
        public ProductDetailsViewModel(
            int categoryCount,
            int productCount,
            ProductViewModel product,
            List<ProductViewModel> products,
            CategoryViewModel category,
            List<CategoryViewModel> categories,
            OrderViewModel order,
            List<Comment> comments,
            List<SliderViewModel> sliders,
            List<ProductInCategoriesAndTagsViewModel> viewModels)
        {
            CategoryCount = categoryCount;
            ProductCount = productCount;
            Product = product;
            Products = products;
            Category = category;
            Categories = categories;
            Order = order;
            Comments = comments;
            Sliders = sliders;
            ViewModels = viewModels;
        }
        public ProductDetailsViewModel(
            ProductViewModel product,
            OrderViewModel order,
            List<OrderViewModel> orders)
        {
            Product = product;
            Order = order;
            Orders = orders;
        }
        #endregion


        public int CategoryCount { get; set; }
        public int ProductCount { get; set; }
        public ProductViewModel Product { get; set; }
        public List<ProductViewModel> Products { get; set; }
        public CategoryViewModel Category { get; set; }
        public List<CategoryViewModel> Categories { get; set; }
        public List<Comment> Comments { get; set; }
        public List<SliderViewModel> Sliders { get; set; }
        public OrderViewModel Order { get; set; }
        public List<OrderViewModel> Orders { get; set; }
        public List<ProductInCategoriesAndTagsViewModel> ViewModels { get; set; }
        public User User { get; set; }
        public Entities.Order OrderDb { get; set; }
    }
}
