﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Mahak.ViewModel.Home
{
    public class CategoryOrTagInfoViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool IsCategory { get; set; }
    }
}
