﻿using System.Collections.Generic;
using Mahak.Entities;
using Mahak.ViewModel.News;

namespace Mahak.ViewModel.Home
{
    public class NewsDetailsViewModel
    {
        public NewsDetailsViewModel(NewsViewModel news, List<Comment> comments)
        {
            News = news;
            Comments = comments;
        }


        public NewsViewModel News { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
