﻿using System.Collections.Generic;
using Mahak.ViewModel.Slider;
using Mahak.ViewModel.Category;

namespace Mahak.ViewModel.Home
{
    public class CategoriesPaginateViewModel
    {
        public CategoriesPaginateViewModel(int categoryCount, CategoryViewModel categories, List<SliderViewModel> sliders)
        {
            CategoryCount = categoryCount;
            Categories = categories;
            Sliders = sliders;
        }


        public int CategoryCount { get; set; }
        public CategoryViewModel Categories { get; set; }
        public List<SliderViewModel> Sliders { get; set; }
    }
}
