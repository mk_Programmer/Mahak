﻿using System.Collections.Generic;
using Mahak.ViewModel.News;

namespace Mahak.ViewModel.Home
{
    public class NewsPaginateViewModel
    {
        public NewsPaginateViewModel(int newsCount, List<NewsViewModel> news)
        {
            NewsCount = newsCount;
            News = news;
        }


        public int NewsCount { get; set; }
        public List<NewsViewModel> News { get; set; }
    }
}
