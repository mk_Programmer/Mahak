﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Mahak.ViewModel.Home
{
    public  class ProductInCategoriesAndTagsViewModel
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public long Price { get; set; }
        public int Stock { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
        public string Url { get; set; }
        public bool? IsDelete { get; set; }
        public bool IsProvide { get; set; }
        public DateTime? CreateDate { get; set; }
        public string PersianCreateDate { get; set; }
        public string NameOfCategories { get; set; }
        public int NumberOfComments { get; set; }
    }
}
