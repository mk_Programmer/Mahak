﻿using System.Collections.Generic;
using Mahak.Entities.Identity;
using Mahak.ViewModel.News;
using Mahak.ViewModel.Product;
using Mahak.ViewModel.Slider;

namespace Mahak.ViewModel.Home
{
    public class HomePageViewModel
    {
        public HomePageViewModel(List<SliderViewModel> sliders, User _user, List<NewsViewModel> news, /*List<NewsViewModel> mostViewedNews, List<Entities.Comment> comments,*/ List<ProductViewModel> products, bool requiresTwoFactor = false)
        {
            user = _user;
            News = news;
            //MostViewedNews = mostViewedNews;
            //Comments = comments;
            Products = products;
            Sliders = sliders;
            user.TwoFactorEnabled = requiresTwoFactor;
        }


        public User user { get; set; }
        public List<NewsViewModel> News { get; set; }
        //public List<NewsViewModel> MostViewedNews { get; set; }
        //public List<Entities.Comment> Comments { get; set; }
        public List<ProductViewModel> Products { get; set; }
        public List<SliderViewModel> Sliders { get; set; }
    }
}
