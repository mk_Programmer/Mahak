﻿using Microsoft.EntityFrameworkCore;

namespace Mahak.Data.Mapping
{
    public static class MahakMapping
    {
        public static void AddCustomMahakMappings(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductCategoryMapping());
            modelBuilder.ApplyConfiguration(new ProductOrderMapping());
            modelBuilder.ApplyConfiguration(new ProductTagMapping());
            modelBuilder.ApplyConfiguration(new NewsTagMapping());
            modelBuilder.ApplyConfiguration(new VisitMapping());
        }
    }
}
