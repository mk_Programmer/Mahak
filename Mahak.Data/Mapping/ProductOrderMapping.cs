﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Mahak.Entities;

namespace Mahak.Data.Mapping
{
    public class ProductOrderMapping : IEntityTypeConfiguration<ProductOrder>
    {
        public void Configure(EntityTypeBuilder<ProductOrder> builder)
        {
            builder.HasKey(t => new { t.OrderId, t.ProductId });
            builder
              .HasOne(p => p.Order)
              .WithMany(t => t.ProductOrders)
              .HasForeignKey(f => f.OrderId);

            builder
               .HasOne(p => p.Product)
               .WithMany(t => t.ProductOrders)
               .HasForeignKey(f => f.ProductId);
        }
    }
}
