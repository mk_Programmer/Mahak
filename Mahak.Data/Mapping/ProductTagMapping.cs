﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Mahak.Entities;

namespace Mahak.Data.Mapping
{
    public class ProductTagMapping : IEntityTypeConfiguration<ProductTag>
    {
        public void Configure(EntityTypeBuilder<ProductTag> builder)
        {
            builder.HasKey(t => new { t.ProductId , t.TagId });
            builder
              .HasOne(p => p.Product)
              .WithMany(t => t.ProductTags)
              .HasForeignKey(f => f.ProductId);

            builder
               .HasOne(p => p.Tag)
               .WithMany(t => t.ProductTags)
               .HasForeignKey(f => f.TagId);
        }
    }
}
