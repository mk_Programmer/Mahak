﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Mahak.Entities;
using Mahak.ViewModel.Category;

namespace Mahak.Data.Contracts
{
    public interface ICategoryRepository
    {
        int CountCategories();
        Category FindByCategoryName(string categoryName);
        Task<List<TreeViewCategory>> GetAllCategoriesAsync();
        bool IsExistCategory(string categoryName, string recentCategoryId = null);
        Task<List<CategoryViewModel>> GetPaginateCategoriesAsync(int offset, int limit,string orderBy, string searchText);
        Task<List<CategoryViewModel>> GetParentInfoAsync(int offset, int limit, string orderBy);
        Task<CategoryViewModel> GetCategoryByIdAsync(string categorytId);

        Task<CategoryViewModel> GetCategoryTitleByIdAsync(string categoryId);
    }
}
