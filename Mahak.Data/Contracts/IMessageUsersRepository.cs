﻿using Mahak.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mahak.Data.Contracts
{
    public interface IMessageUsersRepository
    {
        int CountUnAnsweredComments();
        Task<List<MessageUsersViewModel>> GetPaginateMessagesAsync(int offset, int limit, string orderBy, string searchText, int userId, bool? isRegisterCode);
    }
}
