﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Mahak.ViewModel.Slider;

namespace Mahak.Data.Contracts
{
    public interface ISliderRepository
    {
        Task<List<SliderViewModel>> GetPaginateSlidersAsync(int offset, int limit, string orderBy, string searchText);
        string CheckSliderFileName(string fileName);
    }
}
