﻿using System.Threading.Tasks;

namespace Mahak.Data.Contracts
{
    public interface IUnitOfWork
    {
        MahakDBContext Context { get; }
        IBaseRepository<TEntity> BaseRepository<TEntity>() where TEntity : class;
        IMessageUsersRepository MessageUsersRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IProductRepository ProductRepository { get; }
        INewsRepository NewsRepository { get; }
        ITagRepository TagRepository { get; }
        ICommentRepository CommentRepository { get; }
        INewsletterRepository NewsletterRepository { get; }
        ISliderRepository SliderRepository { get; }
        Task Commit();
    }
}
