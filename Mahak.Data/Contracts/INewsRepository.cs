﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Mahak.Entities;
using Mahak.ViewModel.Home;
using Mahak.ViewModel.News;

namespace Mahak.Data.Contracts
{
    public interface INewsRepository
    {
        string CheckNewsFileName(string fileName);
        Task<List<NewsViewModel>> GetPaginateNewsAsync(int offset, int limit, string orderBy, string searchText, bool? isPublish, bool? isInternal);
        Task<List<NewsViewModel>> MostViewedNewsAsync(int offset, int limit, string duration);
        Task<NewsViewModel> GetNewsByIdAsync(string newsId,int userId);
        Task<List<Comment>> GetNewsCommentsAsync(string newsId);
        Task BindSubComments(Comment comment);
        Task<List<NewsInCategoriesAndTagsViewModel>> GetNewsInTagAsync(string TagId, int pageIndex, int pageSize);
        Task<string> GetWeeklyNewsAsync();
        Task<List<NewsViewModel>> SearchInNews(string textSearch);
        Task InsertVisitOfUserAsync(string newsId, string ipAddress);
        int CountNews();
        int CountFuturePublishedNews();
        int CountNewsPublishedOrDraft(bool isPublish);
        int CountNewsPublished();




        Task<NewsViewModel> GetNewsByLocationAsync(TextLocation location);
    }
}
