﻿using Mahak.Entities;
using Mahak.ViewModel.Newsletter;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mahak.Data.Contracts
{
    public interface INewsletterRepository
    {
        Task<List<NewsletterViewModel>> GetPaginateNewsletterAsync(int offset, int limit,string orderBy, string searchText);
    }
}
