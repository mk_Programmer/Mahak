﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Mahak.Entities;
using Mahak.ViewModel.Home;
using Mahak.ViewModel.Product;
using Mahak.ViewModel.Customer;
using Mahak.ViewModel.Order;

namespace Mahak.Data.Contracts
{
    public interface IProductRepository
    {
        Task<List<ProductViewModel>> GetPaginateProductAsync(int offset, int limit, string orderBy, string searchText);
        string CheckProductFileName(string fileName);
        Task<ProductViewModel> GetProductByIdAsync(string productId, int userId);
        Task<List<Comment>> GetProductCommentsAsync(string productId);
        Task<List<ProductInCategoriesAndTagsViewModel>> GetProductInCategoryAsync(string categoryId, int pageIndex, int pageSize);
        Task<List<ProductInCategoriesAndTagsViewModel>> GetProductInTagAsync(string TagId, int pageIndex, int pageSize);
        Task<string> GetWeeklyProductAsync();
        Task<List<ProductViewModel>> SearchInProduct(string textSearch);
        int CountProduct();
        int CountAvailableProduct();
        int CountUnAvailableProduct();


        Task<CustomerViewModel> GetCustomerIdAsync(int userId);
        int GetOrderByCustomerId(int userId);
        Order GetOrderIdByCustomerId(int userId);
        Task<List<OrderViewModel>> GetPaginateOrdersAsync(int offset, int limit, string orderBy);
        Task<OrderViewModel> GetOrderByIdAsync(string ordertId);
    }
}
