﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Mahak.Entities;
using Mahak.Data.Mapping;
using Mahak.Entities.Identity;

namespace Mahak.Data
{
    public class MahakDBContext : IdentityDbContext<User,Role,int,UserClaim,UserRole,IdentityUserLogin<int>,RoleClaim,IdentityUserToken<int>>
    {
        public MahakDBContext(DbContextOptions options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.AddCustomMahakMappings();
            builder.AddCustomIdentityMappings();

            builder.Entity<User>().Property(b => b.RegisterDateTime).HasDefaultValueSql("CONVERT(DATETIME, CONVERT(VARCHAR(20),GetDate(), 120))");
            builder.Entity<Account>().Property(b => b.RegisterDateTime).HasDefaultValueSql("CONVERT(DATETIME, CONVERT(VARCHAR(20),GetDate(), 120))");
            builder.Entity<MessageUsers>().Property(b => b.RegisterDateTime).HasDefaultValueSql("CONVERT(DATETIME, CONVERT(VARCHAR(20),GetDate(), 120))");
            builder.Entity<Newsletter>().Property(b => b.RegisterDateTime).HasDefaultValueSql("CONVERT(DATETIME, CONVERT(VARCHAR(20),GetDate(), 120))");
            
            builder.Entity<User>().Property(b => b.IsActive).HasDefaultValueSql("1");
            builder.Entity<Newsletter>().Property(b => b.IsActive).HasDefaultValueSql("1");
            builder.Entity<MessageUsers>().Property(b => b.IsAnswer).HasDefaultValueSql("0");
        }


        public virtual DbSet<Account> Accounts { set; get; }
        public virtual DbSet<MessageUsers> MessageUsers { set; get; }
        public virtual DbSet<Customer> Customers { set; get; }
        public virtual DbSet<Product> Products { set; get; }
        public virtual DbSet<Order> Orders { set; get; }
        public virtual DbSet<ProductOrder> ProductOrders { set; get; }
        public virtual DbSet<Category> Categories { set; get; }
        public virtual DbSet<ProductCategory> ProductCategories { set; get; }
        public virtual DbSet<News> News { set; get; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<NewsTag> NewsTags { get; set; }
        public virtual DbSet<ProductTag> ProductTags { get; set; }
        public virtual DbSet<Newsletter> Newsletters { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Visit> Visits { get; set; }
        public virtual DbSet<Slider> Sliders { get; set; }
    }
}
