﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Mahak.Data.Contracts;
using Mahak.Data.Repositories;

namespace Mahak.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public MahakDBContext Context { get; }
        private IMapper _mapper;
        private readonly IConfiguration _configuration;
        private IMessageUsersRepository _messageUsersRepository;
        private ICategoryRepository _categoryRepository;
        private IProductRepository _productRepository;
        private INewsRepository _newsRepository;
        private ITagRepository _tagRepository;
        private ICommentRepository _commentRepository;
        private INewsletterRepository _newsletterRepository;
        private ISliderRepository _sliderRepository;
        public UnitOfWork(MahakDBContext context, IMapper mapper, IConfiguration configuration)
        {
            Context = context;
            _mapper = mapper;
            _configuration = configuration;
        }


        public IBaseRepository<TEntity> BaseRepository<TEntity>() where TEntity : class
        {
            IBaseRepository<TEntity> repository = new BaseRepository<TEntity,MahakDBContext>(Context);
            return repository;
        }


        public IMessageUsersRepository MessageUsersRepository
        {
            get
            {
                if (_messageUsersRepository == null)
                    _messageUsersRepository = new MessageUsersRepository(Context);

                return _messageUsersRepository;
            }
        }


        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                    _categoryRepository = new CategoryRepository(Context, _mapper);

                return _categoryRepository;
            }
        }


        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                    _productRepository = new ProductRepository(Context, _configuration);

                return _productRepository;
            }
        }


        public INewsRepository NewsRepository
        {
            get
            {
                if (_newsRepository == null)
                    _newsRepository = new NewsRepository(Context, _mapper, _configuration, this);

                return _newsRepository;
            }
        }


        public ITagRepository TagRepository
        {
            get
            {
                if (_tagRepository == null)
                    _tagRepository = new TagRepository(Context);

                return _tagRepository;
            }
        }


        public ICommentRepository CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                    _commentRepository = new CommentRepository(Context);

                return _commentRepository;
            }
        }


        public INewsletterRepository NewsletterRepository
        {
            get
            {
                if (_newsletterRepository == null)
                    _newsletterRepository = new NewsletterRepository(Context);

                return _newsletterRepository;
            }
        }


        public ISliderRepository SliderRepository
        {
            get
            {
                if (_sliderRepository == null)
                    _sliderRepository = new SliderRepository(Context);

                return _sliderRepository;
            }
        }


        public async Task Commit()
        {
            await Context.SaveChangesAsync();
        }
    }
}
