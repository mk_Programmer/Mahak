﻿using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Mahak.Common.Extensions;
using Mahak.ViewModel.Category;

namespace Mahak.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly MahakDBContext _context;
        private readonly IMapper _mapper;
        public CategoryRepository(MahakDBContext context, IMapper mapper)
        {
            _context = context;
            _context.CheckArgumentIsNull(nameof(_context));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));
        }


        public async Task<List<CategoryViewModel>> GetPaginateCategoriesAsync(int offset, int limit, string orderBy, string searchText)
        {
            List<CategoryViewModel> categories = await _context.Categories
                                .GroupJoin(_context.Categories,
                                (cl => cl.ParentCategoryId),
                                (or => or.CategoryId),
                                ((cl, or) => new { CategoryInfo = cl, ParentInfo = or }))
                                .SelectMany(p => p.ParentInfo.DefaultIfEmpty(), (x, y) => new { x.CategoryInfo, ParentInfo = y })
                                .OrderBy(orderBy)
                                .Skip(offset).Take(limit)
                                .Select(c => new CategoryViewModel
                                {
                                    CategoryId = c.CategoryInfo.CategoryId,
                                    CategoryName = c.CategoryInfo.CategoryName,
                                    ImageName = c.CategoryInfo.ImageName,
                                    Url = c.CategoryInfo.Url,
                                    Title = c.CategoryInfo.Title,
                                    Description = c.CategoryInfo.Description,
                                    ParentCategoryId = c.ParentInfo.CategoryId,
                                    ParentCategoryName = c.ParentInfo.CategoryName
                                }).AsNoTracking().ToListAsync();

            foreach (var item in categories)
                item.Row = ++offset;

            return categories;
        }


        public async Task<List<CategoryViewModel>> GetParentInfoAsync(int offset, int limit, string orderBy)
        {
            List<CategoryViewModel> categories = await _context.Categories.GroupJoin(_context.Categories,
                                (cl => cl.ParentCategoryId),
                                (or => or.CategoryId),
                                ((cl, or) => new { CategoryInfo = cl, ParentInfo = or }))
                                .SelectMany(p => p.ParentInfo.DefaultIfEmpty(), (x, y) => new { x.CategoryInfo, ParentInfo = y })
                                .OrderBy(orderBy)
                                .Skip(offset).Take(limit)
                                .Select(c => new CategoryViewModel
                                {
                                    ImageName = c.CategoryInfo.ImageName,
                                    Url = c.CategoryInfo.Url,
                                    Description = c.CategoryInfo.Description,
                                    ParentCategoryId = c.ParentInfo.CategoryId,
                                    ParentCategoryName = c.ParentInfo.CategoryName
                                }).AsNoTracking().ToListAsync();

            foreach (var item in categories)
                item.Row = ++offset;

            return categories;
        }


        public async Task<CategoryViewModel> GetCategoryTitleByIdAsync(string categoryId)
        {
            CategoryViewModel category = null;

            var categoryInfo = await (from n in _context.Categories.Where(n => n.CategoryId == categoryId)
                                      select (new CategoryViewModel
                                      {
                                          CategoryId = n.CategoryId,
                                          Title = n.Title,
                                      })).AsNoTracking().ToListAsync();

            if (categoryInfo.Count() != 0)
            {
                var categoryGroup = categoryInfo.GroupBy(g => g.CategoryId).Select(g => new { CategoryId = g.Key, CategoryGroup = g });

                category = new CategoryViewModel()
                {
                    CategoryId = categoryGroup.First().CategoryGroup.First().CategoryId,
                    Title = categoryGroup.First().CategoryGroup.First().Title,
                };
            }

            return category;
        }


        public async Task<List<TreeViewCategory>> GetAllCategoriesAsync()
        {
            var Categories = await (from c in _context.Categories
                                    where (c.ParentCategoryId == null)
                                    select new TreeViewCategory { id = c.CategoryId, title = c.CategoryName, url = c.Url }).ToListAsync();

            foreach (var item in Categories)
                BindSubCategories(item);

            return Categories;
        }


        public void BindSubCategories(TreeViewCategory category)
        {
            var SubCategories = (from c in _context.Categories
                                 where (c.ParentCategoryId == category.id)
                                 select new TreeViewCategory { id = c.CategoryId, title = c.CategoryName, url = c.Url }).ToList();

            foreach (var item in SubCategories)
            {
                BindSubCategories(item);
                category.subs.Add(item);
            }
        }


        public Category FindByCategoryName(string categoryName)
        {
            return _context.Categories.Where(c => c.CategoryName == categoryName.TrimStart().TrimEnd()).FirstOrDefault();
        }


        public bool IsExistCategory(string categoryName, string recentCategoryId = null)
        {
            if (!recentCategoryId.HasValue())
                return _context.Categories.Any(c => c.CategoryName.Trim().Replace(" ", "") == categoryName.Trim().Replace(" ", ""));
            else
            {
                var category = _context.Categories.Where(c => c.CategoryName.Trim().Replace(" ", "") == categoryName.Trim().Replace(" ", "")).FirstOrDefault();

                if (category == null)
                    return false;
                else
                {
                    if (category.CategoryId != recentCategoryId)
                        return true;
                    else
                        return false;
                }
            }
        }


        public async Task<CategoryViewModel> GetCategoryByIdAsync(string categorytId)
        {
            CategoryViewModel category = null;

            var categoryInfo = await (from n in _context.Categories.Where(n => n.CategoryId == categorytId)
                                      select (new CategoryViewModel
                                      {
                                          CategoryId = n.CategoryId,
                                          CategoryName = n.CategoryName,
                                          Url = n.Url,
                                          ImageName = n.ImageName,
                                          Description = n.Description,
                                          ParentCategoryId = n.ParentCategoryId,
                                          ParentCategoryName = n.Parent.CategoryName,
                                      })).AsNoTracking().ToListAsync();

            if (categoryInfo.Count() != 0)
            {
                var categoryGroup = categoryInfo.GroupBy(g => g.CategoryId).Select(g => new { CategoryId = g.Key, CategoryGroup = g });

                category = new CategoryViewModel()
                {
                    CategoryId = categoryGroup.First().CategoryGroup.First().CategoryId,
                    CategoryName = categoryGroup.First().CategoryGroup.First().CategoryName,
                    Url = categoryGroup.First().CategoryGroup.First().Url,
                    ImageName = categoryGroup.First().CategoryGroup.First().ImageName,
                    ParentCategoryId = categoryGroup.First().CategoryGroup.First().ParentCategoryId,
                };
            }

            return category;
        }


        public int CountCategories() => _context.Categories.Count();
    }
}
