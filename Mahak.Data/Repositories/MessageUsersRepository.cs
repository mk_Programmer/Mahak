﻿using Mahak.Common.Extensions;
using Mahak.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Mahak.Entities;
using Mahak.Data.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Mahak.Data.Repositories
{
    public class MessageUsersRepository : IMessageUsersRepository
    {
        private readonly MahakDBContext _context;
        public MessageUsersRepository(MahakDBContext context)
        {
            _context = context;
        }


        public int CountUnAnsweredComments() => _context.MessageUsers.Where(c => c.IsRegisterCode == true).Count();


        public async Task<List<MessageUsersViewModel>> GetPaginateMessagesAsync(int offset, int limit, string orderBy, string searchText, int userId, bool? isRegisterCode)
        {
            //bool? convertConfirm = Convert.ToBoolean(isRegisterCode);
            var getDateTimesForSearch = searchText.GetDateTimeForSearch();

            List<MessageUsersViewModel> accounts = await _context.MessageUsers
                                   //.Where(n => (isRegisterCode == false) && n.UserId.ToString().Contains(userId.ToString()) && ((n.RegisterDateTime >= getDateTimesForSearch.First())))
                                   .OrderBy(orderBy)
                                   .Skip(offset).Take(limit)
                                   .Select(l => new MessageUsersViewModel
                                   {
                                       //Amount=0, FirstName="", LastName="", FullName="", SelectedProvider="", CommentCode="", PhoneNumber="", MessageTypes=0,
                                       MessageId = l.MessageId,
                                       Email = l.Email,
                                       Description = l.Description,
                                       IsRegisterCode = l.IsRegisterCode,
                                       PersianRegisterDateTime = l.RegisterDateTime.ConvertMiladiToShamsi("yyyy/MM/dd ساعت HH:mm:ss"),
                                       MessageType = l.MessageTypes == MessageType.Offers ? "پیشنهادات" : l.MessageTypes == MessageType.Feedback ? "انتقادات" :
                                                       l.MessageTypes == MessageType.Questions ? "سوالات" : l.MessageTypes == MessageType.Other ? "سایر پیام‌ها" : "-",
                                   })
                                   .AsNoTracking()
                                   .ToListAsync();

            foreach (var item in accounts)
                item.Row = ++offset;

            return accounts;
        }
    }
}
