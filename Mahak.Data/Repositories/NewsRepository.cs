﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Mahak.Data.Contracts;
using Mahak.Entities;
using Mahak.ViewModel.Home;
using Mahak.ViewModel.News;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Mahak.Common.Extensions;

namespace Mahak.Data.Repositories
{
    public class NewsRepository : INewsRepository
    {
        private readonly MahakDBContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _uw;
        public NewsRepository(MahakDBContext context, IMapper mapper, IConfiguration configuration, IUnitOfWork uw)
        {
            _context = context;
            _context.CheckArgumentIsNull(nameof(_context));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));

            _configuration = configuration;
            _configuration.CheckArgumentIsNull(nameof(_configuration));

            _uw = uw;
            _uw.CheckArgumentIsNull(nameof(_uw));
        }


        public async Task<List<NewsViewModel>> GetPaginateNewsAsync(int offset, int limit, string orderBy, string searchText, bool? isPublish, bool? isInternal)
        {
            string NameOfTags = "";
            List<NewsViewModel> newsViewModel = new List<NewsViewModel>();
            var getDateTimesForSearch = searchText.GetDateTimeForSearch();

            var convertPublish = Convert.ToBoolean(isPublish);
            var convertInternal = Convert.ToBoolean(isInternal);

            var allNews = await (from n in ((from n in _context.News.Include(v => v.Visits).Include(u => u.User).Include(c => c.Comments)
                                             where ((n.Title.Contains(searchText) || (n.PublishDateTime >= getDateTimesForSearch.First() && n.PublishDateTime <= getDateTimesForSearch.Last())) && (isInternal == null) && (isPublish == null || (convertPublish ? n.IsPublish && n.PublishDateTime <= DateTime.Now : !n.IsPublish)))
                                             select (new
                                             {
                                                 n.NewsId,
                                                 n.Title,
                                                 n.Abstract,
                                                 ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                                 n.Url,
                                                 n.ImageName,
                                                 n.Description,
                                                 NumberOfVisit = n.Visits.Select(v => v.NumberOfVisit).Sum(),
                                                 NumberOfComments = n.Comments.Count(),
                                                 AuthorName = n.User.FirstName + " " + n.User.LastName,
                                                 n.IsPublish,
                                                 n.PublishDateTime,
                                                 n.TextLocations,
                                             })).OrderBy(orderBy).Skip(offset).Take(limit))
                                 join a in _context.NewsTags on n.NewsId equals a.NewsId into ac
                                 from act in ac.DefaultIfEmpty()
                                 join t in _context.Tags on act.TagId equals t.TagId into tg
                                 from tog in tg.DefaultIfEmpty()
                                 select (new NewsViewModel
                                 {
                                     NewsId = n.NewsId,
                                     Title = n.Title,
                                     Abstract = n.Abstract,
                                     ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                     Url = n.Url,
                                     ImageName = n.ImageName,
                                     Description = n.Description,
                                     NumberOfVisit = n.NumberOfVisit,
                                     NumberOfComments = n.NumberOfComments,
                                     AuthorName = n.AuthorName,
                                     IsPublish = n.IsPublish,
                                     PublishDateTime = n.PublishDateTime,
                                     NameOfTags = tog != null ? tog.TagName : "",
                                     Locations = n.TextLocations == TextLocation.News ? "خبر" : n.TextLocations == TextLocation.Introduction ? "معرفی محک" : n.TextLocations == TextLocation.History ? "تاریخچه" : "-",
                                 })).AsNoTracking().ToListAsync();


            var newsGroup = allNews.GroupBy(g => g.NewsId).Select(g => new { NewsId = g.Key, NewsGroup = g });

            foreach (var item in newsGroup)
            {
                NameOfTags = "";

                foreach (var a in item.NewsGroup.Select(a => a.NameOfTags).Distinct())
                {
                    if (NameOfTags == "")
                        NameOfTags = a;
                    else
                        NameOfTags = NameOfTags + " - " + a;
                }

                NewsViewModel news = new NewsViewModel()
                {
                    NewsId = item.NewsId,
                    Title = item.NewsGroup.First().Title,
                    ShortTitle = item.NewsGroup.First().ShortTitle,
                    Abstract = item.NewsGroup.First().Abstract,
                    Url = item.NewsGroup.First().Url,
                    Description = item.NewsGroup.First().Description,
                    NumberOfVisit = item.NewsGroup.First().NumberOfVisit,
                    Status = item.NewsGroup.First().IsPublish == false ? "پیش نویس" : (item.NewsGroup.First().PublishDateTime > DateTime.Now ? "انتشار در آینده" : "منتشر شده"),
                    NameOfTags = NameOfTags,
                    ImageName = item.NewsGroup.First().ImageName,
                    NumberOfComments = item.NewsGroup.First().NumberOfComments,
                    PublishDateTime = item.NewsGroup.First().PublishDateTime,
                    PersianPublishDate = item.NewsGroup.First().PublishDateTime == null ? "-" : DateTimeExtensions.ConvertMiladiToShamsi(item.NewsGroup.First().PublishDateTime, "yyyy/MM/dd ساعت HH:mm"),
                    Locations = item.NewsGroup.First().Locations,
                };
                newsViewModel.Add(news);
            }

            foreach (var item in newsViewModel)
                item.Row = ++offset;

            return newsViewModel;
        }


        public async Task<List<NewsViewModel>> MostViewedNewsAsync(int offset, int limit, string duration)
        {
            List<NewsViewModel> newsViewModel = new List<NewsViewModel>();
            DateTime StartMiladiDate;
            DateTime EndMiladiDate = DateTime.Now;

            if (duration == "week")
            {
                int NumOfWeek = DateTimeExtensions.ConvertMiladiToShamsi(DateTime.Now, "dddd").GetNumOfWeek();
                StartMiladiDate = DateTime.Now.AddDays((-1) * NumOfWeek).Date + new TimeSpan(0, 0, 0);
            }
            else if (duration == "day")
                StartMiladiDate = DateTime.Now.Date + new TimeSpan(0, 0, 0);
            else
            {
                string DayOfMonth = DateTimeExtensions.ConvertMiladiToShamsi(DateTime.Now, "dd").Fa2En();
                StartMiladiDate = DateTime.Now.AddDays((-1) * (int.Parse(DayOfMonth) - 1)).Date + new TimeSpan(0, 0, 0);
            }

            var allNews = await (from n in ((from n in _context.News.Include(v => v.Visits).Include(c => c.Comments)
                                             where (n.PublishDateTime <= EndMiladiDate && StartMiladiDate <= n.PublishDateTime)
                                             select (new
                                             {
                                                 n.NewsId,
                                                 ShortTitle = n.Title.Length > 60 ? n.Title.Substring(0, 60) + "..." : n.Title,
                                                 n.Url,
                                                 NumberOfVisit = n.Visits.Select(v => v.NumberOfVisit).Sum(),
                                                 NumberOfComments = n.Comments.Count(),
                                                 n.ImageName,
                                                 PublishDateTime = n.PublishDateTime == null ? new DateTime(01, 01, 01) : n.PublishDateTime,
                                                 n.TextLocations,
                                             })).OrderBy("NumberOfVisit desc").Skip(offset).Take(limit))
                                 select (new
                                 {
                                     n.NewsId,
                                     n.ShortTitle,
                                     n.Url,
                                     n.NumberOfVisit,
                                     n.NumberOfComments,
                                     n.ImageName,
                                     n.PublishDateTime,
                                     Locations = n.TextLocations == TextLocation.News ? "خبر" : n.TextLocations == TextLocation.Introduction ? "معرفی محک" : n.TextLocations == TextLocation.History ? "تاریخچه" : "-",
                                 })).AsNoTracking().ToListAsync();

            var newsGroup = allNews.GroupBy(g => g.NewsId).Select(g => new { NewsId = g.Key, NewsGroup = g });
            foreach (var item in newsGroup)
            {
                NewsViewModel news = new NewsViewModel()
                {
                    NewsId = item.NewsId,
                    ShortTitle = item.NewsGroup.First().ShortTitle,
                    Url = item.NewsGroup.First().Url,
                    NumberOfVisit = item.NewsGroup.First().NumberOfVisit,
                    PublishDateTime = item.NewsGroup.First().PublishDateTime,
                    ImageName = item.NewsGroup.First().ImageName,
                    Locations = item.NewsGroup.First().Locations,
                };
                newsViewModel.Add(news);
            }

            return newsViewModel;
        }


        public string CheckNewsFileName(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName);
            int fileNameCount = _context.News.Where(f => f.ImageName == fileName).Count();
            int j = 1;

            while (fileNameCount != 0)
            {
                fileName = fileName.Replace(fileExtension, "") + j + fileExtension;
                j++;
            }

            return fileName;
        }


        public async Task<NewsViewModel> GetNewsByIdAsync(string newsId, int userId)
        {
            NewsViewModel news = null;

            var newsInfo = await (from n in _context.News.Where(n => n.NewsId == newsId).Include(v => v.Visits).Include(u => u.User).Include(c => c.Comments)
                                  join a in _context.NewsTags on n.NewsId equals a.NewsId into ac
                                  from act in ac.DefaultIfEmpty()
                                  join t in _context.Tags on act.TagId equals t.TagId into tg
                                  from tog in tg.DefaultIfEmpty()
                                  select (new NewsViewModel
                                  {
                                      NewsId = n.NewsId,
                                      Title = n.Title,
                                      Abstract = n.Abstract,
                                      ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                      Url = n.Url,
                                      ImageName = n.ImageName,
                                      Description = n.Description,
                                      NumberOfVisit = n.Visits.Select(v => v.NumberOfVisit).Sum(),
                                      NumberOfComments = n.Comments.Where(c => c.IsConfirm == true).Count(),
                                      NameOfTags = tog != null ? tog.TagName : "",
                                      IdOfTags = tog != null ? tog.TagId : "",
                                      AuthorInfo = n.User,
                                      IsPublish = n.IsPublish,
                                      PublishDateTime = n.PublishDateTime == null ? new DateTime(01, 01, 01) : n.PublishDateTime,
                                      PersianPublishDate = n.PublishDateTime == null ? "-" : n.PublishDateTime.ConvertMiladiToShamsi("yyyy/MM/dd ساعت HH:mm:ss"),
                                      Locations = n.TextLocations == TextLocation.News ? "خبر" : n.TextLocations == TextLocation.Introduction ? "معرفی محک" : n.TextLocations == TextLocation.History ? "تاریخچه" : "-",
                                  })).AsNoTracking().ToListAsync();

            if (newsInfo.Count() != 0)
            {
                var newsGroup = newsInfo.GroupBy(g => g.NewsId).Select(g => new { NewsId = g.Key, NewsGroup = g });

                news = new NewsViewModel()
                {
                    NewsId = newsGroup.First().NewsGroup.First().NewsId,
                    Title = newsGroup.First().NewsGroup.First().Title,
                    ShortTitle = newsGroup.First().NewsGroup.First().ShortTitle,
                    Abstract = newsGroup.First().NewsGroup.First().Abstract,
                    Url = newsGroup.First().NewsGroup.First().Url,
                    Description = newsGroup.First().NewsGroup.First().Description,
                    NumberOfVisit = newsGroup.First().NewsGroup.First().NumberOfVisit,
                    PersianPublishDate = newsGroup.First().NewsGroup.First().PersianPublishDate,
                    Status = newsGroup.First().NewsGroup.First().IsPublish == false ? "پیش نویس" : (newsGroup.First().NewsGroup.First().PublishDateTime > DateTime.Now ? "انتشار در آینده" : "منتشر شده"),
                    TagNamesList = newsGroup.First().NewsGroup.Select(a => a.NameOfTags).Distinct().ToList(),
                    TagIdsList = newsGroup.First().NewsGroup.Select(a => a.IdOfTags).Distinct().ToList(),
                    ImageName = newsGroup.First().NewsGroup.First().ImageName,
                    AuthorInfo = newsGroup.First().NewsGroup.First().AuthorInfo,
                    NumberOfComments = newsGroup.First().NewsGroup.First().NumberOfComments,
                    PublishDateTime = newsGroup.First().NewsGroup.First().PublishDateTime,
                    Locations = newsGroup.First().NewsGroup.First().Locations,
                };
            }

            return news;
        }


        public async Task<List<Comment>> GetNewsCommentsAsync(string newsId)
        {
            var comments = await (from c in _context.Comments
                                  where (c.ParentCommentId == null && c.NewsId == newsId && c.IsConfirm == true)
                                  select new Comment { CommentId = c.CommentId, Description = c.Description, Email = c.Email, PostageDateTime = c.PostageDateTime, Name = c.Name, NewsId = c.NewsId }).ToListAsync();

            foreach (var item in comments)
                await BindSubComments(item);

            return comments;
        }


        public async Task BindSubComments(Comment comment)
        {
            var subComments = await (from c in _context.Comments
                                     where (c.ParentCommentId == comment.CommentId && c.IsConfirm == true)
                                     select new Comment { CommentId = c.CommentId, Description = c.Description, Email = c.Email, PostageDateTime = c.PostageDateTime, Name = c.Name, NewsId = c.NewsId }).ToListAsync();

            foreach (var item in subComments)
            {
                await BindSubComments(item);
                comment.comments.Add(item);
            }
        }


        public async Task<List<NewsInCategoriesAndTagsViewModel>> GetNewsInTagAsync(string TagId, int pageIndex, int pageSize)
        {
            List<NewsInCategoriesAndTagsViewModel> newsViewModel = new List<NewsInCategoriesAndTagsViewModel>();

            var allNews = await (from n in ((from n in _context.News.Include(v => v.Visits).Include(u => u.User).Include(c => c.Comments).Include(c => c.NewsTags)
                                             where (n.IsPublish == true && n.PublishDateTime <= DateTime.Now && n.NewsTags.Select(c => c.TagId).Contains(TagId))
                                             select (new
                                             {
                                                 n.NewsId,
                                                 n.Title,
                                                 n.Abstract,
                                                 ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                                 n.Url,
                                                 n.ImageName,
                                                 NumberOfVisit = n.Visits.Select(v => v.NumberOfVisit).Sum(),
                                                 NumberOfComments = n.Comments.Where(c => c.IsConfirm == true).Count(),
                                                 AuthorName = n.User.FirstName + " " + n.User.LastName,
                                                 n.PublishDateTime,
                                             })).Skip(pageIndex * pageSize).Take(pageSize))
                                 select (new NewsInCategoriesAndTagsViewModel
                                 {
                                     NewsId = n.NewsId,
                                     Title = n.Title,
                                     Abstract = n.Abstract,
                                     ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                     Url = n.Url,
                                     ImageName = n.ImageName,
                                     NumberOfVisit = n.NumberOfVisit,
                                     NumberOfComments = n.NumberOfComments,
                                     PublishDateTime = n.PublishDateTime,
                                 })).AsNoTracking().ToListAsync();

            var newsGroup = allNews.GroupBy(g => g.NewsId).Select(g => new { NewsId = g.Key, NewsGroup = g });

            foreach (var item in newsGroup)
            {
                NewsInCategoriesAndTagsViewModel news = new NewsInCategoriesAndTagsViewModel()
                {
                    NewsId = item.NewsId,
                    Title = item.NewsGroup.First().Title,
                    ShortTitle = item.NewsGroup.First().ShortTitle,
                    Abstract = item.NewsGroup.First().Abstract,
                    Url = item.NewsGroup.First().Url,
                    NumberOfVisit = item.NewsGroup.First().NumberOfVisit,
                    ImageName = item.NewsGroup.First().ImageName,
                    NumberOfComments = item.NewsGroup.First().NumberOfComments,
                    PersianPublishDate = item.NewsGroup.First().PublishDateTime.ConvertMiladiToShamsi("yyyy/MM/dd"),
                    PersianPublishTime = item.NewsGroup.First().PublishDateTime.ConvertMiladiToShamsi("HH:mm:ss"),
                };
                newsViewModel.Add(news);
            }
            return newsViewModel;
        }


        public async Task<string> GetWeeklyNewsAsync()
        {
            string content = "";
            int NumOfWeek = DateTimeExtensions.ConvertMiladiToShamsi(DateTime.Now, "dddd").GetNumOfWeek();
            DateTime StartMiladiDate = DateTime.Now.AddDays((-1) * NumOfWeek).Date + new TimeSpan(0, 0, 0);
            DateTime EndMiladiDate = DateTime.Now;

            var news = await (from n in _context.News.Include(v => v.Visits).Include(c => c.Comments)
                              where (n.PublishDateTime <= EndMiladiDate && StartMiladiDate <= n.PublishDateTime)
                              select (new NewsViewModel
                              {
                                  NewsId = n.NewsId,
                                  Title = n.Title,
                                  ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                  Url = n.Url,
                                  ImageName = n.ImageName,
                              })).OrderByDescending(o => o.PublishDateTime).AsNoTracking().ToListAsync();

            string url = _configuration.GetValue<string>("SiteSettings:SiteInfo:Url");

            foreach (var item in news)
                content = content + $"<div style='direction:rtl;font-family:tahoma;text-align:center'> <div class='row align-items-center'> <div class='col-12 col-lg-6'><div class='post-thumbnail'> <img src='{url + "/newsImage/" + item.ImageName}' alt='{item.ImageName}'> </div> </div> <div class='col-12 col-lg-6'> <div class='post-content mt-0'> <h4 style='color:#878484;'>{item.Title}</h4> <p> {item.ShortTitle} <a href='{url}/News/{item.NewsId}/{item.Url}'>[ادامه مطلب]</a> </p> </div> </div> </div> </div><hr/>";

            return content;
        }


        public async Task<List<NewsViewModel>> SearchInNews(string textSearch)
        {
            List<NewsViewModel> newsViewModel = new List<NewsViewModel>();

            var allNews = await (from n in _context.News.Where(n => (n.Title.Contains(textSearch) || n.Description.Contains(textSearch)) && n.IsPublish == true && n.PublishDateTime <= DateTime.Now).Include(v => v.Visits).Include(u => u.User).Include(c => c.Comments)
                                 select (new NewsViewModel
                                 {
                                     NewsId = n.NewsId,
                                     Title = n.Title,
                                     Abstract = n.Abstract,
                                     ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                     Url = n.Url,
                                     ImageName = n.ImageName,
                                     Description = n.Description,
                                     NumberOfVisit = n.Visits.Select(v => v.NumberOfVisit).Sum(),
                                     NumberOfComments = n.Comments.Count(),
                                     IsPublish = n.IsPublish,
                                     PublishDateTime = n.PublishDateTime,
                                 })).AsNoTracking().ToListAsync();

            var newsGroup = allNews.GroupBy(g => g.NewsId).Select(g => new { NewsId = g.Key, NewsGroup = g });

            foreach (var item in newsGroup)
            {
                NewsViewModel news = new NewsViewModel()
                {
                    NewsId = item.NewsId,
                    Title = item.NewsGroup.First().Title,
                    ShortTitle = item.NewsGroup.First().ShortTitle,
                    Abstract = item.NewsGroup.First().Abstract,
                    Url = item.NewsGroup.First().Url,
                    NumberOfVisit = item.NewsGroup.First().NumberOfVisit,
                    ImageName = item.NewsGroup.First().ImageName,
                    NumberOfComments = item.NewsGroup.First().NumberOfComments,
                    PersianPublishDate = item.NewsGroup.First().PersianPublishDate,
                    PersianPublishTime = item.NewsGroup.First().PersianPublishTime,
                };
                newsViewModel.Add(news);
            }
            return newsViewModel;
        }


        public async Task InsertVisitOfUserAsync(string newsId, string ipAddress)
        {
            Visit visit = _uw.BaseRepository<Visit>().FindByConditionAsync(n => n.NewsId == newsId && n.IpAddress == ipAddress).Result.FirstOrDefault();

            if (visit != null && visit.LastVisitDateTime.Date != DateTime.Now.Date)
            {
                visit.NumberOfVisit = visit.NumberOfVisit + 1;
                visit.LastVisitDateTime = DateTime.Now;
                await _uw.Commit();
            }
            else if (visit == null)
            {
                visit = new Visit { IpAddress = ipAddress, LastVisitDateTime = DateTime.Now, NewsId = newsId, NumberOfVisit = 1 };
                await _uw.BaseRepository<Visit>().CreateAsync(visit);
                await _uw.Commit();
            }
        }


        public int CountNews() => _context.News.Count();
        public int CountFuturePublishedNews() => _context.News.Where(n => n.PublishDateTime > DateTime.Now).Count();
        public int CountNewsPublishedOrDraft(bool isPublish) => _context.News.Where(n => isPublish ? n.IsPublish && n.PublishDateTime <= DateTime.Now : !n.IsPublish).Count();
        public int CountNewsPublished() => _context.News.Where(n => n.IsPublish == true && n.PublishDateTime <= DateTime.Now).Count();






        public async Task<NewsViewModel> GetNewsByLocationAsync(TextLocation location)
        {
            NewsViewModel news = null;

            var newsInfo = await (from n in _context.News.Where(n => n.TextLocations == location).Include(v => v.Visits).Include(u => u.User).Include(c => c.Comments)
                                  join a in _context.NewsTags on n.NewsId equals a.NewsId into ac
                                  from act in ac.DefaultIfEmpty()
                                  join t in _context.Tags on act.TagId equals t.TagId into tg
                                  from tog in tg.DefaultIfEmpty()
                                  select (new NewsViewModel
                                  {
                                      NewsId = n.NewsId,
                                      Title = n.Title,
                                      Abstract = n.Abstract,
                                      ShortTitle = n.Title.Length > 50 ? n.Title.Substring(0, 50) + "..." : n.Title,
                                      Url = n.Url,
                                      ImageName = n.ImageName,
                                      Description = n.Description,
                                      NumberOfVisit = n.Visits.Select(v => v.NumberOfVisit).Sum(),
                                      NumberOfComments = n.Comments.Where(c => c.IsConfirm == true).Count(),
                                      NameOfTags = tog != null ? tog.TagName : "",
                                      IdOfTags = tog != null ? tog.TagId : "",
                                      AuthorInfo = n.User,
                                      IsPublish = n.IsPublish,
                                      PublishDateTime = n.PublishDateTime == null ? new DateTime(01, 01, 01) : n.PublishDateTime,
                                      PersianPublishDate = n.PublishDateTime == null ? "-" : n.PublishDateTime.ConvertMiladiToShamsi("yyyy/MM/dd ساعت HH:mm:ss"),
                                      Locations = n.TextLocations == TextLocation.News ? "خبر" : n.TextLocations == TextLocation.Introduction ? "معرفی محک" : n.TextLocations == TextLocation.History ? "تاریخچه" : "-",
                                  })).AsNoTracking().ToListAsync();

            if (newsInfo.Count() != 0)
            {
                var newsGroup = newsInfo.GroupBy(g => g.NewsId).Select(g => new { NewsId = g.Key, NewsGroup = g });

                news = new NewsViewModel()
                {
                    NewsId = newsGroup.First().NewsGroup.First().NewsId,
                    Title = newsGroup.First().NewsGroup.First().Title,
                    ShortTitle = newsGroup.First().NewsGroup.First().ShortTitle,
                    Abstract = newsGroup.First().NewsGroup.First().Abstract,
                    Url = newsGroup.First().NewsGroup.First().Url,
                    Description = newsGroup.First().NewsGroup.First().Description,
                    NumberOfVisit = newsGroup.First().NewsGroup.First().NumberOfVisit,
                    PersianPublishDate = newsGroup.First().NewsGroup.First().PersianPublishDate,
                    Status = newsGroup.First().NewsGroup.First().IsPublish == false ? "پیش نویس" : (newsGroup.First().NewsGroup.First().PublishDateTime > DateTime.Now ? "انتشار در آینده" : "منتشر شده"),
                    TagNamesList = newsGroup.First().NewsGroup.Select(a => a.NameOfTags).Distinct().ToList(),
                    TagIdsList = newsGroup.First().NewsGroup.Select(a => a.IdOfTags).Distinct().ToList(),
                    ImageName = newsGroup.First().NewsGroup.First().ImageName,
                    AuthorInfo = newsGroup.First().NewsGroup.First().AuthorInfo,
                    NumberOfComments = newsGroup.First().NewsGroup.First().NumberOfComments,
                    PublishDateTime = newsGroup.First().NewsGroup.First().PublishDateTime,
                    Locations = newsGroup.First().NewsGroup.First().Locations,
                };
            }

            return news;
        }
    }
}
