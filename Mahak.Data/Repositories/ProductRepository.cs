﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Mahak.Data.Contracts;
using Mahak.ViewModel.Home;
using Mahak.Common.Extensions;
using Mahak.ViewModel.Product;
using Mahak.Entities;
using Mahak.ViewModel.Customer;
using Mahak.ViewModel.Order;

namespace Mahak.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly MahakDBContext _context;
        private readonly IConfiguration _configuration;
        public ProductRepository(MahakDBContext context, IConfiguration configuration)
        {
            _context = context;
            _context.CheckArgumentIsNull(nameof(_context));

            _configuration = configuration;
            _configuration.CheckArgumentIsNull(nameof(_configuration));
        }


        public async Task<List<ProductViewModel>> GetPaginateProductAsync(int offset, int limit, string orderBy, string searchText)
        {
            int searchValue = 0;
            string NameOfCategories = "";
            List<ProductViewModel> ViewModels = new List<ProductViewModel>();
            ProductViewModel ViewModel = new ProductViewModel();
            var getDateTimesForSearch = searchText.GetDateTimeForSearch();
            var getValue = searchValue.GetValueForSearch();

            var allProduct = await (from n in ((from n in _context.Products.Include(c => c.Comments)
                                                where ((n.ProductName.Contains(searchText) || (n.CreateDate >= getDateTimesForSearch.First() && n.CreateDate <= getDateTimesForSearch.Last())))
                                                select (new
                                                {
                                                    n.ProductId,
                                                    n.ProductName,
                                                    n.ProductCode,
                                                    n.Price,
                                                    n.Stock,
                                                    n.ImageName,
                                                    n.Description,
                                                    n.Url,
                                                    n.CreateDate,
                                                    NumberOfComments = n.Comments.Count(),
                                                })).OrderBy(orderBy).Skip(offset).Take(limit))
                                    join a in _context.ProductCategories on n.ProductId equals a.ProductId into pc
                                    from pca in pc.DefaultIfEmpty()
                                    join c in _context.Categories on pca.CategoryId equals c.CategoryId into co
                                    from cog in co.DefaultIfEmpty()
                                    select (new ProductViewModel
                                    {
                                        ProductId = n.ProductId,
                                        ProductName = n.ProductName,
                                        ProductCode = n.ProductCode,
                                        Price = n.Price,
                                        Stock = n.Stock,
                                        ImageName = n.ImageName,
                                        Description = n.Description,
                                        Url = n.Url,
                                        CreateDate = n.CreateDate,
                                        PersianCreateDate = n.CreateDate.ConvertMiladiToShamsi("yyyy/MM/dd"),
                                        NameOfCategories = cog != null ? cog.CategoryName : "",
                                        NumberOfComments = n.NumberOfComments,
                                    })).AsNoTracking().ToListAsync();

            var productGroup = allProduct.GroupBy(g => g.ProductId).Select(g => new { ProductId = g.Key, ProductGroup = g });

            foreach (var item in productGroup)
            {
                NameOfCategories = "";

                foreach (var a in item.ProductGroup.Select(a => a.NameOfCategories).Distinct())
                {
                    if (NameOfCategories == "")
                        NameOfCategories = a;
                    else
                        NameOfCategories = NameOfCategories + " - " + a;
                }

                ProductViewModel product = new ProductViewModel()
                {
                    ProductId = item.ProductId,
                    ProductName = item.ProductGroup.First().ProductName,
                    ProductCode = item.ProductGroup.First().ProductCode,
                    Price = item.ProductGroup.First().Price,
                    Stock = item.ProductGroup.First().Stock,
                    ImageName = item.ProductGroup.First().ImageName,
                    Description = item.ProductGroup.First().Description,
                    Url = item.ProductGroup.First().Url,
                    NameOfCategories = NameOfCategories,
                    NumberOfComments = item.ProductGroup.First().NumberOfComments,
                    CreateDate = item.ProductGroup.First().CreateDate,
                    PersianCreateDate = item.ProductGroup.First().CreateDate == null ? "-" : item.ProductGroup.First().CreateDate > DateTime.Now ? DateTimeExtensions.ConvertMiladiToShamsi(item.ProductGroup.First().CreateDate, "yyyy/MM/dd") + " " + "(در دست تولید)" : DateTimeExtensions.ConvertMiladiToShamsi(item.ProductGroup.First().CreateDate, "yyyy/MM/dd"),
                };
                ViewModels.Add(product);
            }

            foreach (var item in ViewModels)
                item.Row = ++offset;

            return ViewModels;
        }


        public string CheckProductFileName(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName);
            int fileNameCount = _context.News.Where(f => f.ImageName == fileName).Count();
            int j = 1;

            while (fileNameCount != 0)
            {
                fileName = fileName.Replace(fileExtension, "") + j + fileExtension;
                fileNameCount = _context.Products.Where(f => f.ImageName == fileName).Count();
                j++;
            }

            return fileName;
        }


        public async Task<ProductViewModel> GetProductByIdAsync(string productId, int userId)
        {
            string NameOfCategories = "";
            ProductViewModel product = null;

            var productInfo = await (from n in _context.Products.Where(n => n.ProductId == productId)
                                     join a in _context.ProductCategories on n.ProductId equals a.ProductId into pc
                                     from pca in pc.DefaultIfEmpty()
                                     join c in _context.Categories on pca.CategoryId equals c.CategoryId into co
                                     from cog in co.DefaultIfEmpty()
                                     select (new ProductViewModel
                                     {
                                         ProductId = n.ProductId,
                                         ProductName = n.ProductName,
                                         ProductCode = n.ProductCode,
                                         Stock = n.Stock,
                                         Price = n.Price,
                                         Url = n.Url,
                                         ImageName = n.ImageName,
                                         Description = n.Description,
                                         CreateDate = n.CreateDate,
                                         NameOfCategories = cog != null ? cog.CategoryName : "",
                                     })).AsNoTracking().ToListAsync();

            if (productInfo.Count() != 0)
            {
                var productGroup = productInfo.GroupBy(g => g.ProductId).Select(g => new { ProductId = g.Key, ProductGroup = g });

                foreach (var a in productGroup.First().ProductGroup.Select(a => a.NameOfCategories).Distinct())
                {
                    if (NameOfCategories == "")
                        NameOfCategories = a;
                    else
                        NameOfCategories = NameOfCategories + " - " + a;
                }

                product = new ProductViewModel()
                {
                    ProductId = productGroup.First().ProductGroup.First().ProductId,
                    ProductName = productGroup.First().ProductGroup.First().ProductName,
                    ProductCode = productGroup.First().ProductGroup.First().ProductCode,
                    Stock = productGroup.First().ProductGroup.First().Stock,
                    Price = productGroup.First().ProductGroup.First().Price,
                    Url = productGroup.First().ProductGroup.First().Url,
                    ImageName = productGroup.First().ProductGroup.First().ImageName,
                    Description = productGroup.First().ProductGroup.First().Description,
                    PersianCreateDate = productGroup.First().ProductGroup.First().PersianCreateDate,
                    CreateDate = productGroup.First().ProductGroup.First().CreateDate,
                    NameOfCategories = NameOfCategories,
                };
            }

            return product;
        }


        public async Task<List<Comment>> GetProductCommentsAsync(string productId)
        {
            var comments = await (from c in _context.Comments
                                  where (c.ParentCommentId == null && c.ProductId == productId && c.IsConfirm == true)
                                  select new Comment { CommentId = c.CommentId, Description = c.Description, Email = c.Email, PostageDateTime = c.PostageDateTime, Name = c.Name, ProductId = c.ProductId }).ToListAsync();

            foreach (var item in comments)
                await BindSubComments(item);

            return comments;
        }


        public async Task BindSubComments(Comment comment)
        {
            var subComments = await (from c in _context.Comments
                                     where (c.ParentCommentId == comment.CommentId && c.IsConfirm == true)
                                     select new Comment { CommentId = c.CommentId, Description = c.Description, Email = c.Email, PostageDateTime = c.PostageDateTime, Name = c.Name, ProductId = c.ProductId }).ToListAsync();

            foreach (var item in subComments)
            {
                await BindSubComments(item);
                comment.comments.Add(item);
            }
        }


        public async Task<List<ProductInCategoriesAndTagsViewModel>> GetProductInCategoryAsync(string categoryId, int pageIndex, int pageSize)
        {
            string NameOfCategories = "";
            List<ProductInCategoriesAndTagsViewModel> productViewModel = new List<ProductInCategoriesAndTagsViewModel>();

            var allProduct = await (from n in ((from n in _context.Products.Include(c => c.ProductCategories)
                                                where (n.CreateDate <= DateTime.Now && n.ProductCategories.Select(c => c.CategoryId).Contains(categoryId))
                                                select (new
                                                {
                                                    n.ProductId,
                                                    n.ProductName,
                                                    n.ProductCode,
                                                    n.Stock,
                                                    n.Price,
                                                    n.Url,
                                                    n.ImageName,
                                                    n.Description,
                                                    n.CreateDate,
                                                })).Skip(pageIndex * pageSize).Take(pageSize))
                                    join e in _context.ProductCategories on n.ProductId equals e.ProductId into bc
                                    from bct in bc.DefaultIfEmpty()
                                    join c in _context.Categories on bct.CategoryId equals c.CategoryId into cg
                                    from cog in cg.DefaultIfEmpty()
                                    select (new ProductInCategoriesAndTagsViewModel
                                    {
                                        ProductId = n.ProductId,
                                        ProductName = n.ProductName,
                                        ProductCode = n.ProductCode,
                                        Price = n.Price,
                                        Stock = n.Stock,
                                        Url = n.Url,
                                        ImageName = n.ImageName,
                                        Description = n.Description,
                                        CreateDate = n.CreateDate,
                                        NameOfCategories = cog != null ? cog.CategoryName : "",
                                    })).AsNoTracking().ToListAsync();

            var productGroup = allProduct.GroupBy(g => g.ProductId).Select(g => new { ProductId = g.Key, ProductGroup = g });

            foreach (var item in productGroup)
            {
                NameOfCategories = "";
                foreach (var a in item.ProductGroup.Select(a => a.NameOfCategories).Distinct())
                {
                    if (NameOfCategories == "")
                        NameOfCategories = a;
                    else
                        NameOfCategories = NameOfCategories + " - " + a;
                }

                ProductInCategoriesAndTagsViewModel product = new ProductInCategoriesAndTagsViewModel()
                {
                    ProductId = item.ProductId,
                    ProductName = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).ProductName,
                    ProductCode = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).ProductCode,
                    Stock = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).Stock,
                    Price = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).Price,
                    Url = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).Url,
                    ImageName = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).ImageName,
                    Description = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).Description,
                    CreateDate = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).CreateDate,
                    PersianCreateDate = Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).CreateDate == null ? "-" : DateTimeExtensions.ConvertMiladiToShamsi(Enumerable.First<ProductInCategoriesAndTagsViewModel>(item.ProductGroup).CreateDate, "yyyy/MM/dd"),
                };
                productViewModel.Add(product);
            }

            return productViewModel;
        }


        public async Task<List<ProductInCategoriesAndTagsViewModel>> GetProductInTagAsync(string TagId, int pageIndex, int pageSize)
        {
            string NameOfCategories = "";
            List<ProductInCategoriesAndTagsViewModel> viewModel = new List<ProductInCategoriesAndTagsViewModel>();

            var allNews = await (from n in ((from n in _context.Products.Include(c => c.Comments).Include(c => c.ProductTags)
                                             where (n.CreateDate <= DateTime.Now && n.ProductTags.Select(c => c.TagId).Contains(TagId))
                                             select (new
                                             {
                                                 n.ProductId,
                                                 n.ProductName,
                                                 n.ProductCode,
                                                 n.Price,
                                                 n.Stock,
                                                 n.Url,
                                                 n.ImageName,
                                                 n.Description,
                                                 n.CreateDate,
                                             })).Skip(pageIndex * pageSize).Take(pageSize))
                                 join e in _context.ProductCategories on n.ProductId equals e.ProductId into bc
                                 from bct in bc.DefaultIfEmpty()
                                 join c in _context.Categories on bct.CategoryId equals c.CategoryId into cg
                                 from cog in cg.DefaultIfEmpty()
                                 select (new ProductInCategoriesAndTagsViewModel
                                 {
                                     ProductId = n.ProductId,
                                     ProductName = n.ProductName,
                                     ProductCode = n.ProductCode,
                                     Price = n.Price,
                                     Stock = n.Stock,
                                     Url = n.Url,
                                     ImageName = n.ImageName,
                                     Description = n.Description,
                                     CreateDate = n.CreateDate,
                                     NameOfCategories = cog != null ? cog.CategoryName : "",
                                 })).AsNoTracking().ToListAsync();

            var newsGroup = allNews.GroupBy(g => g.ProductId).Select(g => new { ProductId = g.Key, ProductGroup = g });
            foreach (var item in newsGroup)
            {
                NameOfCategories = "";
                foreach (var a in item.ProductGroup.Select(a => a.NameOfCategories).Distinct())
                {
                    if (NameOfCategories == "")
                        NameOfCategories = a;
                    else
                        NameOfCategories = NameOfCategories + " - " + a;
                }

                ProductInCategoriesAndTagsViewModel product = new ProductInCategoriesAndTagsViewModel()
                {
                    ProductId = item.ProductId,
                    ProductName = item.ProductGroup.First().ProductName,
                    ProductCode = item.ProductGroup.First().ProductCode,
                    Price = item.ProductGroup.First().Price,
                    Stock = item.ProductGroup.First().Stock,
                    Url = item.ProductGroup.First().Url,
                    ImageName = item.ProductGroup.First().ImageName,
                    Description = item.ProductGroup.First().Description,
                    PersianCreateDate = item.ProductGroup.First().CreateDate.ConvertMiladiToShamsi("yyyy/MM/dd"),
                    NameOfCategories = NameOfCategories,
                    NumberOfComments = item.ProductGroup.First().NumberOfComments,
                };
                viewModel.Add(product);
            }
            return viewModel;
        }


        public async Task<string> GetWeeklyProductAsync()
        {
            string content = "";
            int NumOfWeek = DateTimeExtensions.ConvertMiladiToShamsi(DateTime.Now, "dddd").GetNumOfWeek();
            DateTime StartMiladiDate = DateTime.Now.AddDays((-1) * NumOfWeek).Date + new TimeSpan(0, 0, 0);
            DateTime EndMiladiDate = DateTime.Now;

            var products = await (from n in _context.Products
                                  where (n.CreateDate <= EndMiladiDate && StartMiladiDate <= n.CreateDate)
                                  select (new ProductViewModel
                                  {
                                      ProductId = n.ProductId,
                                      ProductName = n.ProductName,
                                      Url = n.Url,
                                      ImageName = n.ImageName,
                                  })).OrderByDescending(o => o.CreateDate).AsNoTracking().ToListAsync();

            string url = _configuration.GetValue<string>("SiteSettings:SiteInfo:Url");

            foreach (var item in products)
                content = content + $"<div style='direction:rtl;font-family:tahoma;text-align:center'> <div class='row align-items-center'> <div class='col-12 col-lg-6'><div class='post-thumbnail'> <img src='{url + "/newsImage/" + item.ImageName}' alt='{item.ImageName}'> </div> </div> <div class='col-12 col-lg-6'> <div class='post-content mt-0'> <h4 style='color:#878484;'>{item.ProductName}</h4> <a href='{url}/News/{item.ProductId}/{item.Url}'>[ادامه مطلب]</a> </div> </div> </div> </div><hr/>";

            return content;
        }


        public async Task<List<ProductViewModel>> SearchInProduct(string textSearch)
        {
            List<ProductViewModel> productViewModel = new List<ProductViewModel>();

            var allProduct = await (from n in _context.Products.Where(n => (n.ProductName.Contains(textSearch) || n.Description.Contains(textSearch)) && n.CreateDate <= DateTime.Now)
                                    select (new ProductViewModel
                                    {
                                        ProductId = n.ProductId,
                                        ProductName = n.ProductName,
                                        ProductCode = n.ProductCode,
                                        Price = n.Price,
                                        Stock = n.Stock,
                                        Url = n.Url,
                                        ImageName = n.ImageName,
                                        Description = n.Description,
                                        CreateDate = n.CreateDate,
                                    })).AsNoTracking().ToListAsync();

            var productGroup = allProduct.GroupBy(g => g.ProductId).Select(g => new { ProductId = g.Key, ProductGroup = g });

            foreach (var item in productGroup)
            {
                ProductViewModel product = new ProductViewModel()
                {
                    ProductId = item.ProductId,
                    ProductName = item.ProductGroup.First().ProductName,
                    ProductCode = item.ProductGroup.First().ProductCode,
                    Stock = item.ProductGroup.First().Stock,
                    Price = item.ProductGroup.First().Price,
                    Url = item.ProductGroup.First().Url,
                    ImageName = item.ProductGroup.First().ImageName,
                    Description = item.ProductGroup.First().Description,
                    CreateDate = item.ProductGroup.First().CreateDate,
                    PersianCreateDate = item.ProductGroup.First().CreateDate == null ? "-" : DateTimeExtensions.ConvertMiladiToShamsi(item.ProductGroup.First().CreateDate, "yyyy/MM/dd"),
                };
                productViewModel.Add(product);
            }

            return productViewModel;
        }


        public int CountProduct() => _context.Products.Count();
        public int CountAvailableProduct() => _context.Products.Where(p => p.Stock > 0).Count();
        public int CountUnAvailableProduct() => _context.Products.Where(n => n.Stock == 0).Count();




        public async Task<CustomerViewModel> GetCustomerIdAsync(int userId)
        {
            CustomerViewModel customer = null;

            var customerInfo = await( from n in _context.Customers
                               where (n.CustomerId == userId)
                               select (new CustomerViewModel
                               {
                                   CustomerId = n.CustomerId,
                               })).AsNoTracking().ToListAsync();

            if (customerInfo.Count() != 0)
            {
                var customerGroup = customerInfo.GroupBy(g => g.CustomerId).Select(g => new { CustomerId = g.Key, CustomerGroup = g });

                customer = new CustomerViewModel()
                {
                    CustomerId = customerGroup.First().CustomerGroup.First().CustomerId,
                };
            }

            return customer;
        }
        public int GetOrderByCustomerId(int userId) => _context.Orders.Where(c => c.CustomerId == userId).Count();
        public Order GetOrderIdByCustomerId(int userId) => _context.Orders.Where(c => c.CustomerId == userId).FirstOrDefault();



        public async Task<List<OrderViewModel>> GetPaginateOrdersAsync(int offset, int limit, string orderBy)
        {
            string CustomerName = "";
            List<OrderViewModel> viewModels = new List<OrderViewModel>();

            var allNews = await (from n in ((from n in _context.Orders.Include(v => v.Customer)
                                             select (new
                                             {
                                                 n.OrderId,
                                                 n.AmountPaid,
                                                 n.Quantity,
                                                 n.DispatchNumber,
                                                 n.OrderStatus,
                                                 n.PurchaseDateTime,
                                                 Calculate = n.AmountPaid * n.Quantity,
                                                 CustomerName = n.Customer.FirstName1 + " " + n.Customer.LastName1,
                                             })).OrderBy(orderBy).Skip(offset).Take(limit))
                                 join o in _context.ProductOrders on n.OrderId equals o.ProductId into po
                                 from pot in po.DefaultIfEmpty()
                                 join p in _context.Products on pot.OrderId equals p.ProductId into op
                                 from opt in op.DefaultIfEmpty()
                                 select (new OrderViewModel
                                 {
                                     OrderId = n.OrderId,
                                     AmountPaid = n.AmountPaid,
                                     Quantity = n.Quantity,
                                     DispatchNumber = n.DispatchNumber,
                                     OrderStatus = n.OrderStatus,
                                     PurchaseDateTime = n.PurchaseDateTime,
                                     Calculate = n.AmountPaid * n.Quantity,
                                     CustomerName = n.CustomerName,
                                     /////////////////////////
                                     ProductId = opt.ProductId,
                                     ProductName = opt.ProductName,
                                     ProductImageName = opt.ImageName,
                                     ProductCode = opt.ProductCode,
                                     ProductUrl = opt.Url,
                                     ProductPrice = opt.Price,
                                 })).AsNoTracking().ToListAsync();


            var orderGroup = allNews.GroupBy(g => g.OrderId).Select(g => new { OrderId = g.Key, OrderGroup = g });

            foreach (var item in orderGroup)
            {
                CustomerName = "";

                foreach (var a in item.OrderGroup.Select(a => a.CustomerName).Distinct())
                {
                    if (CustomerName == "")
                        CustomerName = a;
                    else
                        CustomerName = CustomerName + " - " + a;
                }

                OrderViewModel order = new OrderViewModel()
                {
                    OrderId = item.OrderId,
                    AmountPaid = item.OrderGroup.First().AmountPaid,
                    Quantity = item.OrderGroup.First().Quantity,
                    DispatchNumber = item.OrderGroup.First().DispatchNumber,
                    OrderStatus = item.OrderGroup.First().OrderStatus,
                    PurchaseDateTime = item.OrderGroup.First().PurchaseDateTime,
                    Calculate = item.OrderGroup.First().Calculate,
                    CustomerName = CustomerName,
                    ///////////////////////////
                    ProductId = item.OrderGroup.First().ProductId,
                    ProductName = item.OrderGroup.First().ProductName,
                    ProductImageName = item.OrderGroup.First().ProductImageName,
                    ProductCode = item.OrderGroup.First().ProductCode,
                    ProductUrl = item.OrderGroup.First().ProductUrl,
                    ProductPrice = item.OrderGroup.First().ProductPrice,
                };
                viewModels.Add(order);
            }

            foreach (var item in viewModels)
                item.Row = ++offset;

            return viewModels;
        }


        public async Task<OrderViewModel> GetOrderByIdAsync(string ordertId)
        {
            OrderViewModel order = null;

            var orderInfo = await (from n in _context.Orders.Where(n => n.OrderId == ordertId)
                                      select (new OrderViewModel
                                      {
                                          OrderId = n.OrderId,
                                          AmountPaid = n.AmountPaid,
                                          Quantity = n.Quantity,
                                          DispatchNumber = n.DispatchNumber,
                                          OrderStatus = n.OrderStatus,
                                          PurchaseDateTime = n.PurchaseDateTime,
                                          Calculate = n.AmountPaid * n.Quantity,
                                      })).AsNoTracking().ToListAsync();

            if (orderInfo.Count() != 0)
            {
                var orderGroup = orderInfo.GroupBy(g => g.OrderId).Select(g => new { OrderId = g.Key, OrderGroup = g });

                order = new OrderViewModel()
                {
                    OrderId = orderGroup.First().OrderGroup.First().OrderId,
                    AmountPaid = orderGroup.First().OrderGroup.First().AmountPaid,
                    Quantity = orderGroup.First().OrderGroup.First().Quantity,
                    DispatchNumber = orderGroup.First().OrderGroup.First().DispatchNumber,
                    OrderStatus = orderGroup.First().OrderGroup.First().OrderStatus,
                    PurchaseDateTime = orderGroup.First().OrderGroup.First().PurchaseDateTime,
                    Calculate = orderGroup.First().OrderGroup.First().Calculate,
                };
            }

            return order;
        }
    }
}
