﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Mahak.Data.Contracts;
using Mahak.ViewModel.Slider;
using Mahak.Common.Extensions;
using Mahak.Entities;

namespace Mahak.Data.Repositories
{
    public class SliderRepository : ISliderRepository
    {
        private readonly MahakDBContext _context;
        public SliderRepository(MahakDBContext context)
        {
            _context = context;
        }


        public async Task<List<SliderViewModel>> GetPaginateSlidersAsync(int offset, int limit, string orderBy, string searchText)
        {
            var getDateTimesForSearch = searchText.GetDateTimeForSearch();

            List<SliderViewModel> sliders = await _context.Sliders.Where(c => c.Title.Contains(searchText) || c.SliderLocations.Equals(searchText))
                                    .OrderBy(orderBy).Skip(offset).Take(limit)
                                    .Select(c => new SliderViewModel
                                    {
                                        SliderId = c.SliderId,
                                        Title = c.Title,
                                        Url = c.Url,
                                        ImageName = c.ImageName,
                                        Locations = c.SliderLocations == SliderLocation.Home ? "صفحه اول" : c.SliderLocations == SliderLocation.CategoriesPaginate ? "صفحه دسته بندی ها" : "-",
                                    }).AsNoTracking().ToListAsync();

            foreach (var item in sliders)
                item.Row = ++offset;

            return sliders;
        }


        public string CheckSliderFileName(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName);
            int fileNameCount = _context.Sliders.Where(f => f.ImageName == fileName).Count();
            int j = 1;

            while (fileNameCount != 0)
            {
                fileName = fileName.Replace(fileExtension, "") + j + fileExtension;
                fileNameCount = _context.Sliders.Where(f => f.ImageName == fileName).Count();
                j++;
            }

            return fileName;
        }
    }
}
