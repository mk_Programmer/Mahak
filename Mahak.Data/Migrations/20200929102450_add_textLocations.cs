﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Mahak.Data.Migrations
{
    public partial class add_textLocations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TextLocations",
                table: "News",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TextLocations",
                table: "News");
        }
    }
}
