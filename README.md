### بسمه التعالی

### پروزه سایت موسسه خیریه محک با .Net Core 3.1

این پروژه بصورت 7 لایه و ارتباط با برقراری رفرنس های الویت بندی شده بروی گیت لب قرار داده شده.

این سایت نیز مانند اکثر سایت های دیگر بغیر از صفحات اصلی برای عموم افراد، دارای یک پنل مدیریت است که عمده بخش های این پنل از کدهای آموزش ویدئویی دیگری با موضوع سایت خبری کمک گرفته شده.

لایه شروع و ابتدایی پروژه  `Mahak` است.

===========================

### Admin users credentails

`Username:` Admin

`Password:` 123456

===========================

### قسمت هایی که بنده اضافه کرده ام:

`Mahak` --> `Areas` --> `Admin` --> Controllers & Views

MessageUsers

Product

Slider


`Mahak` --> Controllers & Views

Account

AddressManager

Code

Home


`Mahak` --> Views

Shared

---------------------------------

`Mahak.Data` --> Repositories

MassageUsers

Product

Slider


`Mahak.Data` --> Mapping

ProductCategory

ProductOrder

ProductTag

---------------------------------

`Mahak.Entities` --> ~

MassageUsers

Product

ProductCategory

ProductOrder

ProductTag

Slider

---------------------------------

`Mahak.ViewModel` --> ~

Account

Customer

Home

Order

Product

Slider

-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------

### قسمتهایی که تکمیل شده اند و توضیح نواقص و مشکلات :

**پنل ادمین**

کنترلر  Product افزودن ویرایش یا حذف محصولات سایت را انجام می دهد
-- نکته : برای درج یا ویرایش محصول ModelState غیر معتبر است که باید رفع شود
کنترلر MessageUsers برای نمایش . پاسخ دادن به نظرات یا انتقادات کاربران از طریق پروفایل خود است
-- نکته : این کنترلر کامل نشده
کنترلر Slider افزودن ویرایش یا حذف اسلایدرهای صفحات مختلف سایت را انجام می دهد

--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------

### صفحات اصلی

**قسمت login + TwoFactor**

عملیات ورود و خروج و عضویت کاربر بهمراه احراز هویت دو مرحله که کدهای ان در کنترلر Account نوشته شده اند.
عملیات بازیابی رمز عبور وقتی فراموشی رمز عبور صورت گرفته که کدهای این قسمت در کنترلر Account است.
-- نکته : برای احراز هویت نواقصی وجود دارد که کد احراز هویت فقط به ایمیل فرستاده می شود و پنل پیامک خریداری نشده همچنین کدی برای قسمت کد اعتبارسنجی مدت دار نوشته نشده. 

**قسمت پنل کاربر**

این قسمت در کنترلر Account  و  ویو Profile و پارشیال ویوی _ProfileMenu قرار گرفته و قسمت های زیر تکمیل شده اند
انتقادات و پیشنهادات <-- کدهای این قسمت در کنترلر Account و ویو به همین نام قرار گرفته
آدرس ها --> کدهای این قسمت در کنترلر AddressManager قرار گرفته  
اطلاعات حساب كاربری --> کدهای این قسمت در کنترلر Account و ویو به همین نام قرار گرفته

**قسمت دسته بندی محصولات**

صفحه اول دسته بندی ها تقریبا درست شده که کدهای ان در کنترلر Home قرار دارد
با انتخاب دسته بندی باید لیستی ار محصولات مرتبط نمایش داده شود که در این قسمت مشکل وجود دارد
قسمت توضیحات محصول انتخابی کامل شده اما کاربر باید بتواند نظری برای ان ثبت کند که این مورد ناقص است


-------------------------------------
-------------------------------------


# Mahak-Charity

My demo on creating web charity with asp.net core 3.1

----------------------------
### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

----------------------------
### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be After cloning or downloading project, there are some steps you need to do in order to run application, depends on which operation system are you using

----------------------------
### Admin users credentails

`Username:` Admin

`Password:` 123456

----------------------------
### Windows

You need to have .Net Core 2.2 installed on your machine
MSSQL localdb server

MacOs

You need to have .Net Core 2.2 installed on your machine
Sqlite3 installed
Linux

You need to have .Net Core 2.2 installed on your machine
PostgreSql installed, and configured to have user with -- User id: postgres -- Password: password
PostgreSql server will run on localhost:5432 by default, you can change setings from 'appsettings.Development.json' file Screenshot-20190608104225-2226x428

----------------------------
### How to Run

In order to run application you need to go Shop.Web project, and execute commands in bash/terminal below This will run application in default mode

----------------------------
### dotnet run

This will run application, and start watch, each time you make changes in c# code, application will restart automatically

----------------------------
### dotnet watch run

After running application, you should see output something like this

----------------------------
### Running the tests

Currently there are no tests to run

----------------------------
### Built With

Asp.Net Core - The web framework used
Entity Framework Core - ORM

Sql servers

Microsoft SQL server - Used Sql server for windows
Postgre SQL - Used Sql server for linux
SQLite - Used Sql server for MacOs

Authors

`Mohammad Khaksari` - Initial work - `Khaksari`

See also the list of contributors who participated in this project.

----------------------------
### License

This project is licensed under the MIT License - see the LICENSE.md file for details
